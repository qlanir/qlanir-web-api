﻿using Newtonsoft.Json;
using qlanir_web_api.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace qlanir_mail_importer
{
    public class QlanirProvider
    {
        public static String URL_STRING
        {
            get
            {
                return ConfigurationManager.AppSettings["QLANIR_API_URL"];
            }
        }

        public static String SYSTEM_USER_TOCKEN
        {
            get
            {
                return ConfigurationManager.AppSettings["QLANIR_SYSTEM_USER_TOKEN"];
            }
        }

        public static HttpWebResponse PostMethod<T>(T subject, string link, string param) where T : Subject
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(String.Format("{0}/{1}?{2}", URL_STRING, link, param)); //example url_string=qlanir.com/api; link=people/import; param='email=test@qlanir.com' => url=qlanir.com/api/people/import?email=test@qlanir.com
            request.Method = "POST";
            //request.Credentials = CredentialCache.DefaultCredentials;

            UTF8Encoding encoding = new UTF8Encoding();

            String postedData = JsonConvert.SerializeObject(subject);

            var bytes = encoding.GetBytes(postedData);

            request.Headers.Add("Authorization", String.Format("tocken {0}", SYSTEM_USER_TOCKEN));

            request.ContentType = "application/json;charset=UTF-8";
            request.ContentLength = bytes.Length;

            using (var newStream = request.GetRequestStream())
            {
                newStream.Write(bytes, 0, bytes.Length);
                newStream.Close();
            }
            return (HttpWebResponse)request.GetResponse();
        }
    }
}
