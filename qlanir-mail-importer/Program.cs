﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qlanir_mail_importer
{
    class Program
    {
        static void Main(string[] args)
        {
             bool isRunning = false;
             using (var m = new System.Threading.Mutex(true, "qlanir-mail-import", out isRunning))
             {
                 if (!isRunning)
                     return;
                 MailImporter.Start();
             }
        }
    }
}
