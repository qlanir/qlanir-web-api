﻿using Limilabs.Mail.BusinessCard;
using OpenPop.Mime;
using OpenPop.Pop3;
using qlanir_web_api.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qlanir_mail_importer
{
    public static class MailImporter
    {

        public static void Start()
        {
            var path_for_logs = ConfigurationManager.AppSettings["PATH_FOR_LOGS"];
            if (!String.IsNullOrEmpty(path_for_logs)) //init dir
            {
                DirectoryInfo dir = new DirectoryInfo(path_for_logs);
                if (!dir.Exists)
                    dir.Create();
                path_for_logs += "\\";
            }

            using (StreamWriter streamWriter = File.AppendText(String.Format("{1}log_{0}.txt", DateTime.Now.ToShortDateString(), path_for_logs)))
            {
                using (Pop3Client client = new Pop3Client())
                {
                    try
                    {
                        initPop3Client(client);
                        int messageCount = client.GetMessageCount();

                        Dictionary<String, List<VCard>> vCards = new Dictionary<String, List<VCard>>();

                        for (int i = messageCount; i > 0; i--)
                        {
                            try
                            {
                                Message message = client.GetMessage(i);

                                var res = GetVCards(message);

                                var address = message.Headers.To[0].Address;
                                if (res.Count != 0)
                                    if (vCards.ContainsKey(address))
                                        vCards[address].AddRange(res);
                                    else
                                        vCards.Add(address, res);
                            }
                            catch (Exception e)
                            {
                                log(streamWriter, e);
                            }
                        }


                        //-----init items from vCard-------
                        Dictionary<String, List<Person>> itemsForImport = new Dictionary<String, List<Person>>();

                        foreach (var address in vCards.Keys)
                        {
                            if (!itemsForImport.ContainsKey(address))
                                itemsForImport.Add(address, new List<Person>());

                            foreach (var vCard in vCards[address])
                            {
                                Person person = new Person();
                                if (vCard.Name != null && (!String.IsNullOrEmpty(vCard.Name.FirstName) || !String.IsNullOrEmpty(vCard.Name.LastName)))
                                {
                                    var nameParam = vCard.GetHeader("N").ValueParameters;

                                    var middleName = "";
                                    if (nameParam.Count > 2 && !String.IsNullOrEmpty(nameParam[2].Key))
                                        middleName = nameParam[2].Key;

                                    var name = string.Join(" ", String.Format("{0} {1} {2}", vCard.Name.LastName, vCard.Name.FirstName, middleName).Split(' ').Where(s => !String.IsNullOrEmpty(s)));

                                    person.Label = name;
                                }
                                else if (!String.IsNullOrEmpty(vCard.FullName))
                                    person.Label = vCard.FullName; //TODO: ИОФ -> ФИО
                                else
                                    person.NameParts = new PersonNameParts { FirstName = "Безымянный контакт" };

                                var contacts = new List<ContactLink>();

                                foreach (var phone in vCard.Phones)
                                {
                                    Tag role = new Tag();

                                    if (phone.Types.Count != 0)
                                    {
                                        foreach (var _role in phone.Types)
                                        {
                                            //var _role = phone.Types[0].Name.ToLower();
                                            GetRole(out role, _role.Name.ToLower());
                                            if (role != null)
                                                break;
                                        }
                                    }
                                    else
                                        role = null;

                                    contacts.Add(new ContactLink
                                    {
                                        Tag = role,
                                        Subject = new Contact { Label = phone.Phone.Replace("+", ""), ContactType = new Tag { Id = (int)TagTypes.Phone } }
                                    });

                                }

                                foreach (var email in vCard.Emails)
                                {
                                    Tag role = new Tag();

                                    if (email.Types.Count != 0)
                                    {
                                        foreach (var _role in email.Types)
                                        {
                                            //var _role = email.Types[0].Name.ToLower();
                                            GetRole(out role, _role.Name.ToLower());
                                            if (role != null)
                                                break;
                                        }
                                    }
                                    else
                                        role = null;

                                    contacts.Add(new ContactLink
                                    {
                                        Tag = role,
                                        Subject = new Contact { Label = email.Value, ContactType = new Tag { Id = (int)TagTypes.Email } }
                                    });
                                }

                                if (!String.IsNullOrEmpty(vCard.Url))
                                    contacts.Add(new ContactLink
                                    {
                                        Subject = new Contact { Label = vCard.Url, ContactType = new Tag { Id = (int)TagTypes.Site } }
                                    });


                                person.Contacts = contacts.ToArray();

                                itemsForImport[address].Add(person);
                            }

                        }

                        //--------sendItems-------
                        foreach (var address in itemsForImport.Keys)
                            foreach (var item in itemsForImport[address])
                            {
                                var response = QlanirProvider.PostMethod(item, "people/import", String.Format("email={0}", address));
                                response.Close();
                            }

                        client.DeleteAllMessages(); //DELETE All readed e-mails
                    }
                    catch (Exception e)
                    {
                        log(streamWriter, e);
                    }
                }
            }
        }

        private static void GetRole(out Tag role, string _role)
        {
            role = new Tag();
            if (_role == "work")
                role.Id = -44;
            else if (_role == "home")
                role.Id = -45;
            else if (_role == "fax")
                role.Id = -46;
            else if (_role == "cell")
                role.Id = -47;
            else if (_role == "pref")
                role.Id = -48;
            /*else if (!String.IsNullOrEmpty(_role))
                role.Label = _role;*/
            else
                role = null;
        }

        private static void initPop3Client(this Pop3Client client)
        {
            var mailHost = ConfigurationManager.AppSettings["MAIL_HOST"];
            var mailPort = int.Parse(ConfigurationManager.AppSettings["MAIL_PORT"]);
            var mailLogin = ConfigurationManager.AppSettings["MAIL_LOGIN"];
            var mailPassword = ConfigurationManager.AppSettings["MAIL_PASSWORD"];
            var useSSL = bool.Parse(ConfigurationManager.AppSettings["MAIL_USE_SLL"]);

            if (client == null)
                client = new Pop3Client();

            client.Connect(mailHost, mailPort, useSSL);

            client.Authenticate(mailLogin, mailPassword);
        }

        #region Log

        private static void log(StreamWriter streamWriter, Exception e)
        {
            log(streamWriter, String.Format("{0} {1}", String.Format("{0:HH:mm:ss}", DateTime.Now), e.Message));
        }

        private static void log(StreamWriter streamWriter, string message)
        {
            streamWriter.WriteLine(message);
        }

        #endregion

        #region vCards

        private static List<VCard> GetVCards(MessagePart messagePart, List<VCard> list, VCardParser parser)
        {
            if (list == null)
                list = new List<VCard>();

            if (messagePart.IsAttachment && (messagePart.ContentType.MediaType == "text/x-vcard" || messagePart.ContentType.MediaType == "application/octet-stream" || messagePart.ContentType.Name.ToLower().Contains(".vcf") || messagePart.ContentType.Name.ToLower().Contains(".vcard")))
            {
                try
                {
                    //var vCard = parser.Parse(Encoding.UTF8.GetString(messagePart.Body, 0, messagePart.Body.Count()));

                    var _vcds = Encoding.UTF8.GetString(messagePart.Body, 0, messagePart.Body.Count()).Split(new[] { "BEGIN:VCARD" }, StringSplitOptions.RemoveEmptyEntries);

                    foreach (var vc in _vcds)
                    {
                        if (String.IsNullOrEmpty(vc))
                            continue;

                        var vCard = parser.Parse("BEGIN:VCARD" + vc);

                        list.Add(vCard);
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
            else if (messagePart.MessageParts != null && messagePart.MessageParts.Count != 0)
                foreach (var _messagePart in messagePart.MessageParts)
                    GetVCards(_messagePart, list, parser);

            return list;
        }

        private static List<VCard> GetVCards(Message message)
        {
            return GetVCards(message.MessagePart, new List<VCard>(), new VCardParser());
        }

        #endregion
    }
}
