﻿## Setup data sources

+ MS SQL 
+ Neo4J ~2.3.2
+ Elastic search ~1.7

### MS SQL
1. Create db 
+ Collattion should be `Cyrillic_General_CS_AI`
+ Or chnage via change-collation.sql script
1. Update Data base from model, all checkbox off
2. run scripts
+ funcs_views_columns.sql
+ defaults.ru-RU.sql
+ defaults.en-EN.sql

### Run ElasticSearch and Neo4J via docker-compose

docker-compose file is in `www-web-app` project

`docker-compose up`

### Initialize data

`http://localhost:8080/api/admin/clean`
Then restart app


## Important - Commands for Docker on Windows 

## Neo4J

```
docker run \
    --detach \
    --publish=7474:7474 \
    --volume=$HOME/neo4j/data:/data \
	--name=neo \
    neo4j/neo4j
```

##Elastic search

```
docker run  -d -v "$PWD/config":/usr/share/elasticsearch/config -v "$PWD/esdata":/usr/share/elasticsearch/data -p 9200:9200 -p 9300:9300 --name es elasticsearch:1.7 -Des.node.name="Node01" -Des.network.host=::0
```
http://baio-qr.cloudapp.net:9200
http://neo4j:123@baio-qr.cloudapp.net:7474/db/data

