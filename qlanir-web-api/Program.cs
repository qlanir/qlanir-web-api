﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;
using Owin;
using System.Web;
using System.IO;
using Microsoft.Owin.Extensions;
using System.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Web.Http;
using Microsoft.Owin.Hosting;



//[assembly: OwinStartup(typeof(qlanir_web_api.Program))]
namespace qlanir_web_api
{
    
    class Program
    {
        static void Main(string[] args)
        {
            string uri = ConfigurationManager.AppSettings.Get("SERVER_URI");

            using (WebApp.Start<qlanir_web_api.Startup>(uri))
            {
                Console.WriteLine(string.Format("Server runned on {0}", uri));

                Console.ReadKey();

                Console.WriteLine("Bye");
            }
        }
    }

    public class Startup
    {

        public void Configuration(IAppBuilder app)
        {
            app.Use<JWTAuthenticationComponent>();

            ConfigureWebApi(app);

            //Create user if necessary

            var user = ConfigurationManager.AppSettings.Get("DEV_USER");

            if (!string.IsNullOrEmpty(user))
            {
                qlanir_web_api.DataContext.ProfileDataContext pdx = new DataContext.ProfileDataContext();
                pdx.CreateUserIfNotExist(user, user, Models.TagTypes.Email);
            }
            
        }

        private void ConfigureWebApi(IAppBuilder app)
        {
            var config = new HttpConfiguration();
            
            config.MapHttpAttributeRoutes();
            config.DependencyResolver = new NinjectResolver(NinjectConfig.CreateKernel());
            var cors = new System.Web.Http.Cors.EnableCorsAttribute("*", "*", "GET,POST,PUT,DELETE");        
            config.EnableCors(cors);

            JsonSerializerSettings jsonSetting = new JsonSerializerSettings();
            jsonSetting.Converters.Add(new Newtonsoft.Json.Converters.StringEnumConverter { CamelCaseText = true });
            
            var jsonFormatter = config.Formatters.JsonFormatter;// config.Formatters.OfType<JsonMediaTypeFormatter>().FirstOrDefault();
            jsonFormatter.SerializerSettings = jsonSetting;
            jsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();


            app.UseWebApi(config);

            config.EnsureInitialized();           
        }


    }

}
