﻿/* RU - 100000 */
SET IDENTITY_INSERT EventTypes ON;
GO
INSERT INTO EventTypes (Id, Name) VALUES (-1 - 100000, 'legal relation')
INSERT INTO EventTypes (Id, Name) VALUES (-2- 100000, 'use')
INSERT INTO EventTypes (Id, Name) VALUES (-3- 100000, 'registration')
INSERT INTO EventTypes (Id, Name) VALUES (-4- 100000, 'advantage')
INSERT INTO EventTypes (Id, Name) VALUES (-5- 100000, 'user event')
INSERT INTO EventTypes (Id, Name) VALUES (-6- 100000, 'invalid')
INSERT INTO EventTypes (Id, Name) VALUES (-7- 100000, 'diposal')
INSERT INTO EventTypes (Id, Name) VALUES (-8- 100000, 'proprietary relationship')
INSERT INTO EventTypes (Id, Name) VALUES (-9- 100000, 'recruitment')
INSERT INTO EventTypes (Id, Name) VALUES (-10- 100000, 'relation') /* default */
INSERT INTO EventTypes (Id, ParentId, Name) VALUES (-11- 100000, -5- 100000, 'meeting')
INSERT INTO EventTypes (Id, Name) VALUES (-12- 100000, 'dissmisal')
INSERT INTO EventTypes (Id, Name) VALUES (-13- 100000, 'ownership')
INSERT INTO EventTypes (Id, Name) VALUES (-14- 100000, 'resource')
INSERT INTO EventTypes (Id, Name) VALUES (-15-100000, 'resource acquitance')
GO
SET IDENTITY_INSERT EventTypes OFF;
GO
SET IDENTITY_INSERT Subjects ON;
GO
INSERT INTO Subjects (Id) VALUES (-1)
GO
SET IDENTITY_INSERT Subjects OFF;
GO


SET IDENTITY_INSERT [dbo].[Tags] ON
INSERT INTO [dbo].[Tags] ([Id], [ParentId], [Name]) VALUES (-1-100000, NULL, 'business')
INSERT INTO [dbo].[Tags] ([Id], [ParentId], [Name]) VALUES (-2-100000, -1-100000, 'trade')
INSERT INTO [dbo].[Tags] ([Id], [ParentId], [Name]) VALUES (-3-100000, -1-100000, 'manufacture')
INSERT INTO [dbo].[Tags] ([Id], [ParentId], [Name]) VALUES (-4-100000, -1-100000, 'finance')
INSERT INTO [dbo].[Tags] ([Id], [ParentId], [Name]) VALUES (-5-100000, NULL, 'competitions')
INSERT INTO [dbo].[Tags] ([Id], [ParentId], [Name]) VALUES (-6-100000, NULL, 'discount')
INSERT INTO [dbo].[Tags] ([Id], [ParentId], [Name]) VALUES (-7-100000, NULL, 'passion')
INSERT INTO Tags (Id, Name) VALUES (-8-100000, 'identifiers')
INSERT INTO Tags (Id, Name) VALUES (-9-100000, 'address')
INSERT INTO Tags (Id, Name) VALUES (-10-100000, 'property')
INSERT INTO Tags (Id, Name) VALUES (-11-100000, 'equipment')
INSERT INTO Tags (Id, ParentId, Name) VALUES (-12-100000, -8-100000, 'phone')
INSERT INTO Tags (Id, ParentId, Name) VALUES (-13-100000, -8-100000, 'google contact')
INSERT INTO Tags (Id, ParentId, Name) VALUES (-14-100000, -8-100000, 'e-mail')
INSERT INTO Tags (Id, ParentId, Name) VALUES (-15-100000, -8-100000, 'skype')
INSERT INTO Tags (Id, ParentId, Name) VALUES (-16-100000, -8-100000, 'site')

INSERT INTO [dbo].[Tags] ([Id], [ParentId], [Name]) VALUES (-500-100000, NULL, 'system')
INSERT INTO [dbo].[Tags] ([Id], [ParentId], [Name]) VALUES (-501-100000, -500-100000, 'profile')
INSERT INTO [dbo].[Tags] ([Id], [ParentId], [Name]) VALUES (-502-100000, -500-100000, 'authenticator')
INSERT INTO [dbo].[Tags] ([Id], [ParentId], [Name]) VALUES (-503-100000, -500-100000, 'unknown provider')

INSERT INTO Tags (Id, Name) VALUES (-600-100000, 'import')
INSERT INTO Tags (Id, ParentId, Name) VALUES (-601-100000, -600-100000, 'not confirmed')
INSERT INTO Tags (Id, ParentId, Name) VALUES (-602-100000, -600-100000, 'mailed contact')
INSERT INTO Tags (Id, ParentId, Name) VALUES (-603-100000, -8-100000, 'own mailbox')
SET IDENTITY_INSERT [dbo].[Tags] OFF

GO

SET IDENTITY_INSERT [dbo].[Roles] ON
GO
INSERT INTO [dbo].[Roles] ([Id], [ParentId], [Name]) VALUES (-1-100000, NULL, 'employer')
INSERT INTO [dbo].[Roles] ([Id], [ParentId], [Name]) VALUES (-2-100000, NULL, 'working relationship')
INSERT INTO [dbo].[Roles] ([Id], [ParentId], [Name]) VALUES (-3-100000, -2-100000, 'chief')
INSERT INTO [dbo].[Roles] ([Id], [ParentId], [Name]) VALUES (-4-100000, -2-100000, 'employee')
INSERT INTO [dbo].[Roles] ([Id], [ParentId], [Name]) VALUES (-5-100000, -2-100000, 'partner')
INSERT INTO [dbo].[Roles] ([Id], [ParentId], [Name]) VALUES (-6-100000, NULL, 'kinship')
INSERT INTO [dbo].[Roles] ([Id], [ParentId], [Name]) VALUES (-7-100000, -6-100000, 'brother')
INSERT INTO [dbo].[Roles] ([Id], [ParentId], [Name]) VALUES (-8-100000, -6-100000, 'sister')
INSERT INTO [dbo].[Roles] ([Id], [ParentId], [Name]) VALUES (-9-100000, -6-100000, 'daughter')
INSERT INTO [dbo].[Roles] ([Id], [ParentId], [Name]) VALUES (-10-100000, -6-100000, 'mather')
INSERT INTO [dbo].[Roles] ([Id], [ParentId], [Name]) VALUES (-11-100000, -6-100000, 'father')
INSERT INTO [dbo].[Roles] ([Id], [ParentId], [Name]) VALUES (-12-100000, -6-100000, 'sister')
INSERT INTO [dbo].[Roles] ([Id], [ParentId], [Name]) VALUES (-13-100000, -6-100000, 'cousin')
INSERT INTO [dbo].[Roles] ([Id], [ParentId], [Name]) VALUES (-14-100000, -6-100000, 'sun')
INSERT INTO [dbo].[Roles] ([Id], [ParentId], [Name]) VALUES (-15-100000, NULL, 'position')
INSERT INTO [dbo].[Roles] ([Id], [ParentId], [Name]) VALUES (-16-100000, -15-100000, 'general manager')
INSERT INTO Roles (Id, Name) VALUES (-17-100000, 'employer')
INSERT INTO Roles (Id, Name) VALUES (-18-100000, 'user')
INSERT INTO Roles (Id, Name) VALUES (-19-100000, 'legal relation')
INSERT INTO Roles (Id, Name) VALUES (-20-100000, 'identifiers')
INSERT INTO Roles (Id, Name) VALUES (-21-100000, 'locations')
INSERT INTO Roles (Id, Name) VALUES (-22-100000, 'founder')
INSERT INTO Roles (Id, Name) VALUES (-23-100000, 'institution')
INSERT INTO Roles (Id, Name) VALUES (-24-100000, 'property')
INSERT INTO Roles (Id, Name) VALUES (-25-100000, 'issuer')
INSERT INTO Roles (Id, Name) VALUES (-26-100000, 'vice man')
INSERT INTO Roles (Id, Name) VALUES (-27-100000, 'invalid')
INSERT INTO Roles (Id, Name) VALUES (-28-100000, 'owner')
INSERT INTO Roles (Id, Name) VALUES (-29-100000, 'owned')
INSERT INTO Roles (Id, Name) VALUES (-30-100000, 'proprietary relationship')
INSERT INTO Roles (Id, Name) VALUES (-31-100000, 'merchenary')
INSERT INTO Roles (Id, Name) VALUES (-32-100000, 'part')
INSERT INTO Roles (Id, Name) VALUES (-33-100000, 'whole')
INSERT INTO Roles (Id, Name) VALUES (-34-100000, 'relation')
INSERT INTO Roles (Id, Name) VALUES (-35-100000, 'witness')
INSERT INTO Roles (Id, Name) VALUES (-36-100000, 'unknown')
INSERT INTO Roles (Id, Name) VALUES (-37-100000, 'participant')
INSERT INTO Roles (Id, ParentId, Name) VALUES (-38-100000, -17-100000,'main job')
INSERT INTO Roles (Id, ParentId, Name) VALUES (-39-100000, -34-100000, 'acquitance')
INSERT INTO Roles (Id, ParentId, Name) VALUES (-40-100000, -31-100000, 'position')
INSERT INTO Roles (Id, ParentId, Name) VALUES (-41-100000, NULL, 'resource')
INSERT INTO Roles (Id, ParentId, Name) VALUES (-42-100000, NULL, 'resource contact')
INSERT INTO Roles (Id, ParentId, Name) VALUES (-43-100000, NULL, 'resource proxy')

INSERT INTO Roles (Id, ParentId, Name) VALUES (-44-100000, -20-100000, 'work')
INSERT INTO Roles (Id, ParentId, Name) VALUES (-45-100000, -20-100000, 'home')
INSERT INTO Roles (Id, ParentId, Name) VALUES (-46-100000, -20-100000, 'fax')
INSERT INTO Roles (Id, ParentId, Name) VALUES (-47-100000, -20-100000, 'mobile')
INSERT INTO Roles (Id, ParentId, Name) VALUES (-48-100000, -20-100000, 'preferenced')

GO

SET IDENTITY_INSERT [dbo].[Roles] OFF
GO