﻿SET IDENTITY_INSERT EventTypes ON;
GO
INSERT INTO EventTypes (Id, Name) VALUES (-1, 'юридическое отношение')
INSERT INTO EventTypes (Id, Name) VALUES (-2, 'пользование')
INSERT INTO EventTypes (Id, Name) VALUES (-3, 'регистрация')
INSERT INTO EventTypes (Id, Name) VALUES (-4, 'преимущество')
INSERT INTO EventTypes (Id, Name) VALUES (-5, 'пользовательские события')
INSERT INTO EventTypes (Id, Name) VALUES (-6, 'invalid')
INSERT INTO EventTypes (Id, Name) VALUES (-7, 'распоряжение')
INSERT INTO EventTypes (Id, Name) VALUES (-8, 'имущественная взаимосвязь')
INSERT INTO EventTypes (Id, Name) VALUES (-9, 'найм')
INSERT INTO EventTypes (Id, Name) VALUES (-10, 'отношение') /* default */
INSERT INTO EventTypes (Id, ParentId, Name) VALUES (-11, -5, 'встреча')
INSERT INTO EventTypes (Id, Name) VALUES (-12, 'увольнение')
INSERT INTO EventTypes (Id, Name) VALUES (-13, 'владение')
INSERT INTO EventTypes (Id, Name) VALUES (-14, 'ресурс')
INSERT INTO EventTypes (Id, Name) VALUES (-15, 'знакомый ресурса')
GO
SET IDENTITY_INSERT EventTypes OFF;
GO
SET IDENTITY_INSERT Subjects ON;
GO
INSERT INTO Subjects (Id) VALUES (-1)
GO
SET IDENTITY_INSERT Subjects OFF;
GO


SET IDENTITY_INSERT [dbo].[Tags] ON
INSERT INTO [dbo].[Tags] ([Id], [ParentId], [Name]) VALUES (-1, NULL, 'бизнес')
INSERT INTO [dbo].[Tags] ([Id], [ParentId], [Name]) VALUES (-2, -1, 'торговля')
INSERT INTO [dbo].[Tags] ([Id], [ParentId], [Name]) VALUES (-3, -1, 'производство')
INSERT INTO [dbo].[Tags] ([Id], [ParentId], [Name]) VALUES (-4, -1, 'финансы')
INSERT INTO [dbo].[Tags] ([Id], [ParentId], [Name]) VALUES (-5, NULL, 'компетенции')
INSERT INTO [dbo].[Tags] ([Id], [ParentId], [Name]) VALUES (-6, NULL, 'скидка')
INSERT INTO [dbo].[Tags] ([Id], [ParentId], [Name]) VALUES (-7, NULL, 'увлечение')
INSERT INTO Tags (Id, Name) VALUES (-8, 'идентификаторы')
INSERT INTO Tags (Id, Name) VALUES (-9, 'адреса')
INSERT INTO Tags (Id, Name) VALUES (-10, 'имущество')
INSERT INTO Tags (Id, Name) VALUES (-11, 'оборудование')
INSERT INTO Tags (Id, ParentId, Name) VALUES (-12, -8, 'телефон')
INSERT INTO Tags (Id, ParentId, Name) VALUES (-13, -8, 'google contact')
INSERT INTO Tags (Id, ParentId, Name) VALUES (-14, -8, 'e-mail')
INSERT INTO Tags (Id, ParentId, Name) VALUES (-15, -8, 'skype')
INSERT INTO Tags (Id, ParentId, Name) VALUES (-16, -8, 'сайт')
INSERT INTO Tags (Id, ParentId, Name) VALUES (-17, -8, 'facebook')
INSERT INTO Tags (Id, ParentId, Name) VALUES (-18, -8, 'google')
INSERT INTO Tags (Id, ParentId, Name) VALUES (-19, -8, 'twitter')
INSERT INTO Tags (Id, ParentId, Name) VALUES (-20, -8, 'vkontakte')


INSERT INTO [dbo].[Tags] ([Id], [ParentId], [Name]) VALUES (-500, NULL, 'системные')
INSERT INTO [dbo].[Tags] ([Id], [ParentId], [Name]) VALUES (-501, -500, 'профайл')
INSERT INTO [dbo].[Tags] ([Id], [ParentId], [Name]) VALUES (-502, -500, 'аутентификатор')
INSERT INTO [dbo].[Tags] ([Id], [ParentId], [Name]) VALUES (-503, -500, 'неизвестный исполнитель услуги')


INSERT INTO Tags (Id, Name) VALUES (-600, 'импорт')
INSERT INTO Tags (Id, ParentId, Name) VALUES (-601, -600, 'не подтверждено')
INSERT INTO Tags (Id, ParentId, Name) VALUES (-602, -600, 'принято по почте')
INSERT INTO Tags (Id, ParentId, Name) VALUES (-603, -8, 'личный mailbox')
SET IDENTITY_INSERT [dbo].[Tags] OFF

GO

SET IDENTITY_INSERT [dbo].[Roles] ON
GO

INSERT INTO [dbo].[Roles] ([Id], [ParentId], [Name]) VALUES (-1, NULL, 'работодатель')
INSERT INTO [dbo].[Roles] ([Id], [ParentId], [Name]) VALUES (-2, NULL, 'рабочие отношения')
INSERT INTO [dbo].[Roles] ([Id], [ParentId], [Name]) VALUES (-3, -2, 'начальник')
INSERT INTO [dbo].[Roles] ([Id], [ParentId], [Name]) VALUES (-4, -2, 'сотрудник')
INSERT INTO [dbo].[Roles] ([Id], [ParentId], [Name]) VALUES (-5, -2, 'партнер')
INSERT INTO [dbo].[Roles] ([Id], [ParentId], [Name]) VALUES (-6, NULL, 'родственные отношения')
INSERT INTO [dbo].[Roles] ([Id], [ParentId], [Name]) VALUES (-7, -6, 'брат родной')
INSERT INTO [dbo].[Roles] ([Id], [ParentId], [Name]) VALUES (-8, -6, 'брат двоюродный')
INSERT INTO [dbo].[Roles] ([Id], [ParentId], [Name]) VALUES (-9, -6, 'дочь')
INSERT INTO [dbo].[Roles] ([Id], [ParentId], [Name]) VALUES (-10, -6, 'мать')
INSERT INTO [dbo].[Roles] ([Id], [ParentId], [Name]) VALUES (-11, -6, 'отец')
INSERT INTO [dbo].[Roles] ([Id], [ParentId], [Name]) VALUES (-12, -6, 'сестра родная')
INSERT INTO [dbo].[Roles] ([Id], [ParentId], [Name]) VALUES (-13, -6, 'сестра двоюродная')
INSERT INTO [dbo].[Roles] ([Id], [ParentId], [Name]) VALUES (-14, -6, 'сын')
INSERT INTO [dbo].[Roles] ([Id], [ParentId], [Name]) VALUES (-15, NULL, 'должность')
INSERT INTO [dbo].[Roles] ([Id], [ParentId], [Name]) VALUES (-16, -15, 'генеральный директор')
INSERT INTO Roles (Id, Name) VALUES (-17, 'наниматель')
INSERT INTO Roles (Id, Name) VALUES (-18, 'пользователь')
INSERT INTO Roles (Id, Name) VALUES (-19, 'юридические oтношения')
INSERT INTO Roles (Id, Name) VALUES (-20, 'идентификаторы')
INSERT INTO Roles (Id, Name) VALUES (-21, 'местоположения')
INSERT INTO Roles (Id, Name) VALUES (-22, 'учредитель')
INSERT INTO Roles (Id, Name) VALUES (-23, 'учреждение')
INSERT INTO Roles (Id, Name) VALUES (-24, 'имущество')
INSERT INTO Roles (Id, Name) VALUES (-25, 'источник')
INSERT INTO Roles (Id, Name) VALUES (-26, 'нужный человек')
INSERT INTO Roles (Id, Name) VALUES (-27, 'invalid')
INSERT INTO Roles (Id, Name) VALUES (-28, 'владелец')
INSERT INTO Roles (Id, Name) VALUES (-29, 'во-владении')
INSERT INTO Roles (Id, Name) VALUES (-30, 'имущественная взаимосвязь')
INSERT INTO Roles (Id, Name) VALUES (-31, 'наёмник')
INSERT INTO Roles (Id, Name) VALUES (-32, 'часть')
INSERT INTO Roles (Id, Name) VALUES (-33, 'целое')
INSERT INTO Roles (Id, Name) VALUES (-34, 'oтношение')
INSERT INTO Roles (Id, Name) VALUES (-35, 'свидетель отношения')
INSERT INTO Roles (Id, Name) VALUES (-36, 'unknown')
INSERT INTO Roles (Id, Name) VALUES (-37, 'участник')
INSERT INTO Roles (Id, ParentId, Name) VALUES (-38, -17,'основная работа')
INSERT INTO Roles (Id, ParentId, Name) VALUES (-39, -34, 'знакомый')
INSERT INTO Roles (Id, ParentId, Name) VALUES (-150, -34, 'сестра')
INSERT INTO Roles (Id, ParentId, Name) VALUES (-151, -34, 'брат')
INSERT INTO Roles (Id, ParentId, Name) VALUES (-40, -31, 'должность')
INSERT INTO Roles (Id, ParentId, Name) VALUES (-41, NULL, 'ресурс')
INSERT INTO Roles (Id, ParentId, Name) VALUES (-42, NULL, 'контакт ресурса')
INSERT INTO Roles (Id, ParentId, Name) VALUES (-43, NULL, 'посредник ресурса')

INSERT INTO Roles (Id, ParentId, Name) VALUES (-152, -34, 'родственник')
INSERT INTO Roles (Id, ParentId, Name) VALUES (-153, -34, 'родитель')
INSERT INTO Roles (Id, ParentId, Name) VALUES (-154, -34, 'ребенок')
INSERT INTO Roles (Id, ParentId, Name) VALUES (-155, -34, 'супруг')
INSERT INTO Roles (Id, ParentId, Name) VALUES (-157, -34, 'неприятель')
INSERT INTO Roles (Id, ParentId, Name) VALUES (-158, -34, 'друг')
INSERT INTO Roles (Id, ParentId, Name) VALUES (-159, -34, 'муж')
INSERT INTO Roles (Id, ParentId, Name) VALUES (-160, -34, 'жена')


INSERT INTO Roles (Id, ParentId, Name) VALUES (-44, -20, 'рабочий')
INSERT INTO Roles (Id, ParentId, Name) VALUES (-45, -20, 'домашний')
INSERT INTO Roles (Id, ParentId, Name) VALUES (-46, -20, 'факс')
INSERT INTO Roles (Id, ParentId, Name) VALUES (-47, -20, 'мобильный')
INSERT INTO Roles (Id, ParentId, Name) VALUES (-48, -20, 'предпочитаемый')

INSERT INTO Roles (Id, Name) VALUES (-20, 'идентификаторы')
GO

SET IDENTITY_INSERT [dbo].[Roles] OFF
GO