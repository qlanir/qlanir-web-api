﻿
CREATE function [dbo].[udf_GetEventTagLabel](
    @Id int
) Returns nvarchar(MAX)
AS BEGIN
 
    DECLARE @label nvarchar(MAX)
    DECLARE @parentLabel nvarchar(MAX)
    DECLARE @parentId int
 
    SELECT TOP 1 
          @label = Name,
          @parentId = ParentId
       FROM EventTypes WHERE Id = @Id
 
    IF @parentId IS NOT NULL AND @parentID <> 9
    BEGIN
    EXEC @parentLabel = udf_GetEventTagLabel @parentId;
	SET @label = @parentLabel + '/ ' + @label 
    END
    	
    RETURN @label
END

GO

CREATE function [dbo].[udf_GetEventTagPredecessor](
    @Id int
) Returns nvarchar(MAX)
AS BEGIN
 
	DECLARE @parId int
    DECLARE @res nvarchar(MAX)
    DECLARE @pre nvarchar(MAX)
    
    SET @res = '['+Str(@Id)+']'
    
    SELECT TOP 1 
          @parId = ParentId
       FROM EventTypes WHERE Id = @Id
       
    IF @parId IS NOT NULL 
    BEGIN
    EXEC @pre = [udf_GetEventTagPredecessor] @parId;
	SET @res =  @pre + @res
    END
    	
    RETURN @res
END

GO

CREATE function [dbo].[udf_GetLinkTagLabel](
    @Id int
) Returns nvarchar(MAX)
AS BEGIN
 
    DECLARE @label nvarchar(MAX)
    DECLARE @parentLabel nvarchar(MAX)
    DECLARE @parentId int
 
    SELECT TOP 1 
          @label = Name,
          @parentId = ParentId
       FROM Roles WHERE Id = @Id
 
    IF @parentId IS NOT NULL AND @parentId <> 5 AND @parentID <> 12 AND @parentId <> 50 AND @parentId <> 181 AND @parentId <> 204 AND @parentId <> 201 AND @parentId <> -2 AND @parentId <> 243 AND @parentId <> 3
    BEGIN
    EXEC @parentLabel = udf_GetLinkTagLabel @parentId;
	SET @label = @parentLabel + '/ ' + @label
    END
    	
    RETURN @label
END

GO

CREATE function [dbo].[udf_GetLinkTagPredecessor](
    @Id int
) Returns nvarchar(MAX)
AS BEGIN
 
	DECLARE @parId int
    DECLARE @res nvarchar(MAX)
    DECLARE @pre nvarchar(MAX)
    
    SET @res = '['+Str(@Id)+']'
    
    SELECT TOP 1 
          @parId = ParentId
       FROM Roles WHERE Id = @Id
       
    IF @parId IS NOT NULL 
    BEGIN
    EXEC @pre = [udf_GetLinkTagPredecessor] @parId;
	SET @res =  @pre + @res
    END
    	
    RETURN @res
END

GO

CREATE function [dbo].[udf_GetTagLabel](
    @Id int
) Returns nvarchar(MAX)
AS BEGIN
 
    DECLARE @label nvarchar(MAX)
    DECLARE @parentLabel nvarchar(MAX)
    DECLARE @parentId int
 
    SELECT TOP 1 
          @label = Name,
          @parentId = ParentId
       FROM Tags WHERE Id = @Id
 
    IF @parentId IS NOT NULL AND @parentID <> 33 AND @parentID <> 493 AND @parentID <> 459 AND @parentID <> 600
    BEGIN
    EXEC @parentLabel = udf_GetTagLabel @parentId;
	SET @label = @parentLabel + '/ ' + @label
    END
    	
    RETURN @label
END

GO

CREATE function [dbo].[udf_GetSubjectLabel](
    @id int
) Returns nvarchar(MAX)
AS BEGIN
 
    DECLARE @label nvarchar(MAX)
 
    SELECT TOP 1 
          @label = RTRIM(LTRIM(ISNULL(LastName,'') + ' ' + ISNULL(FirstName,'') + ' ' + ISNULL(MiddleName,'')))
       FROM Persons WHERE Id = @Id

	if (@label = '')
		begin
		    SELECT TOP 1 
				@label = NickName
				FROM Persons WHERE Id = @Id
		end
       
    if (@label is NULL)
		begin
		    SELECT TOP 1 
				  @label = Name
			   FROM Companies WHERE Id = @Id
		end 
	
	if (@label is NULL)
		begin
		    SELECT TOP 1 
				  @label = Value
			   FROM Identifiers WHERE Id = @Id
			if (@label is not NULL)
				begin
					SELECT TOP 1 
							@label = Label + ': ' + @label
						FROM Tags WHERE Id in (SELECT TOP 1 TagId FROM Identifiers WHERE Id = @Id)
				end
		end 
	
	if (@label is NULL)
		begin
			SELECT TOP 1 
				@label = Label
			FROM Tags WHERE Id in (SELECT TOP 1 TagId FROM Assets WHERE Id = @Id)
		end 

	if (@label is NULL)
		begin
			SELECT TOP 1 
				@label = REPLACE(REPLACE(Label,'/',','),'\','/')
			FROM Tags WHERE Id in (SELECT TOP 1 TagId FROM Locations WHERE Id = @Id)
		end 
			
    RETURN @label
END

GO

CREATE function [dbo].[udf_GetTagDescendants](
    @ParentId int
) Returns nvarchar(MAX)
AS BEGIN
 
	DECLARE @descId int
    DECLARE @res nvarchar(MAX)
    DECLARE @desc nvarchar(MAX)
    
    SET @res = '[' + ASCII(@ParentId) + ']'
    
    SELECT TOP 1 
          @descId = Id
       FROM Tags WHERE ParentId = @ParentId
       
    IF @descId IS NOT NULL 
    BEGIN
    EXEC @desc = udf_GetTagDescendants @descId;
	SET @res = @res + @desc
    END
    	
    RETURN @res
END

GO

CREATE function [dbo].[udf_GetTagPredecessor](
    @Id int
) Returns nvarchar(MAX)
AS BEGIN
 
	DECLARE @parId int
    DECLARE @res nvarchar(MAX)
    DECLARE @pre nvarchar(MAX)
    
    SET @res = '['+Str(@Id)+']'
    
    SELECT TOP 1 
          @parId = ParentId
       FROM Tags WHERE Id = @Id
       
    IF @parId IS NOT NULL 
    BEGIN
    EXEC @pre = [udf_GetTagPredecessor] @parId;
	SET @res =  @pre + @res
    END
    	
    RETURN @res
END

GO

CREATE FUNCTION [dbo].[udf_GetMutualRelationCount]
(
	@eventTypeId int,
	@roleId1 int,
	@roleId2 int
)
RETURNS INT
AS
BEGIN
	DECLARE @cnt INT

	SELECT @cnt = COUNT(*)
	FROM Events 
	WHERE 
		TypeId = @eventTypeId AND
		@roleId1 in (SELECT RoleId FROM Links WHERE EventId = Events.Id) AND
		@roleId2 in (SELECT RoleId FROM Links WHERE EventId = Events.Id)

	RETURN @cnt
END

GO

ALTER TABLE Tags
   DROP COLUMN Predecessors

ALTER TABLE Tags
   ADD Predecessors AS dbo.udf_GetTagPredecessor(Id)
GO

ALTER TABLE Tags
   DROP COLUMN Label

ALTER TABLE Tags
   ADD Label AS dbo.udf_GetTagLabel(Id)
GO
---
ALTER TABLE Roles
   DROP COLUMN Predecessors

ALTER TABLE Roles
   ADD Predecessors AS dbo.udf_GetLinkTagPredecessor(Id)
GO

ALTER TABLE Roles
   DROP COLUMN Label

ALTER TABLE Roles
   ADD Label AS dbo.udf_GetLinkTagLabel(Id)
GO

-----

ALTER TABLE EventTypes
   DROP COLUMN Predecessors

ALTER TABLE EventTypes
   ADD Predecessors AS dbo.udf_GetEventTagPredecessor(Id)
GO

ALTER TABLE EventTypes
   DROP COLUMN Label

ALTER TABLE EventTypes
   ADD Label AS dbo.udf_GetEventTagLabel(Id)
GO

------

ALTER TABLE Subjects
   DROP COLUMN Label

ALTER TABLE Subjects
   ADD Label AS dbo.udf_GetSubjectLabel(Id)
GO

----
ALTER TABLE pref_MutualRelations
   DROP COLUMN [Count]

ALTER TABLE pref_MutualRelations
   ADD [Count] AS  dbo.udf_GetMutualRelationCount(EventTypeId,RoleId1,RoleId2)
GO
-----
alter table Invites 
 add constraint df_invites_created
 default getutcdate() for [Created]
 GO


alter table Subjects 
 add constraint df_subject_created
 default getutcdate() for [Created]
 GO

alter table Links
 add constraint df_link_created
 default getutcdate() for [Created]
 GO

alter table [Events]
 add constraint df_event_created
 default getutcdate() for [Created]
 GO

alter table [EventRelations]
add constraint df_eventRelations_created
default getutcdate() for [Created]
GO

drop table [dbo].[V_SubjectLinks]
GO

CREATE view [dbo].[V_SubjectLinks] as 
select l.Id as Id, s.Id as SubjectId, ls.Id as LinkedSubjectId, e.Id as EventId, le.Id as LinkedLinkId from dbo.Subjects s
join dbo.Links l on s.Id = l.SubjectId
join dbo.Events e on e.Id = l.EventId
join dbo.Links le on le.EventId = e.Id and le.Id <> l.Id
join dbo.Subjects ls on ls.Id = le.SubjectId
GO

DROP function [dbo].[udf_GetGlobalId];
GO 

CREATE function [dbo].[udf_GetGlobalId](
    @Id int
) Returns int
AS BEGIN
	DECLARE @res int
	SET @res = @Id
	if @Id <= -100000
		SET @res = @res + 100000
	
	return @res
END

GO

ALTER TABLE Tags
   DROP COLUMN GlobalId

ALTER TABLE Tags
   ADD GlobalId AS dbo.udf_GetGlobalId(Id)
GO

ALTER TABLE Roles
   DROP COLUMN GlobalId

ALTER TABLE Roles
   ADD GlobalId AS dbo.udf_GetGlobalId(Id)
GO

ALTER TABLE EventTypes
   DROP COLUMN GlobalId

ALTER TABLE EventTypes
   ADD GlobalId AS dbo.udf_GetGlobalId(Id)
GO