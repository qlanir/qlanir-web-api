﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Owin;
using qlanir_web_api.Cache;
using qlanir_web_api.DataContext;
using System.Configuration;
using System.IO;
using System.Net;
using System.Net.Http;

namespace qlanir_web_api
{
    using AppFunc = Func<IDictionary<string, object>, Task>;

    public class AddOriginHeaderComponent
    {
        AppFunc _next;

        public AddOriginHeaderComponent(AppFunc next)
        {
            _next = next;
        }

        public async Task Invoke(IDictionary<string, object> env)
        {
            if ((string)env["owin.RequestMethod"] != "OPTIONS")            
            {
                var headers = (IDictionary<string, string[]>)env["owin.ResponseHeaders"];
                headers.Add("Access-Control-Allow-Origin", new[] { "*" });             
            }

            await _next(env);
        }     
    }
}
