﻿using qlanir_web_api.DataContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace qlanir_web_api.Controllers
{
    //[Authorize]
    public class SubjectLinksController : BaseController
    {
        private readonly ISubjectsDataContext _dataContext;

        public SubjectLinksController(ISubjectsDataContext DataContext)
        {
            _dataContext = DataContext;
        }

        public struct LinkedSubject
        {
            public int? Id { get; set; }
            public String Label { get; set; }
        }

        public struct SubjectLinkBody //body for persons, org links
        {
            public Models.Tag Role { get; set; }
            public Models.Tag HostRole { get; set; }
            public LinkedSubject LinkedSubject { get; set; }
        }

        public struct ContactLinkBody //body for contacts
        {
            public int? roleId { get; set; }
            public int typeId { get; set; }
            public string value { get; set; }
        }

        [HttpPost]
        [Route("api/regenMailbox")]   
        public IHttpActionResult RegenMailBox()
        {
            return Invoke(() => SubjectsDataContext.NewMailBox(RequestContext.Principal.Identity.Name));
        }

        #region Attach links to subject

        [HttpPost]
        [Route("api/{resourceType}/{subjectId:int}/organizationsLinks")]
        public IHttpActionResult AttachSubjectOrganization(int subjectId, string resourceType, [FromBody] SubjectLinkBody body)
        {
            return Invoke(() => SubjectsDataContext.AttachLinkedSubject(RequestContext.Principal.Identity.Name, subjectId, body.HostRole, body.Role, body.LinkedSubject.Id, body.LinkedSubject.Label, "organization"));
        }

        [HttpPost]
        [Route("api/{resourceType}/{subjectId:int}/peopleLinks")]
        public IHttpActionResult AttachSubjectPerson(int subjectId, string resourceType, [FromBody] SubjectLinkBody body)
        {
            return Invoke(() => SubjectsDataContext.AttachLinkedSubject(RequestContext.Principal.Identity.Name, subjectId, body.HostRole, body.Role, body.LinkedSubject.Id, body.LinkedSubject.Label, "person"));
        }

        [HttpPost]
        [Route("api/{resourceType}/{subjectId:int}/contactsLinks")]
        public IHttpActionResult AttachSubjectContact(int subjectId, [FromBody]ContactLinkBody body)
        {
            return Invoke(() => SubjectsDataContext.AttachContact(RequestContext.Principal.Identity.Name, subjectId, body.roleId, body.typeId, body.value));
        }

        //[Route("api/{resourceType}/{subjectId:int}/services")]
        //[HttpPost]
        //public IHttpActionResult AttachSubjectSrevice(int subjectId, [FromBody]string label)
        //{
        //    return Invoke(() => SubjectsDataContext.AttachLinkedService(RequestContext.Principal.Identity.Name, subjectId, label));
        //}

        #endregion

        #region Update links to subject

        [Route("api/{resourceType}/{subjectId:int}/contactsLinks/{contactLinkId:int}")]
        [HttpPut]
        public IHttpActionResult UpdateLinkedContact(int subjectId, int contactLinkId, [FromBody]ContactLinkBody body)
        {
            return Invoke(() => SubjectsDataContext.UpdateContactLink(RequestContext.Principal.Identity.Name, contactLinkId, subjectId, body.typeId, body.value, body.roleId));

        }

        [Route("api/{resourceType}/{subjectId:int}/peopleLinks/{linkId:int}")]
        [HttpPut]
        public IHttpActionResult UpdateLinkedPerson(int subjectId, int linkId, [FromBody]SubjectLinkBody body)
        {
            return Invoke(() => SubjectsDataContext.UpdateLinkedSubject(RequestContext.Principal.Identity.Name, subjectId, body.HostRole, body.Role, body.LinkedSubject.Id, body.LinkedSubject.Label, "person", linkId));
        }

        [Route("api/{resourceType}/{subjectId:int}/organizationsLinks/{linkId:int}")]
        [HttpPut]
        public IHttpActionResult UpdateLinkedOrg(int subjectId, int linkId, [FromBody]SubjectLinkBody body)
        {
            return Invoke(() => SubjectsDataContext.UpdateLinkedSubject(RequestContext.Principal.Identity.Name, subjectId, body.HostRole, body.Role, body.LinkedSubject.Id, body.LinkedSubject.Label, "organization", linkId));
        }
        #endregion


        #region Delete links
        [Route("api/people/{subjectId:int}/{linkType}/{contactLinkId:int}")]
        [HttpDelete]
        public IHttpActionResult DeletePersonLink(int subjectId, int contactLinkId)
        {
            return Invoke(() => _dataContext.DeleteSubjectLink<Models.Person>(RequestContext.Principal.Identity.Name, contactLinkId, subjectId));

        }

        [Route("api/organizations/{subjectId:int}/{linkType}/{linkId:int}")]
        [HttpDelete]
        public IHttpActionResult DeleteOrgLink(int subjectId, int linkId)
        {
            return Invoke(() => _dataContext.DeleteSubjectLink<Models.Organization>(RequestContext.Principal.Identity.Name, linkId, subjectId));

        }

        [Route("api/services/{subjectId:int}/{linkType}/{linkId:int}")]
        [HttpDelete]
        public IHttpActionResult DeleteServiceLink(int subjectId, int linkId)
        {
            return Invoke(() => _dataContext.DeleteSubjectLink<Models.Service>(RequestContext.Principal.Identity.Name, linkId, subjectId));

        }
        #endregion

    }

}
