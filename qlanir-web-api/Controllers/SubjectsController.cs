﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using qlanir_web_api.Models;
using qlanir_web_api.DataContext;
using System.Data;
using System.Web.Http.Results;
using System.Data.Entity.Core;

namespace qlanir_web_api.Controllers
{
    public class SubjectsController : BaseController 
    {
        private readonly ISubjectsDataContext _dataContext;

        public SubjectsController(ISubjectsDataContext DataContext)
        {
            _dataContext = DataContext;
        }

        [Route("api/people/{id:int}")]        
        public IHttpActionResult GetPerson(int id)        
        {
            return Get<Models.Person>(id);
        }

        [Route("api/organizations/{id:int}")]
        public IHttpActionResult GetOrganization(int id)
        {
            return Get<Models.Organization>(id);
        }

        private IHttpActionResult Get<T>(int Id) where T : Models.Subject, new()
        {
            return Invoke(() => _dataContext.Get<T>(Id, RequestContext.Principal.Identity.Name));
        }

        [HttpPost]
        [Route("api/people")]
        public IHttpActionResult PostPerson([FromBody] Models.Person Subject)
        {
            return UpdateSubject(Subject, null);
        }

        [HttpPost]
        [Route("api/people/import")]
        public IHttpActionResult ImportPerson([FromBody] Models.Person Subject, [FromUri] String Email)
        {
            return Invoke(() => _dataContext.Import(Email, RequestContext.Principal.Identity.Name, Subject));
        }

        [HttpPut]
        [Route("api/people/convert")]
        public IHttpActionResult convertPerson([FromBody] Models.Person Subject)
        {
            return Invoke(() => (Models.Organization)_dataContext.Convert(RequestContext.Principal.Identity.Name, Subject));
        }

        [HttpPut]
        [Route("api/people/{id:int}")]
        public IHttpActionResult PutPerson([FromBody] Models.Person Subject, int id)
        {
            return UpdateSubject(Subject, id);
        }

        [HttpPost]
        [Route("api/organizations")]
        public IHttpActionResult PostOrganization([FromBody] Models.Organization Subject)
        {
            return UpdateSubject(Subject, null);
        }

        [HttpPut]
        [Route("api/organizations/{id:int}")]
        public IHttpActionResult PutOrganization([FromBody] Models.Organization Subject, int id)
        {
            return UpdateSubject(Subject, id);
        }

        [HttpPost]
        [Route("api/contacts")]
        public IHttpActionResult PostContact([FromBody] Models.Contact Subject)
        {
            return UpdateSubject(Subject, null);                
        }

        [HttpPut]
        [Route("api/contacts/{id:int}")]
        public IHttpActionResult PutContact([FromBody] Models.Contact Subject, int id)
        {
            return UpdateSubject(Subject, id);
        }     

        private IHttpActionResult UpdateSubject<T>(T Subject, int? Id) where T : Models.Subject, new()
        {
            return Invoke(() => _dataContext.Update(Id, RequestContext.Principal.Identity.Name, Subject));
        }


        [HttpDelete]
        [Route("api/people/{id:int}")]
        public IHttpActionResult DeletePerson(int id)
        {
            return Delete<Models.Person>(id);
        }

        [HttpDelete]
        [Route("api/organizations/{id:int}")]
        public IHttpActionResult DeleteOrganization(int id)
        {
            return Delete<Models.Organization>(id);
        }
        
        public IHttpActionResult Delete<T>(int id) where T : Models.Subject, new()
        {
            return Invoke(() => _dataContext.Delete<T>(id, RequestContext.Principal.Identity.Name));
        }

    }
}
