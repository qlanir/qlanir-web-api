﻿using qlanir_web_api.DataContext;
using qlanir_web_api.DataContext.ElasticDataContext;
using qlanir_web_api.DataContext.GraphDataContext;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace qlanir_web_api.Controllers
{
    //[Authorize(Roles="admin")]
    public class AdminController : ApiController
    {
        [Route("api/admin/index")]
        [HttpGet]
        [AllowAnonymous]
        public IHttpActionResult Index()
        {
            using (var graph = new GraphDataContext())
            {
                graph.IndexAll();
            }

            using (var elastic = new ElasticDataContext())
            {
                elastic.IndexAll();
            }
        
            return Ok(new { ok = true });
        }

        [Route("api/admin/clean")]
        [HttpGet]
        [AllowAnonymous]
        public IHttpActionResult Clean()
        {
            SubjectsDataContext.CleanAll();

            using (var graph = new GraphDataContext())
            {
                graph.IndexAll();
            }

            using (var elastic = new ElasticDataContext())
            {
                elastic.IndexAll();
            }

            return Ok(new { ok = true });
        }

        [Route("api/admin/cleanDbAndCreateProfile")]
        [HttpGet]
        [AllowAnonymous]
        public IHttpActionResult CleanDbAndCreateProfile()
        {
            UtilsDataContext.CleanDbAndCreateProfile();

            return Ok(new { ok = true });
        }


        [Route("api/version")]
        [HttpGet]
        [AllowAnonymous]
        public IHttpActionResult GetVersion()
        {
            //test 1
            return Ok(Assembly.GetEntryAssembly().GetName().Version); //ConfigurationManager.AppSettings.Get("SERVER_VERSION") + " " + ConfigurationManager.AppSettings.Get("SERVER_ENV"));
        }

    }
}
