﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;

namespace qlanir_web_api.Controllers
{
    [Authorize]
    public class BaseController : ApiController
    {
        protected IHttpActionResult Invoke(Action Action)
        {
            
            try
            {
                Action();

                return Ok(new CustomOkResult());
            }
            catch (ObjectNotFoundException ex)
            {
                return GetHttpResultFromException(ex);
            }
            catch (ArgumentException ex)
            {
                return GetHttpResultFromException(ex);
            }
#if !DEBUG
            catch(Exception ex)
            {
                return GetHttpResultFromException(ex);
            }
#endif

        }

        protected IHttpActionResult Invoke(Func<object> Action) 
        {
            /*
            var res = Action();

            return Ok(res);
            */
            try
            {
                var res = Action();

                return Ok(res);
            }
            catch (ObjectNotFoundException ex)
            {
                return GetHttpResultFromException(ex);
            }
            catch (ArgumentException ex)
            {
                return GetHttpResultFromException(ex);
            }
#if !DEBUG
            catch(Exception ex)
            {
                return GetHttpResultFromException(ex);
            }
#endif
        }

        private IHttpActionResult GetHttpResultFromException(Exception Exception)
        {
            if (Exception is ObjectNotFoundException)
            {
                return new NotFoundResult(this);
            }
            else if (Exception is ArgumentException)
            {
                return new BadRequestResult(this);
            }

            return new InternalServerErrorResult(this);

        }
    }

    public class CustomOkResult
    {
        public bool ok = true;
    }
}

