﻿using qlanir_web_api.DataContext;
using qlanir_web_api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace qlanir_web_api.Controllers
{
    public class ServicesController : BaseController
    {
        private readonly ISubjectsDataContext _dataContext;

        public ServicesController(ISubjectsDataContext DataContext)
        {
            _dataContext = DataContext;
        }

        //BKG:0.0.12 
        [Route("api/services/{id:int}")]
        public IHttpActionResult GetServices(int id) //id - id типа услуги (Tag.Id)
        {
            return Invoke(() => _dataContext.Get<Models.Service>(id, RequestContext.Principal.Identity.Name));
        }

        [Route("api/offers/{id:int}")]
        public IHttpActionResult GetOffer(int id)
        {
            return Invoke(() => _dataContext.Get<Models.Offer>(id, RequestContext.Principal.Identity.Name));
        }

        [HttpPost]
        [Route("api/offers")]
        public IHttpActionResult PostOffer([FromBody] PostOffer PostOffer)
        {
            return UpdateOffer(PostOffer);
        }

        [HttpPut]
        [Route("api/offers/{id:int}")]
        public IHttpActionResult PutOffer(int id, [FromBody] PostOffer PostOffer)
        {
            return UpdateOffer(PostOffer, id);
        }

        [HttpDelete]
        [Route("api/offers/{id:int}")]
        public IHttpActionResult DeleteOffer(int id)
        {
            return Invoke(() => _dataContext.Delete<Models.Offer>(id, RequestContext.Principal.Identity.Name));
        }

        private IHttpActionResult UpdateOffer(PostOffer PostOffer, int? OfferId = null)
        {
            //convert to internal structure
            Models.ISubjectWithLinks subject = null;
            if (PostOffer.LinkedSubject != null && !String.IsNullOrEmpty(PostOffer.LinkedSubject.Label)) //если есть исполнитель
            {
                if (PostOffer.LinkedSubject.Type == "person")
                {
                    if (PostOffer.LinkedSubject.Id != 0)
                    {
                        subject = _dataContext.Get<Person>(PostOffer.LinkedSubject.Id, RequestContext.Principal.Identity.Name);
                        ((Person)subject).NameParts = null;
                    }
                    else
                        subject = SubjectUtils.CreateSubject<Person>();
                }
                else if (PostOffer.LinkedSubject.Type == "organization")
                {
                    subject = SubjectUtils.CreateSubject<Organization>();
                }
                else
                {
                    return BadRequest("LinkedSubject.Type could be only 'person' or 'organization'");
                }

                subject.Id = PostOffer.LinkedSubject.Id;
                subject.Label = PostOffer.LinkedSubject.Label;

            }
            else if (PostOffer.Proxy != null && !String.IsNullOrEmpty(PostOffer.Proxy.Label)) //если исполнитель неназван, но есть посредник
            {
                if (PostOffer.Proxy.Id != 0)
                {
                    subject = _dataContext.Get<Person>(PostOffer.Proxy.Id, RequestContext.Principal.Identity.Name);
                    ((Person)subject).NameParts = null;
                }
                else
                    subject = SubjectUtils.CreateSubject<Person>();

                subject.Id = PostOffer.Proxy.Id;
                subject.Label = PostOffer.Proxy.Label;
            }
            else
                return BadRequest("LinkedSubject or Proxy could not be empty");

            subject.Offers = new OfferLink[] { 
                new OfferFull {
                    Tag = new Tag { Label = PostOffer.Type.Label, Id = PostOffer.Type.Id },
                    Subject = new OfferBase { Label = PostOffer.Description, Id = OfferId == null ? 0 : (int)OfferId },
                    HostSubject = PostOffer.LinkedSubject!=null && !String.IsNullOrEmpty(PostOffer.LinkedSubject.Label) ? new SubjectBase{ Id = subject.Id, Label = subject.Label, Type = subject.Type} : null,
                    Proxy = PostOffer.Proxy == null ? null : new PersonBase { Label = PostOffer.Proxy.Label, Id = PostOffer.Proxy.Id }
                }
            };
            subject.Tags = null; //QL-122
            subject.Organizations = null;
            subject.People = null;
            subject.Contacts = null;

            return Invoke(() =>
            {
                ISubjectWithLinks subj = null;

                if (subject is Models.Person)
                    subj = _dataContext.Update(subject.Id == 0 ? null : (int?)subject.Id, RequestContext.Principal.Identity.Name, (Models.Person)subject);
                else
                    subj = _dataContext.Update(subject.Id == 0 ? null : (int?)subject.Id, RequestContext.Principal.Identity.Name, (Models.Organization)subject);

                var offerId = OfferId != null ? (int)OfferId : (int)subj.Offers.Where(p => p.Tag.Label == PostOffer.Type.Label).OrderByDescending(p => p.Id).First().Id;

                return _dataContext.Get<Models.Offer>(offerId, RequestContext.Principal.Identity.Name);
            });

        }



    }
}
