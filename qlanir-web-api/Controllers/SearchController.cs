﻿using qlanir_web_api.DataContext;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;

namespace qlanir_web_api.Controllers
{
    public class SearchController : BaseController
    {
        private readonly ISearchSubjectsDataContext<Models.SearchResult> _dataContext;

        public SearchController(ISearchSubjectsDataContext<Models.SearchResult> DataContext)
        {
            _dataContext = DataContext;
        }

        [Route("api/search")]
        [HttpGet]
        public IHttpActionResult Search([FromUri]DataContextFilter Filter)
        {
            return Invoke(() => _dataContext.Search(RequestContext.Principal.Identity.Name, Filter));
        }

        [Route("api/search/names")]
        [HttpGet]
        public IHttpActionResult SearchNames([FromUri]DataContextFilter Filter)
        {

            return Invoke(() => 
            {
                using (var elastic = new qlanir_web_api.DataContext.ElasticDataContext.ElasticDataContext())
                {
                    var types = Filter.Type.Split('&');
                  
                    IEnumerable<Models.Tag> result = new Models.Tag[0];

                    foreach (var type in types)
                    {
                        result = result.Union(elastic.SearchNames(RequestContext.Principal.Identity.Name, new DataContextFilter { Type = type, IncludeLinkedTags = Filter.IncludeLinkedTags, Term = Filter.Term }));
                    }

                    return result.ToArray();
                }
            });
        }

    }
}
