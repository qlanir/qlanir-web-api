﻿using qlanir_web_api.DataContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace qlanir_web_api.Controllers
{
    public class TagsController : BaseController 
    {
        [HttpPost]
        [Route("api/subjects/{subjectId:int}/tags/{label}")]
        public IHttpActionResult UpdateLinkedTag(int subjectId, string label)
        {
            return Invoke(() => TagsDataContext.UpdateLinkedTag(RequestContext.Principal.Identity.Name, subjectId, label));
        }

        [HttpDelete]
        [Route("api/subjects/{subjectId:int}/tags/{label}")]
        public IHttpActionResult RemoveLinkedTag(int subjectId, string label)
        {
            return Invoke(() => TagsDataContext.RemoveLinkedTag(RequestContext.Principal.Identity.Name, subjectId, label));
        }

        [HttpPost]
        [Route("api/tags/{label}")]
        public IHttpActionResult CreateTag(string label)
        {
            return Invoke(() => TagsDataContext.CreateTag(RequestContext.Principal.Identity.Name, label));
        }

        [HttpDelete]
        [Route("api/tags/{id:int}")]
        public IHttpActionResult RemoveTag(int id)
        {
            return Invoke(() => TagsDataContext.RemoveTag(RequestContext.Principal.Identity.Name, id));
        }

        [HttpGet]
        [Route("api/tags/{id:int}/linksCount")]
        public IHttpActionResult GetTagLinksCount(int id)
        {
            return Invoke(() => TagsDataContext.GetTagLinksCount(RequestContext.Principal.Identity.Name, id));
        }
    }
}
