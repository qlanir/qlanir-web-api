﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using qlanir_web_api.Models;
using qlanir_web_api.DataContext;
using System.Data;
using System.Web.Http.Results;
using System.Data.Entity.Core;

namespace qlanir_web_api.Controllers
{
    public class PinsController : BaseController
    {
        private readonly IPinsDataContext _dataContext;

        public PinsController(IPinsDataContext DataContext)
        {
            _dataContext = DataContext;
        }

        [Route("api/pins")]        
        [HttpGet]
        public IHttpActionResult GetPins()        
        {
            return Invoke(() => _dataContext.GetPins(RequestContext.Principal.Identity.Name));
        }

        [Route("api/pins/{id:int}")]
        [HttpGet]
        public IHttpActionResult GetPin(int id)
        {
            return Invoke(() => _dataContext.GetPin(RequestContext.Principal.Identity.Name, id));
        }        

        [Route("api/pins")]
        [HttpPost]
        public IHttpActionResult AddPin(CreatePin pin)
        {
            return Invoke(() => _dataContext.AddPin(RequestContext.Principal.Identity.Name, pin));

        }

        [Route("api/pins/{id:int}")]
        [HttpPut]
        public IHttpActionResult UpdatePin(int id, [FromBody] CreatePin Pin)
        {
            return Invoke(() => _dataContext.UpdatePin(RequestContext.Principal.Identity.Name, id, Pin.Description));
        }        

        [Route("api/pins/{id:int}")]
        [HttpDelete]
        public IHttpActionResult RemovePin(int id)
        {
            return Invoke(() => _dataContext.RemovePin(RequestContext.Principal.Identity.Name, id));
        }
    }
}
