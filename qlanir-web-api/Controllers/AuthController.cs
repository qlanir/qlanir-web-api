﻿using Newtonsoft.Json;
using qlanir_web_api.DataContext;
using qlanir_web_api.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace qlanir_web_api.Controllers
{
    public class AuthController : ApiController
    {
        private readonly IProfileDataContext _dataContext;

        public AuthController(IProfileDataContext DataContext)
        {
            _dataContext = DataContext;
        }

        [HttpGet]
        [Route("api/user")]
        public  IHttpActionResult GetUser()
        {
            var auth = Request.Headers.GetValues("authorization").FirstOrDefault();

            try
            {
                var profile = JWTAuth.Authorize(auth, null);

                TagTypes identifyerType = TagTypes.Ident;

                switch (profile.provider)
                {
                    case "password":
                        identifyerType = TagTypes.Email;
                        break;
                    case "twitter":
                        identifyerType = TagTypes.Twitter;
                        break;
                    case "vk":
                        identifyerType = TagTypes.Vkontakte;
                        break;
                    case "facebook":
                        identifyerType = TagTypes.Facebook;
                        break;
                    case "google":
                        identifyerType = TagTypes.Google;
                        break;
                    default:
                        return InternalServerError(new Exception("Prfile provider " + profile.provider + " is not known"));
                        
                }

                var newUserId = _dataContext.CreateUserIfNotExist(profile.name, profile.identifyer, identifyerType);

                return Ok(new UserResponse { User = profile.name, IsNew = newUserId == -1 ? false : true }); 
            }
            catch (JWT.SignatureVerificationException)
            {
                return Unauthorized();
            }
        }
    }
}
