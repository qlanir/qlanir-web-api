﻿using qlanir_web_api.DataContext;
using qlanir_web_api.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;

namespace qlanir_web_api.Controllers
{

    public class GraphController : BaseController
    {
       
        [HttpGet]
        [Route("api/graph/people/{id:int?}")]        
        public IHttpActionResult GetPeopleGraph(int? id = null, int step = 1)
        {
            return Get(id, "person", step);
        }

        [HttpGet]
        [Route("api/graph/tags/{id:int?}")]
        public IHttpActionResult GetTagsGraph(int? id = null, int step = 1)
        {
            return Get(id, "tag", step);
        }

        [HttpGet]
        [Route("api/graph/organizations/{id:int?}")]
        public IHttpActionResult GetOrgGraph(int? id = null, int step = 1)
        {
            return Get(id, "organization", step);
        }

        [HttpGet]
        [Route("api/graph/offers/{id:int?}")]
        public IHttpActionResult GetOfferGraph(int? id = null, int step = 1)
        {
            return Get(id, "offer", step);
        }

        [HttpGet]
        [Route("api/graph/paths/{id:int}")]
        public IHttpActionResult GetPaths(int id)
        {
            var profileId = DataContext.SubjectsDataContext.GetUserId(RequestContext.Principal.Identity.Name);

            using (var graph = new DataContext.GraphDataContext.GraphDataContext())
            {
                return Ok(graph.GetPaths(profileId, id));
            }
        }


        private IHttpActionResult Get(int? Id, string Type, int Step)
        {
            return Invoke(() =>{
                var owner = DataContext.SubjectsDataContext.GetUserId(RequestContext.Principal.Identity.Name);

                if (Id == null)
                    Id = owner;

                using (var graph = new DataContext.GraphDataContext.GraphDataContext())
                {
                    return graph.GetNodeGraph(owner, (int)Id, Type, Step);
                }
            });
        }

    }
}
