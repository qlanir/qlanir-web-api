﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qlanir_web_api.DataContext
{
    public class DataContextFilter
    {
        public string Term { get; set; }

        public string Type { get; set; }

        public bool IncludeLinkedTags { get; set; }
    }
}
