﻿using qlanir_web_api.Models;
using QlanirEntities;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qlanir_web_api.DataContext
{
    partial class SubjectsDataContext
    {
        public static void AttachContact(string User, int SubjectId, int? RoleId, int TypeId, string ContactValue)
        {
            if (TypeId == 0 || string.IsNullOrEmpty(ContactValue))
            {
                throw new ArgumentException("Both ContactLinkTagId and ContactValue should be defined");
            }

            using (var ctx = new QlanirEntities.QlanirEntitySet())
            {
                var owner = (int)GetUserId(User, ctx);

                var subjectEnty = ctx.Subjects.FirstOrDefault(p => p.Id == SubjectId && (p.OwnerId == owner || p.Id == owner));

                var identTagEnty = ctx.Tags.FirstOrDefault(p => p.Id == (int)TypeId && p.OwnerId == null);

                if (subjectEnty == null || identTagEnty == null)
                {
                    throw new ObjectNotFoundException();
                }

                var identEntry = new QlanirEntities.Identifier { Value = ContactValue, Tag = identTagEnty, OwnerId = owner };

                var evt = new Event { OwnerId = owner, TypeId = (int)qlanir_web_api.Models.EventTypes.Owner };
                var link1 = new Link { OwnerId = owner, RoleId = (int)qlanir_web_api.Models.RoleTypes.Owner, Subject = subjectEnty, Event = evt };

                var contactRoleId = RoleId == null ? (int)qlanir_web_api.Models.RoleTypes.Identifyers : RoleId;
                var link2 = new Link { OwnerId = owner, RoleId = (int)contactRoleId, Subject = identEntry, Event = evt };

                ctx.Subjects.AddObject(identEntry);
                ctx.Events.AddObject(evt);
                ctx.Links.AddObject(link1);
                ctx.Links.AddObject(link2);

                ctx.SaveChanges();

                ctx.Refresh(System.Data.Entity.Core.Objects.RefreshMode.StoreWins, subjectEnty);

                Index(subjectEnty, true);

            }

        }

        public static ContactLink NewMailBox(string User)
        {
            using (var ctx = new QlanirEntities.QlanirEntitySet())
            {
                var owner = (int)GetUserId(User, ctx);

                var ownerEnty = ctx.Subjects.FirstOrDefault(p => p.Id == owner);

                var identTagEnty = ctx.Tags.FirstOrDefault(p => p.Id == (int)TagTypes.SystemEmail);

                var identEntry = ctx.Subjects.OfType<QlanirEntities.Identifier>().FirstOrDefault(f => f.TagId == (int)qlanir_web_api.Models.TagTypes.SystemEmail && f.OwnerId == owner);

                if (identEntry == null)
                {
                    identEntry = new QlanirEntities.Identifier { Value = GenerateMailBoxForImport(ctx), Tag = identTagEnty, OwnerId = owner };

                    var evt = new Event { OwnerId = owner, TypeId = (int)qlanir_web_api.Models.EventTypes.Owner };
                    var link1 = new Link { OwnerId = owner, RoleId = (int)qlanir_web_api.Models.RoleTypes.Owner, Subject = ownerEnty, Event = evt };

                    var contactRoleId = (int)qlanir_web_api.Models.RoleTypes.Identifyers;
                    var link2 = new Link { OwnerId = owner, RoleId = (int)contactRoleId, Subject = identEntry, Event = evt };

                    ctx.Subjects.AddObject(identEntry);
                    ctx.Events.AddObject(evt);
                    ctx.Links.AddObject(link1);
                    ctx.Links.AddObject(link2);
                }
                else
                    identEntry.Value = GenerateMailBoxForImport(ctx);

                ctx.SaveChanges();

                ctx.Refresh(System.Data.Entity.Core.Objects.RefreshMode.StoreWins, ownerEnty);

                Index(ownerEnty, true);

                return new ContactLink
                {
                    Id = identEntry.Links.FirstOrDefault().Id,
                    Subject = new Contact
                    {
                        Id = identEntry.Id,
                        ContactType = new Models.Tag { Id = (int)TagTypes.SystemEmail },
                        Label = identEntry.Value,
                        Owner = owner
                    }
                };
            }
        }

        private static string GenerateMailBoxForImport(QlanirEntities.QlanirEntitySet ctx)
        {
            var result = "";
            var words = "0123456789qwertyuiopasdfghjklzxcvbnm";
            var max_position = words.Length - 1;
            var rnd = new Random();
            for (var i = 0; i < 5; i++)
            {
                int position = rnd.Next(0, max_position);
                result = result + words.Substring(position, 1);
            }

            var _email = String.Format("{0}@{1}", result, System.Configuration.ConfigurationManager.AppSettings["IMPORT_EMAIL_PREFIX"]);

            if (ctx.Subjects.OfType<QlanirEntities.Identifier>().Any(a => a.Value == _email))
                return GenerateMailBoxForImport(ctx);
            else
                return _email;
        }



    }
}
