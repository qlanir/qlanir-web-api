﻿using QlanirEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qlanir_web_api.DataContext
{
    partial class SubjectsDataContext
    {
        internal static int CreatePerson(QlanirEntities.QlanirEntitySet ctx, Models.Person Person, string UserIdentifyer)
        {
            //1. create person itself
            var person = new Person { FirstName = Person.Label };

            //2. create tags
            foreach (var tag in Person.Tags)
            {
                person.TagMaps.Add(new TagMap { Subject = person, TagId = tag.Id });
            }

            //3. create contacts
            foreach (var contactLink in Person.Contacts)
            {
                var ctt = new Identifier { Owner = person,  TagId = ((qlanir_web_api.Models.Contact)contactLink.Subject).ContactType.Id, Value = contactLink.Subject.Label };
                var link1 = new Link { Owner = person, RoleId = (int)qlanir_web_api.Models.RoleTypes.Identifyers, Subject = ctt };
                var link2 = new Link { Owner = person, RoleId = (int)qlanir_web_api.Models.RoleTypes.Owner, Subject = person };
                var evt = new Event { Owner = person, TypeId = (int)qlanir_web_api.Models.EventTypes.Owner };
                evt.Links.Add(link1);
                evt.Links.Add(link2);

                ctx.Events.AddObject(evt);

            }

            ctx.Users.AddObject(new User { Name = UserIdentifyer, Subject = person });

            ctx.SaveChanges();

            ctx.Refresh(System.Data.Entity.Core.Objects.RefreshMode.StoreWins, person);

            Index(person);

            SubjectsDataContext.NewMailBox(UserIdentifyer);

            return person.Id;
        }
    }
}
