﻿using QlanirEntities;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qlanir_web_api.DataContext
{
    partial class SubjectsDataContext
    {
        private IEnumerable<Subject> UpdateLinkedOffers<T>(QlanirEntities.QlanirEntitySet ctx, QlanirEntities.Subject Enty, T Model) where T : Models.ISubjectWithLinks
        {
            List<Subject> res = new List<Subject>();

            foreach (qlanir_web_api.Models.OfferLink offer in Model.Offers)
            {

                QlanirEntities.Subject hostSubject = Enty; //исполнитель услуги
                int owner = Model.Owner;

                if (Model.Id != 0 && offer.Proxy != null && offer.Proxy.Id == Model.Id)
                    if (offer is qlanir_web_api.Models.OfferFull && ((qlanir_web_api.Models.OfferFull)offer).HostSubject == null) //при сохранении услуги с неизвестным исполнителем
                        hostSubject = null;
                    else
                        continue; //при сохрании карточки посредника услуги - не апдейтится линк (пока не требуется)

                QlanirEntities.Tag evtTagEnty = ctx.Tags.FirstOrDefault(p => p.Label.ToLower() == offer.Tag.Label.ToLower() && (p.OwnerId == owner || p.Id < 0));

                if (evtTagEnty == null)
                {
                    evtTagEnty = new QlanirEntities.Tag { Name = offer.Tag.Label, OwnerId = owner };
                    ctx.AddToTags(evtTagEnty);
                }
                //else может быть привязано несколько услуг одного типа
                //{
                //    //Check if this tag already linked with same subject (via event)
                //    if (ctx.Events.Any(p => p.Tags.Any(s => s.Id == evtTagEnty.Id) && p.Links.Any(s => s.SubjectId == Enty.Id)))
                //        throw new ArgumentException("Same service (event's tag with the same label) could be linked to subject only once");
                //}



                // new QlanirEntities.Tag { OwnerId = owner };


                QlanirEntities.Event evtEnty = null;
                if (offer.Subject != null && offer.Subject.Id != 0)
                    evtEnty = ctx.Events.FirstOrDefault(p => p.Id == offer.Subject.Id);

                if (evtEnty == null) //new offer
                {
                    evtEnty = new QlanirEntities.Event
                    {
                        OwnerId = owner,
                        TypeId = (int)Models.EventTypes.Resource,
                        Description = offer.Subject == null ? null : offer.Subject.Label
                    };
                    evtEnty.Tags.Add(evtTagEnty);
                    ctx.AddToEvents(evtEnty);

                    if (hostSubject != null) //исполнитель известен
                    {
                        var linkEnty = new QlanirEntities.Link { OwnerId = owner, Event = evtEnty, RoleId = (int)Models.RoleTypes.ReourceOwner, Subject = hostSubject };
                        ctx.AddToLinks(linkEnty);
                    }
                }
                else //for offer update
                {
                    if (!evtEnty.Tags.Any(p => p.Label.ToLower() == offer.Tag.Label.ToLower()))
                    {
                        evtEnty.Tags.Clear();
                        evtEnty.Tags.Add(evtTagEnty);
                    }
                    evtEnty.Description = offer.Subject.Label;

                    var hostLinkEnty = evtEnty.Links.FirstOrDefault(p => p.RoleId == (int)Models.RoleTypes.ReourceOwner);
                    if (hostLinkEnty != null)
                    {
                        if (hostSubject != null)
                        {
                            if (hostLinkEnty.SubjectId != hostSubject.Id) //исполнитель изменен
                            {
                                var oldHost = hostLinkEnty.Subject;
                                hostLinkEnty.Subject = hostSubject;
                                res.Add(oldHost);
                            }
                        } //исполнитель стал неизвестен
                        else
                        {
                            res.Add(hostLinkEnty.Subject);
                            evtEnty.Links.Remove(hostLinkEnty);
                            ctx.Links.DeleteObject(hostLinkEnty);
                        }
                    }
                    else if (hostSubject != null) //исполнитель стал известен
                    {
                        var linkEnty = new QlanirEntities.Link { OwnerId = owner, Event = evtEnty, RoleId = (int)Models.RoleTypes.ReourceOwner, Subject = hostSubject };
                        ctx.AddToLinks(linkEnty);
                    }

                }

                if (offer.Proxy != null && (offer.Proxy.Label != null || offer.Proxy.Id != 0))
                {
                    Person proxyPerson = null;

                    if (offer.Proxy.Id != 0)
                    {
                        proxyPerson = ctx.Subjects.OfType<Person>().FirstOrDefault(p => p.Id == offer.Proxy.Id);

                        if (proxyPerson == null)
                            throw new ArgumentException("Proxy person not found");
                    }
                    else
                    {
                        var names = offer.Proxy.Label.Split(' ');
                        var firstName = names.Length == 1 ? names[0] : names[1];
                        var lastName = names.Length > 1 ? names[0] : null;
                        var middleName = names.Length > 2 ? names[2] : null;

                        proxyPerson = new Person { FirstName = firstName, LastName = lastName, MiddleName = middleName, OwnerId = owner };
                        ctx.Subjects.AddObject(proxyPerson);
                    }

                    QlanirEntities.Link proxyLinkEnty = null;

                    if (evtEnty.Id != 0) //for offer update
                    {
                        proxyLinkEnty = evtEnty.Links.FirstOrDefault(p => p.RoleId == (int)Models.RoleTypes.ResourceProxy);
                        if (proxyLinkEnty != null && proxyLinkEnty.SubjectId != proxyPerson.Id)
                        {
                            var oldProxy = proxyLinkEnty.Subject;
                            proxyLinkEnty.Subject = proxyPerson;

                            res.Add(oldProxy);
                        }
                    }

                    if (proxyLinkEnty == null)
                    {
                        proxyLinkEnty = new QlanirEntities.Link
                        {
                            OwnerId = owner,
                            Event = evtEnty,
                            RoleId = (int)Models.RoleTypes.ResourceProxy,
                            Subject = proxyPerson
                        };

                        ctx.AddToLinks(proxyLinkEnty);
                    }
                    if (hostSubject != null)
                        res.Add(proxyPerson);
                }
                else if (evtEnty.Id != 0) //for offer update
                {
                    var proxyLinkEnty = evtEnty.Links.FirstOrDefault(p => p.RoleId == (int)Models.RoleTypes.ResourceProxy);
                    if (proxyLinkEnty != null)
                    {
                        res.Add(proxyLinkEnty.Subject);
                        evtEnty.Links.Remove(proxyLinkEnty);
                        ctx.Links.DeleteObject(proxyLinkEnty);
                    }
                }
            }

            return res;
        }
    }
}
