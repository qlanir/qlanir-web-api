﻿using Neo4jClient.Cypher;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qlanir_web_api.DataContext.GraphDataContext
{
    partial class GraphDataContext
    {
        public NodeGraphPath[] GetPathsFromProfile(int ProfileId, int Id, string Type)
        {
            if (ProfileId == Id)
                return new NodeGraphPath[0];

            var paths = _client.Cypher.Match("(n:person)-[r_n_p]->(p:person)-[r_p_m]->(m:" + Type + ")")
                .Where("n.Id = {ProfileId} and m.Id = {Id}")
                .WithParam("ProfileId", ProfileId)
                .WithParam("Id", Id)
                .Return((n, p, m) => new { n = n.As<NodeGraphNode>(), p = p.As<NodeGraphNode>(), m = m.As<NodeGraphNode>() })
                .Limit(10)
                .Results.Select(p => new { n = p.n, p = new[] { p.p }, m = p.m }).ToArray();

            if (paths.Length < 10)
            {
                paths = paths.Union(
                    _client.Cypher.Match(string.Format("(n:person)-[r_n_p]->(p:person)-[r_p_m]->(p1:person)-[r_p1_m]->(m:{0})", Type))
                    .Where("n.Id = {ProfileId} and m.Id = {Id} and p.Id <> {ProfileId} and p1.Id <> {ProfileId} and p.Id <> {Id} and p1.Id <> {ProfileId}")
                    .WithParam("ProfileId", ProfileId)
                    .WithParam("Id", Id)
                    .Return((n, p, p1, m) => new { n = n.As<NodeGraphNode>(), p = p.As<NodeGraphNode>(), p1 = p1.As<NodeGraphNode>(), m = m.As<NodeGraphNode>() })
                    .Limit(10 - paths.Length)
                    .Results.Select(p => new { n = p.n, p = new[] { p.p, p.p1 }, m = p.m })
                    ).ToArray();
            }

            return paths.Select(p => new NodeGraphPath { Issuer = p.n, Taget = p.m, Proxies = p.p }).ToArray();
             
        }
    }
}
