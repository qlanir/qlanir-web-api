﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qlanir_web_api.DataContext.GraphDataContext
{
    public class GraphRelation
    {
        public GraphNode startNode { get; set; }

        public GraphNode endNode { get; set; }

        public GraphNode link { get; set; }

    }
}
