﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qlanir_web_api.DataContext.GraphDataContext
{
    partial class GraphDataContext
    {
        public void Clean()
        {
            _client.Cypher.Match("n-[r]-m").Delete("r").ExecuteWithoutResults();
            _client.Cypher.Match("n").Delete("n").ExecuteWithoutResults();
        }

        public static void CleanAll()
        {
            using (var graph = new GraphDataContext())
            {
                graph.Clean();
            }
        }

    }
}
