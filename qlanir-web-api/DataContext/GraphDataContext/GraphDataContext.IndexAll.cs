﻿using qlanir_web_api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qlanir_web_api.DataContext.GraphDataContext
{
    public partial class GraphDataContext
    {
        public void IndexAll()
        {
            this.Clean();

            using (var ctx = new QlanirEntities.QlanirEntitySet())
            {

                foreach (var subject in ctx.Subjects.Include("Links.Event.Links").Include("Links.Event.Tags").Where(p => !(p is QlanirEntities.Identifier)))
                {
                    IndexSubject(subject);
                }

                foreach (var subject in ctx.Subjects.OfType<QlanirEntities.Identifier>().Include("Links.Event.Links").Include("Links.Event.Tags").Include("Tag"))
                {
                    IndexSubject(subject);
                }


                foreach (var tag in ctx.Tags)
                {
                    IndexTag(tag);
                }

                //create links between subjects
                foreach (var evt in ctx.Events.Include("Links.Subject").Include("Links.Role").Include("Links.Event.Links").Include("Links.Event.Tags"))
                {
                    if (evt.TypeId == (int)EventTypes.Resource)
                    {
                        IndexOfferEvent(evt);
                    }
                    else if (evt.Links.Count() == 2)
                    {
                        IndexEvent(evt);
                    }

                }

                /*
                //create rels between tags
                foreach (var tag in ctx.Tags.Include("Parent"))
                {
                    if (tag.ParentId != null)
                    {
                        this.SetRelation(IndexActionType.Create,
                            new RelationPoint { NodeId = (int)tag.ParentId, NodeType = "tag", Type = "tt_child_of", TagId = 0, TagLabel = tag.Parent.Label },
                            new RelationPoint { NodeId = tag.Id, NodeType = "tag", Type = "tt_parent_of", TagId = 0, TagLabel = tag.Label });
                    }
                }
                 */

                //create rels between tags and subjects
                foreach (var tagMap in ctx.TagMaps.Include("Tag").Include("Subject"))
                {
                    IndexTagMap(tagMap);
                }
            }
        }

        private void IndexTagMap(QlanirEntities.TagMap tagMap)
        {
            IndexTag(tagMap.Tag);

            this.SetRelation(IndexActionType.Create,
                new RelationPoint { Id = tagMap.Id, NodeId = (int)tagMap.SubjectId, NodeType = GetType(tagMap.Subject), Type = "st", TagId = 0, TagLabel = null },
                new RelationPoint { Id = tagMap.Id, NodeId = tagMap.TagId, NodeType = "tag", Type = "ts", TagId = 0, TagLabel = null });
        }

        private void IndexEvent(QlanirEntities.Event Event)
        {
            var links = Event.Links.ToArray();

            if (links.Length == 2)
            {

                var subj1 = MapSubject(links[0].Subject);
                var subj2 = MapSubject(links[1].Subject);

                var linkId1 = links[0].Id;
                var linkId2 = links[1].Id;

                var role1 = links[0].Role;
                var role2 = links[1].Role;

                /*
                if (subj1.Type == "contact")
                {
                    //linkId1 = links[1].Id;
                    //linkId2 = links[0].Id;
                    
                    role1 = links[0].Role;
                }


                if (subj2.Type == "contact")
                {
                    //linkId1 = links[1].Id;
                    //linkId2 = links[0].Id;

                    role2 = links[1].Role;
                    //role2 = new QlanirEntities.Role { Id = ((QlanirEntities.Identifier)links[1].Subject).Tag.Id, Label = ((QlanirEntities.Identifier)links[1].Subject).Tag.Label };
                }
                 */


                CreateRelation(linkId1, linkId2, subj1, subj2, role1, role2);

            }
            else
            {
                throw new NotImplementedException();
            }

        }

        private void IndexOfferEvent(QlanirEntities.Event Event)
        {

            var links = Event.Links.ToArray();

            var hostSubjLink = links.FirstOrDefault(p => p.RoleId != (int)RoleTypes.ResourceProxy);
            var proxyLink = links.FirstOrDefault(p => p.RoleId == (int)RoleTypes.ResourceProxy);
            var offerEvt = hostSubjLink != null ? hostSubjLink.Event : proxyLink.Event;

            SubjectNode hostSubj = hostSubjLink != null ? MapSubject(hostSubjLink.Subject) : null;
            SubjectNode proxySubj = proxyLink != null ? MapSubject(proxyLink.Subject) : null;
            var offerSubj = new SubjectOfferNode
                        {
                            Id = offerEvt.Id,
                            LabelId = offerEvt.Tags.FirstOrDefault().Id,
                            Label = offerEvt.Tags.FirstOrDefault().Label,
                            Type = "offer",
                            Description = offerEvt.Description,
                            Owner = (int)offerEvt.OwnerId,
                            Created = offerEvt.Created

                        };

            this.SetNode(IndexActionType.Update, offerSubj.Id, "offer",
                    new SubjectOfferNode
                    {
                        Id = Event.Id,
                        Label = offerSubj.Label,
                        Owner = offerSubj.Owner,
                        Description = offerSubj.Description,
                        Type = offerSubj.Type,
                        LabelId = offerSubj.LabelId,
                        Created = offerSubj.Created
                    });

            if (hostSubj != null)
            {
                CreateRelation(hostSubjLink.Id, hostSubjLink.Id, hostSubj, offerSubj,
                       new QlanirEntities.Role { Id = (int)Models.RoleTypes.Resource },
                       new QlanirEntities.Role { Id = (int)Models.RoleTypes.ReourceOwner });
            }

            if (proxySubj != null)
            {
                if (hostSubj != null)
                    this.IndexSubject(proxyLink.Subject);

                CreateRelation(proxyLink.Id, proxyLink.Id, offerSubj, proxySubj,
                            new QlanirEntities.Role { Id = (int)Models.RoleTypes.ResourceProxy },
                            new QlanirEntities.Role { Id = (int)Models.RoleTypes.ResourceProxy });
            }

            // var subjs = MapSubjectOffers(subject);

            
        }

        private static string SplitLabel(string Label)
        {
            var label = Label;
            if (!string.IsNullOrEmpty(label))
            {
                label = label.Remove(0, label.IndexOf("/") + 1).Trim().ToLower();
            }

            return label;
        }

        private void CreateRelation(int Id1, int Id2, SubjectNode subj1, SubjectNode subj2, QlanirEntities.Role Role1, QlanirEntities.Role Role2)
        {
            var label1 = SplitLabel(Role1.Label);
            var label2 = SplitLabel(Role2.Label);

            this.SetRelation(IndexActionType.Create,
                new RelationPoint { Id = Id1, NodeId = subj1.Id, NodeType = subj1.Type, Type = "ss", TagId = Role1.Id, TagLabel = label1 },
                new RelationPoint { Id = Id2, NodeId = subj2.Id, NodeType = subj2.Type, Type = "ss", TagId = Role2.Id, TagLabel = label2 });
        }

        private void IndexTag(QlanirEntities.Tag Tag)
        {
            var tag = new { Id = Tag.Id, Label = Tag.Label, Owner = Tag.OwnerId, Type = "tag" };

            this.SetNode(IndexActionType.Update, tag.Id, "tag", tag);
        }

        private static IEnumerable<SubjectOfferWithProxyNode> MapSubjectOffers(QlanirEntities.Subject Enty)
        {
            if (Enty.Id == -1)
                return new SubjectOfferWithProxyNode[0];

            var offerEvents =
                Enty.Links.SelectMany(p => p.Event.Links).Where(p => p.RoleId == (int)RoleTypes.ReourceOwner).Distinct();

            return offerEvents.Select(p =>
            {
                var proxyLink = p.Event.Links.FirstOrDefault(s => s.RoleId == (int)RoleTypes.ResourceProxy);
                SubjectNode proxyPerson = null;

                if (proxyLink != null)
                {
                    var proxyEnty = proxyLink.Subject;
                    proxyPerson = new SubjectNode { Id = proxyEnty.Id, Type = "person", Label = proxyEnty.Label, Owner = proxyEnty.OwnerId == null ? proxyEnty.Id : (int)proxyEnty.OwnerId };
                }

                var res = new SubjectOfferWithProxyNode
                {
                    Offer =
                        new SubjectOfferNode
                        {
                            Id = (int)p.EventId,
                            LabelId = p.Event.Tags.FirstOrDefault().Id,
                            Label = p.Event.Tags.FirstOrDefault().Label,
                            Type = "offer",
                            Description = p.Event.Description,
                            Owner = p.OwnerId ?? p.Id
                        },
                    Proxy = proxyPerson
                };

                return res;
            });

        }

        private static SubjectNode MapSubject(QlanirEntities.Subject Enty)
        {
            if (Enty.Id == -1)
                return null;

            var type = GetType(Enty);

            if (type != null)
            {
                SubjectNode model;

                if (Enty is QlanirEntities.Identifier)
                {
                    var contactModel = new SubjectContactNode();
                    contactModel.Label = ((QlanirEntities.Identifier)Enty).Value;


                    var label = SplitLabel(((QlanirEntities.Identifier)Enty).Tag.Label);
                    contactModel.ContactTypeLabel = label;
                    contactModel.ContactTypeId = ((QlanirEntities.Identifier)Enty).Tag.Id;

                    model = contactModel;

                }
                else
                {
                    if (type == "person")
                    {
                        var person = (QlanirEntities.Person)Enty;
                        var personModel = new SubjectPersonNode();
                        model = personModel;

                        personModel.Birthdate = person.Birthday == null ? null : person.Birthday.Value.ToString("dd.MM.yyyy");
                        personModel.FirstName = person.FirstName;
                        personModel.LastName = person.LastName;
                        personModel.MiddleName = person.MiddleName;
                        personModel.NickName = person.NickName;
                        personModel.Sex = person.Sex;
                    }
                    else
                    {
                        model = new SubjectNode();
                    }

                    model.Label = Enty.Label;
                }

                model.Id = Enty.Id;
                model.Type = GetType(Enty);
                model.Owner = Enty.OwnerId ?? Enty.Id;
                model.Created = Enty.Created;

                return model;
            }


            return null;
        }

        private void IndexSubject(QlanirEntities.Subject Enty)
        {
            var subj = MapSubject(Enty);

            this.SetNode(IndexActionType.Update, subj.Id, subj.Type, subj);
        }

        private void IndexOffers(QlanirEntities.Subject Enty)
        {
            var offers = MapSubjectOffers(Enty);
        }

        internal static string GetType(QlanirEntities.Subject Enty)
        {
            if (Enty is QlanirEntities.Person)
            {
                return "person";
            }
            else if (Enty is QlanirEntities.Company)
            {
                return "organization";
            }
            else if (Enty is QlanirEntities.Identifier)
            {
                return "contact";
            }
            else
            {
                return null;
            }
        }

        public class SubjectNode
        {
            public int Id { get; set; }

            public string Type { get; set; }

            public string Label { get; set; }

            public int Owner { get; set; }

            public System.DateTime? Created { get; set; }

        }

        public class SubjectOfferNode : SubjectNode
        {
            public int LabelId { get; set; }

            public string Description { get; set; }
        }

        public class SubjectOfferWithProxyNode
        {
            public SubjectNode Proxy;

            public SubjectOfferNode Offer;
        }


        public class SubjectContactNode : SubjectNode
        {
            public int ContactTypeId { get; set; }

            public string ContactTypeLabel { get; set; }
        }

        public class SubjectPersonNode : SubjectNode
        {
            public string Birthdate { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string MiddleName { get; set; }
            public string NickName { get; set; }
            public int? Sex { get; set; } //0 - woman, 1 - man, null - unknown
        }

    }
}
