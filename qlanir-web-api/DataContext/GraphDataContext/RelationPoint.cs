﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qlanir_web_api.DataContext.GraphDataContext
{
    public class RelationPoint
    {
        public int Id { get; set; }

        public int NodeId { get; set; }

        public string NodeType { get; set; }

        public string Type { get; set; }

        public int TagId { get; set; }

        public string TagLabel { get; set; }
    }
}
