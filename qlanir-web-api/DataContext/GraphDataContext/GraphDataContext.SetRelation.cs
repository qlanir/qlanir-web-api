﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qlanir_web_api.DataContext.GraphDataContext
{
    partial class GraphDataContext
    {
        private void SetRelation(IndexActionType IndexActionType, RelationPoint Point1, RelationPoint Point2)
        {
            if (IndexActionType == IndexActionType.Create)
            {
                /*
                var id1 = 
                    string.Format("{0}_{1}_{2}_{3}",
                    Point1.NodeId, Point1.Type, Point2.NodeId, Point2.Type);
                var id2 = 
                    string.Format("{0}_{1}_{2}_{3}",
                    Point2.NodeId, Point2.Type, Point1.NodeId, Point1.Type);
                 */



                _client.Cypher
                .Match("(s1:" + Point1.NodeType + ")", "(s2:" + Point2.NodeType + ")")
                .Where("s1.Id = " + Point1.NodeId)
                .AndWhere("s2.Id = " + Point2.NodeId)
                .CreateUnique("s1-[r1:" + Point1.Type + "]->s2")
                .CreateUnique("s2-[r2:" + Point2.Type + "]->s1")
                .Set(string.Format("r1 = {{ Id: \"{0}\", TagId: {1}, TagLabel: \"{2}\" }}", Point1.Id, Point1.TagId, Point1.TagLabel))
                .Set(string.Format("r2 = {{ Id: \"{0}\", TagId: {1}, TagLabel: \"{2}\" }}", Point2.Id, Point2.TagId, Point2.TagLabel))
                .ExecuteWithoutResults();

            }
        }
    }
}
