﻿using Neo4jClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qlanir_web_api.DataContext.GraphDataContext
{
    public partial class GraphDataContext : IDisposable
    {
        private readonly GraphClient _client;

        public GraphDataContext()
        {
            var neo4jUri = ConfigurationManager.AppSettings.Get("NEO4J_URI");
            _client = new GraphClient(new Uri(neo4jUri));
            _client.Connect();            
        }

        public void Dispose()
        {
        }
    }
}
