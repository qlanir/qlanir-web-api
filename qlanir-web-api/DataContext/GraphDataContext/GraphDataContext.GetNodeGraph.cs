﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qlanir_web_api.DataContext.GraphDataContext
{
    partial class GraphDataContext
    {

        public IEnumerable<NodeGraphLink> GetNodeGraph(int Owner, int Id, string Type, int LegsCount = 1)
        {            
             
            var nodes = _client.Cypher.Match("(n:" + Type + ")-[*1.." + LegsCount + "]->(m)")
                .Where("n.Id = {Id} and m.Id <> {Id} and n.Owner = {Owner} and m.Owner = {Owner} and m.Type <> 'contact'")
                .WithParam("Id", Id)
                .WithParam("Owner", Owner)
                .Return((n, m) => new { n_id = n.Id(), m_id = m.Id() })
                .Results.ToArray();

            var ids = nodes.Select(p => p.m_id).Union(new[] { nodes.First().n_id }).Distinct().ToArray();

            var links = _client.Cypher.Start(new { n = "NODE(" + string.Join(",", ids) + ")", m = "NODE(" + string.Join(",", ids) + ")" })
                .Match("n-[r]->m")
                .Return((n, r, m) => new { n = n.As<NodeGraphNode>(), r = r.As<NodeGraphLink>(), m = m.As<NodeGraphNode>() })
                .Results.ToArray();

            return links.Select(p => new NodeGraphLink
            {
                Id = p.r.Id,
                Label = p.r.Label,
                FromNode = new NodeGraphNode
                {
                    Id = p.n.Id,
                    Label = p.n.Label,
                    Type = p.n.Type
                },
                ToNode = new NodeGraphNode
                {
                    Id = p.m.Id,
                    Label = p.m.Label,
                    Type = p.m.Type
                }
            });
        }
    }

    public class NodeGraphNode
    {
        public int Id { get; set; }

        public string Type { get; set; }

        public string Label { get; set; }
    }

    public class NodeGraphPath
    {
        public NodeGraphNode Issuer { get; set; }

        public NodeGraphNode [] Proxies { get; set; }      

        public NodeGraphNode Taget { get; set; }
    }


    public class NodeGraphLink
    {
        public string Id { get; set; }

        public string Label { get; set; }

        public string Type { get; set; }

        public NodeGraphNode FromNode { get; set; }

        public NodeGraphNode ToNode { get; set; }
    }

}
