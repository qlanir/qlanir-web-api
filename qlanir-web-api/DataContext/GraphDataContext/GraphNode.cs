﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qlanir_web_api.DataContext.GraphDataContext
{
    public class GraphNode
    {
        public int id { get; set; }

        public string name { get; set; }

        public int owner { get; set; }

        public string type { get; set; }
    }

    public class Rel
    {
        public RelData data { get; set; }

        public string start { get; set; }

        public string end { get; set; }
    }

    public class RelData
    {
        public int TagId { get; set; }

        public string TagLabel { get; set; }    
    }

}
