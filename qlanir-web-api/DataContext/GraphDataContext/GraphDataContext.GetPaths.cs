﻿using Neo4jClient.Cypher;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qlanir_web_api.DataContext.GraphDataContext
{
    partial class GraphDataContext
    {
        public IEnumerable<NodeGraphNode> [] GetPaths(int ProfileId, int Id)
        {
            var paths = _client.Cypher.Match("p=n-[r:ss*1..5]->m")
                .Where(string.Format("n.Id = {0} and m.Id={1} and all(k in TAIL(nodes(p)) where k.Owner = {0} and  k.Id <> {0} and (k.Type = \"person\" or k.Type = \"organization\" or k.Type=\"offer\"))", ProfileId, Id))
                .Return((p, n) => Return.As<IEnumerable<NodeGraphNode>>("nodes(p)") )
                .Results.ToArray();

            return paths.Where(p => p.Select(s => s.Id).Distinct().Count() == p.Count()).OrderBy(p => p.Count()).ToArray();
        }
    }
}
