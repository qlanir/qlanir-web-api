﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qlanir_web_api.DataContext.GraphDataContext
{
    partial class GraphDataContext
    {

        private static GraphDataContext.SubjectNode AsSubjectNode(string NodeStr)
        {
            GraphDataContext.SubjectNode res = null;

            var node = JsonConvert.DeserializeObject<dynamic>(NodeStr);

            //TODO: When node retrieved via relation query, all data are contained in .data field, if directly data are in structure itself
            dynamic data = node["data"] != null ? node.data : node; 

            if (data.Type == "contact")
            {
                var contact = new GraphDataContext.SubjectContactNode();

                contact.ContactTypeId = (int)data.ContactTypeId;
                contact.ContactTypeLabel = (string)data.ContactTypeLabel;

                res = contact;
            }
            else if (data.Type == "offer")
            {
                var offer = new GraphDataContext.SubjectOfferNode();

                offer.Description = data.Description;
                offer.LabelId = data.LabelId;

                res = offer;
            }
            else if (data.Type == "person")
            {
                var person = new GraphDataContext.SubjectPersonNode();

                person.Birthdate = data.Birthdate;
                person.LastName = data.LastName;
                person.FirstName = data.FirstName;
                person.MiddleName = data.MiddleName;
                person.NickName = data.NickName;
                person.Sex = data.Sex;

                res = person;
            }
            else
            {
                res = new GraphDataContext.SubjectNode();
            }

            res.Id = data.Id;
            res.Label = data.Label;
            res.Owner = data.Owner == null ? 0 : data.Owner;
            res.Type = data.Type;
            res.Created = data.Created;

            return res;
        }

        private SubjectNode MatchAs(string SubjectType, int SubjectId)
        {
            var res = _client.Cypher.Match("(n:" + SubjectType + ")")
                    .Where("n.Id = {Id}")
                    .WithParam("Id", SubjectId)
                    .Return(n => n.As<string>())
                    .Results.Select(p => (SubjectNode)AsSubjectNode(p)).FirstOrDefault();

            if (res == null)
                throw new System.Data.Entity.Core.ObjectNotFoundException();

            return res;
        }

        public T GetSubject<T>(int SubjectId)
            where T : Models.Subject, new()
        {
            if (typeof(T) == typeof(Models.Offer))
            {
                return GetOffer<T>(SubjectId);
            }

            T subj = new T();

            SubjectNode node = MatchAs(subj.Type, SubjectId);

            subj.Id = node.Id;
            subj.Label = node.Label;
            subj.Owner = node.Owner;
            if (node.Created != null)
                subj.Created = (System.DateTime)node.Created;

            var linksQ = _client.Cypher.Match("((n:" + subj.Type + ")-[r]->(m)),((m)-[h]->(n))")
               .Where("n.Id = {Id}")
               .WithParam("Id", SubjectId)
               .Return((n, r, m, h) => new
               {
                   r = r.As<GraphLink>(),
                   m = m.As<string>(),
                   h = h.As<GraphLink>()
               });

            var links = linksQ
               .Results.Select(p => new { h = p.h, r = p.r, m = AsSubjectNode(p.m) })
               .ToArray();



            
            if (subj.Type == "person") 
            {
                var person = (Models.Person)(Models.Subject)subj;
                var personNode = ((SubjectPersonNode)node);

                person.Birthdate = personNode.Birthdate;
                person.NameParts = new Models.PersonNameParts {
                    FirstName = personNode.FirstName,
                    LastName = personNode.LastName,
                    MiddleName = personNode.MiddleName,
                    NickName = personNode.NickName
                };
                person.Sex = personNode.Sex;
            }


            if (subj is Models.ISubjectWithLinks)
            {
                var subjLinks = (Models.ISubjectWithLinks)subj;

                subjLinks.Tags = links.Where(p => p.m.Type == "tag").Select(p => new Models.Tag { Id = p.m.Id, Label = p.m.Label, Owner = p.m.Owner })
                    .OrderBy(p => p.Label).ToArray();
                subjLinks.People = links.Where(p => p.m.Type == "person").Select(p => (Models.PersonLink)CreateLink<Models.PersonBase>(p.h, p.m, p.r))
                    .OrderBy(p => p.Subject.Label).ToArray();
                subjLinks.Organizations = links.Where(p => p.m.Type == "organization").Select(p => CreateLink<Models.OrganizationBase>(p.h, p.m, p.r))
                    .OrderBy(p => p.Subject.Label).ToArray();
                subjLinks.Offers = links.Where(p => p.m.Type == "offer").Select(p => (Models.OfferLink)CreateLink<Models.OfferBase>(p.h, p.m, p.r))
                    .OrderBy(p => p.Subject.Label).ToArray();
                subjLinks.Contacts = links.Where(p => p.m.Type == "contact").Select(p => CreateLink<Models.Contact>(p.h, p.m, p.r))
                    .OrderBy(p => p.Subject.Label).ToArray();

                //Get service proxies if exists

                var offerIds = subjLinks.Offers.Select(p => p.Subject.Id).ToArray();

                if (offerIds.Length > 0)
                {
                    var offerProxiesQ = _client.Cypher.Match("(s:offer)-[r]->(p:person)")
                       .Where("s.Id IN [" + string.Join(",", offerIds) + "] AND r.TagId = -43")
                       .Return((s, p) => new
                       {
                           s = s.As<string>(),
                           p = p.As<string>()
                       });

                    var offerProxies = offerProxiesQ
                       .Results.Select(p => new {p = AsSubjectNode(p.p), s = AsSubjectNode(p.s) })
                       .ToArray();

                    foreach (var offer in subjLinks.Offers)
                    {
                        var proxyPair = offerProxies.FirstOrDefault(p => p.s.Id == offer.Subject.Id);

                        if (proxyPair != null)
                        {
                            offer.Proxy = new Models.PersonBase { Id = proxyPair.p.Id, Label = proxyPair.p.Label, Owner = proxyPair.p.Owner };
                        }
                    }

                    subjLinks.Offers = subjLinks.Offers.OrderBy(p => p.Tag.Label).ToArray();
                }
            }

            return subj;
        }

        private static Models.Link<T> CreateLink<T>(GraphLink Link, SubjectNode Subject, GraphLink HostLink) where T : Models.Subject, new()
        {
            Models.Link<T> res = null;

            if (Subject.Type == "offer")
            {
                res = (Models.Link<T>)(object)(Models.Link<Models.OfferBase>)new Models.OfferLink
                {
                    Id = Subject.Id,//Link.Id,
                    Tag = new Models.Tag { Id = Link.TagId, Label = string.IsNullOrEmpty(Link.TagLabel) ? null : Link.TagLabel, Owner = Subject.Owner },
                    Subject = new Models.OfferBase { Id = Subject.Id, Label = Subject.Label, Owner = Subject.Owner },
                    HostTag = new Models.Tag { Id = HostLink.TagId, Label = string.IsNullOrEmpty(HostLink.TagLabel) ? null : HostLink.TagLabel, Owner = Subject.Owner }
                };

                var offerNode = (SubjectOfferNode)Subject;
                var offer = (Models.OfferBase)(Models.Subject)res.Subject;
                offer.Label = offerNode.Description;
                res.Tag.Id = offerNode.LabelId;
                res.Tag.Label = offerNode.Label;
                res.Tag.Owner = offerNode.Owner;

                res.HostTag = null;
            }
            else
            {
                if (Subject.Type == "person")
                {
                    res = (Models.Link<T>)(object)(Models.Link<Models.PersonBase>)new Models.PersonLink();
                }
                else
                {
                    res = new Models.Link<T>();
                }

                res.Id = Link.Id;
                res.Tag = new Models.Tag { Id = Link.TagId, Label = string.IsNullOrEmpty(Link.TagLabel) ? null : Link.TagLabel, Owner = Subject.Owner };
                res.Subject = new T { Id = Subject.Id, Label = Subject.Label, Owner = Subject.Owner};
                res.HostTag = new Models.Tag { Id = HostLink.TagId, Label = string.IsNullOrEmpty(HostLink.TagLabel) ? null : HostLink.TagLabel, Owner = Subject.Owner };

                if (Subject.Type == "contact")
                {
                    var contactSubject = (SubjectContactNode)Subject;
                    ((Models.Contact)(Models.Subject)res.Subject).ContactType = new Models.Tag
                    {
                        Id = contactSubject.ContactTypeId,
                        Label = contactSubject.ContactTypeLabel,
                        Owner = Subject.Owner
                    };
                }

                if (Subject.Type == "person")
                {
                    var personNode = (SubjectPersonNode)Subject;

                    ((Models.PersonBase)(Models.Subject)res.Subject).NameParts = new Models.PersonNameParts
                    {                        
                        FirstName = personNode.FirstName, LastName = personNode.LastName, MiddleName = personNode.MiddleName, NickName = personNode.MiddleName
                    };

                    ((Models.PersonBase)(Models.Subject)res.Subject).Sex = personNode.Sex;
                }
            }

            return res;
        }

        public class GraphLink
        {
            public int Id { get; set; }

            public int TagId { get; set; }

            public string TagLabel { get; set; }
        }

        public class GraphSelectItem
        {
            public GraphLink r { get; set; }

            public GraphLink h { get; set; }

            public SubjectNode m { get; set; }
        }



    }
}
