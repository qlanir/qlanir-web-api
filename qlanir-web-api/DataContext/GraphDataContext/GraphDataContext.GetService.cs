﻿using Newtonsoft.Json;
using qlanir_web_api.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qlanir_web_api.DataContext.GraphDataContext
{
    partial class GraphDataContext
    {

        public T GetService<T>(int TypeId) where T : Models.Subject, new()
        {
            T res = new T();

            SubjectOfferNode[] nodes = _client.Cypher.Match("(n:offer)")
                     .Where("n.LabelId = {Id}")
                     .WithParam("Id", TypeId)
                     .Return((n) => n.As<string>())
                     .Results.Select(p => (SubjectOfferNode) AsSubjectNode(p)).ToArray();

            if (nodes.Length == 0)
                throw new System.Data.Entity.Core.ObjectNotFoundException();


            res.Id = nodes[0].LabelId;
            res.Label = nodes[0].Label;
            res.Owner = nodes[0].Owner;
            if (nodes[0].Created != null)
                res.Created = (System.DateTime)nodes[0].Created;

            List<OfferFull> offers = new List<OfferFull>();

            foreach (var node in nodes)
            {
                var linksQ = _client.Cypher.Match("((n:offer)-[r]->(m)),((m)-[h]->(n))")
                   .Where("n.Id = {Id}")
                   .WithParam("Id", node.Id)
                   .Return((n, r, m, h) => new
                   {
                       r = r.As<GraphLink>(),
                       m = m.As<string>(),
                       h = h.As<GraphLink>()
                   });
                var links = linksQ
                   .Results.Select(p => new { h = p.h, r = p.r, m = AsSubjectNode(p.m) })
                   .ToArray();

                var hostSubject = links.Where(p => p.r.TagId == -42).Select(p =>
                    new SubjectBase
                    {
                        Label = p.m.Label,
                        Type = p.m.Type,
                        Id = p.m.Id,
                        Owner = p.m.Owner
                    }).FirstOrDefault();

                var proxySubject = links.Where(p => p.r.TagId == -43).Select(p =>
                    new PersonBase
                    {
                        Label = p.m.Label,
                        Id = p.m.Id,
                        Owner = p.m.Owner
                    }).FirstOrDefault();

                var offer = new OfferFull
                    {
                        Id = node.Id,
                        HostSubject = hostSubject,
                        Proxy = proxySubject,
                        Subject = new OfferBase { Id = node.Id, Label = node.Description, Owner = node.Owner },
                        Tag = new Tag { Id = node.LabelId, Label = node.Label, Owner = node.Owner }
                    };

                offers.Add(offer);
            }

            var service = (IService)res;
            service.Offers = offers.ToArray();

            return res;
        }

        public T GetOffer<T>(int SubjectId)
           where T : Models.Subject, new()
        {
            T subj = new T();

            SubjectOfferNode node = _client.Cypher.Match("(n:" + subj.Type + ")")
                    .Where("n.Id = {Id}")
                    .WithParam("Id", SubjectId)
                    .Return(n => n.As<string>())
                    .Results.Select(p => (SubjectOfferNode)AsSubjectNode(p)).FirstOrDefault();

            //TO DO : throw not found exception
            if (node == null)
                throw new System.Data.Entity.Core.ObjectNotFoundException();

            subj.Id = node.Id;
            subj.Label = node.Description;
            subj.Owner = node.Owner;
            if (node.Created != null)
                subj.Created = (System.DateTime)node.Created;

            var linksQ = _client.Cypher.Match("((n:" + subj.Type + ")-[r]->(m)),((m)-[h]->(n))")
               .Where("n.Id = {Id}")
               .WithParam("Id", SubjectId)
               .Return((n, r, m, h) => new
               {
                   r = r.As<GraphLink>(),
                   m = m.As<string>(),
                   h = h.As<GraphLink>()
               });

            var links = linksQ
               .Results.Select(p => new { h = p.h, r = p.r, m = AsSubjectNode(p.m) })
               .ToArray();


            if (subj is Models.ISubjectWithLinks)
            {
                var subjLinks = (Models.ISubjectWithLinks)subj;

                //subjLinks.Tags = links.Where(p => p.m.Type == "tag").Select(p => new Models.Tag { Id = p.m.Id, Label = p.m.Label, Owner = p.m.Owner }).ToArray();
                subjLinks.People = links.Where(p => p.m.Type == "person").Select(p => (Models.PersonLink)CreateLink<Models.PersonBase>(p.r, p.m, p.h))
                    .OrderBy(p => Math.Abs(p.Tag.Id)).ToArray();
                subjLinks.Organizations = links.Where(p => p.m.Type == "organization").Select(p => CreateLink<Models.OrganizationBase>(p.r, p.m, p.h))
                    .OrderBy(p => p.Subject.Label).ToArray();
                subjLinks.Offers = new OfferLink[] { };//links.Where(p => p.m.Type == "service").Select(p => (Models.ServiceLink)CreateLink<Models.ServiceBase>(p.r, p.m, p.h)).ToArray();
                subjLinks.Contacts = new Link<Contact>[] { };//links.Where(p => p.m.Type == "contact").Select(p => CreateLink<Models.Contact>(p.r, p.m, p.h)).ToArray();

                subjLinks.Tags = new Tag[] { new Tag {Id= node.LabelId, Label = node.Label, Owner = node.Owner} };
            }

            return subj;
        }

    }
}
