﻿using qlanir_web_api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qlanir_web_api.DataContext.GraphDataContext
{
    partial class GraphDataContext 
    {
        public LinkedIndexes Index(QlanirEntities.Subject Subject, bool IndexLinks)
        {
            LinkedIndexes linkedIndexes = new LinkedIndexes();

            if (IndexLinks)
            {
                DeleteNode(Subject.Id, GetType(Subject));
            }

            IndexSubject(Subject);

            if (IndexLinks)
            {
                foreach (var evt in Subject.Links.Select(p => p.Event).ToArray())
                {
                    if (evt.TypeId != (int)EventTypes.Resource)
                    {
                        var otherPointSubject = evt.Links.Select(p => p.Subject).Where(p => p.Id != Subject.Id).FirstOrDefault();

                        IndexSubject(otherPointSubject);

                        if (otherPointSubject is QlanirEntities.Person)
                        {
                            ((List<int>)linkedIndexes.People).Add(otherPointSubject.Id);
                        }
                        else if (otherPointSubject is QlanirEntities.Company)
                        {
                            ((List<int>)linkedIndexes.Orgs).Add(otherPointSubject.Id);
                        }

                        IndexEvent(evt);
                    }
                    else
                    { 
                        //Consider offer
                        IndexOfferEvent(evt);

                        ((List<int>)linkedIndexes.Offers).Add(evt.Id);
                    }
                }

                foreach (var tagMap in Subject.TagMaps.ToArray())
                {
                    IndexTagMap(tagMap);
                }
            }

            return linkedIndexes;
        }

        public class LinkedIndexes
        {
            public LinkedIndexes()
            {
                Offers = new List<int>();
                People = new List<int>();
                Orgs = new List<int>();
            }

            public IEnumerable<int> Offers { get; set; }
            public IEnumerable<int> People { get; set; }
            public IEnumerable<int> Orgs { get; set; }
        }
    }
}
