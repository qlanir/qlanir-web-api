﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qlanir_web_api.DataContext.GraphDataContext
{
    partial class GraphDataContext
    {
        public IEnumerable<GraphRelation> GetRelationsBetween2Subjects(int User, int Subject)
        {
            if (User == Subject)
                return new GraphRelation[0];

            var path = _client.Cypher.Match("(n:person)<-[r:ss*1]-(m)")
                .Where("n.Id = {id1} and m.Id = {id2}")
                .WithParams(new { id1 = User, id2 = Subject })
                .Return((n, r, m) => new { rels = r.As<IEnumerable<Rel>>(), startNode = n.As<GraphNode>(), endName = m.As<GraphNode>() })
                .Results.FirstOrDefault();

            if (path == null)
            {
                path = _client.Cypher.Match("(n:person)<-[r:ss*2]-(m)")
                    .Where("n.Id = {id1} and m.Id = {id2}")
                    .WithParams(new { id1 = User, id2 = Subject })
                    .Return((n, r, m) => new { rels = r.As<IEnumerable<Rel>>(), startNode = n.As<GraphNode>(), endName = m.As<GraphNode>() })
                    .Results.FirstOrDefault();

                if (path == null)
                {
                    return new GraphRelation[0];
                }
                else
                {                    
                    var s = path.rels.First().start.Split('/');
                    var proxyId = int.Parse(s[s.Length - 1]);
                    var proxy = _client.Cypher.Match("n").Where("id(n)={id}").WithParam("id", proxyId).Return((n) => n.As<string>()).Results.Select(p => AsSubjectNode(p)).First();

                    var prx = new GraphNode { id = proxy.Id, name = proxy.Label, type = proxy.Type, owner = proxy.Owner };

                    return new[] { 
                        new GraphRelation { startNode = path.startNode, endNode = prx } ,
                        new GraphRelation { startNode = prx, endNode = path.endName } 
                    };
                }
            }
            else
            {
                var rel = path.rels.First();
                return new[] { new GraphRelation { startNode = path.startNode, endNode = path.endName, link = new GraphNode { id = rel.data.TagId, name = rel.data.TagLabel } } };
            }
        }

    }
}
