﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qlanir_web_api.DataContext.GraphDataContext
{
    partial class GraphDataContext
    {
        public void DeleteNode(int Id, string Type)
        {
            //for nodes with relations
            var q = _client.Cypher.Match("(n:" + Type + " {Id : " + Id + "})-[r]-()").Delete("n, r");
            q.ExecuteWithoutResults();

            //for nodes without relations
            q = _client.Cypher.Match("(n:" + Type + " {Id : " + Id + "})").Delete("n");
            q.ExecuteWithoutResults();

        }
    }
}
