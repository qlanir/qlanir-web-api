﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qlanir_web_api.DataContext.GraphDataContext
{
    partial class GraphDataContext
    {
        public void SetNode(IndexActionType IndexActionType, int Id, string Type, object Props)
        {
            if (IndexActionType == IndexActionType.Update)
            {
                var q = _client.Cypher
                    .Merge("(s:" + Type + " { Id: {id} })")
                    //.OnCreate()
                    .Set("s = {props}")
                    .WithParams(new
                    {
                        id = Id,
                        props = Props
                    });

               q.ExecuteWithoutResults();
            }
        }
    }
}
