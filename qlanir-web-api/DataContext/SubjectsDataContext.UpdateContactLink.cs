﻿using QlanirEntities;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qlanir_web_api.DataContext
{
    partial class SubjectsDataContext
    {
        public static void UpdateContactLink(string User, int ContactLinkId, int SubjectId, int TypeId, string ContactValue, int? RoleId)
        {
            if (TypeId == null && string.IsNullOrEmpty(ContactValue))
            {
                throw new ArgumentException("Either ContactLinkTagId or ContactValue should be defined");
            }

            using (var ctx = new QlanirEntities.QlanirEntitySet())
            {
                var owner = (int)GetUserId(User, ctx);

                var subjectEnty = ctx.Subjects.FirstOrDefault(p => p.Id == SubjectId && (p.OwnerId == owner || p.Id == owner));
                var linkEnty = ctx.Links.FirstOrDefault(p => p.Id == ContactLinkId && p.OwnerId == owner);

                if (linkEnty == null || subjectEnty == null)
                {
                    throw new ObjectNotFoundException();
                }

                var identEnty = (QlanirEntities.Identifier)linkEnty.Subject;

                if (TypeId != null)
                {
                    identEnty.TagId = TypeId;
                }

                linkEnty.RoleId = RoleId == null ? (int)qlanir_web_api.Models.RoleTypes.Identifyers : (int)RoleId;

                if (!string.IsNullOrEmpty(ContactValue))
                {
                    identEnty.Value = ContactValue;
                }

                ctx.SaveChanges();

                ctx.Refresh(System.Data.Entity.Core.Objects.RefreshMode.StoreWins, subjectEnty);

                Index(subjectEnty, true);
            }
        }
    }
}
