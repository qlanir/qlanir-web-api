﻿using qlanir_web_api.DataContext.ElasticDataContext;
using QlanirEntities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Core;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qlanir_web_api.DataContext
{
    public partial class SubjectsDataContext : ISubjectsDataContext
    {
        public static void IndexAll()
        {
            using (var graph = new GraphDataContext.GraphDataContext())
            {
                graph.IndexAll();
            }

            using (var elastic = new ElasticDataContext.ElasticDataContext())
            {
                elastic.IndexAll();
            }

        }

        public static void CleanAll(bool All = false)
        {
            using (var ctx = new QlanirEntities.QlanirEntitySet())
            {

                foreach (var evt in ctx.Events.Include("Tags"))
                {
                    foreach (var tag in evt.Tags.ToList())
                    {
                        evt.Tags.Remove(tag);
                    }
                    ctx.DeleteObject(evt);
                }

                foreach (var subject in ctx.Subjects)
                {
                    ctx.DeleteObject(subject);
                }

                var tagMaps = (IQueryable<TagMap>)ctx.TagMaps;

                if (!All)
                    tagMaps = tagMaps.Where(p => p.TagId > 0);

                foreach (var tagMap in tagMaps)
                {
                    ctx.DeleteObject(tagMap);
                }

                var tags = (IQueryable<Tag>)ctx.Tags;

                if (!All)
                    tags = tags.Where(p => p.Id > 0);

                foreach (var tag in tags)
                {
                    ctx.DeleteObject(tag);
                }

                foreach (var invite in ctx.Invites)
                {
                    ctx.DeleteObject(invite);
                }

                foreach (var user in ctx.Users)
                {
                    ctx.DeleteObject(user);
                }

                if (All)
                {
                    foreach (var ety in ctx.EventTypes)
                    {
                        ctx.DeleteObject(ety);
                    }

                    foreach (var ety in ctx.Roles)
                    {
                        ctx.DeleteObject(ety);
                    }


                }

                ctx.SaveChanges();
            }
        }

        internal static int GetUserId(string User, QlanirEntities.QlanirEntitySet QlanirEntitySet = null)
        {
            User user;

            if (User == "password_systemUser@qlanir.com")
                return -1;

            if (QlanirEntitySet != null)
            {
                user = QlanirEntitySet.Users.Where(p => p.Name == User).FirstOrDefault();
            }
            else
            {
                using (var ctx = new QlanirEntities.QlanirEntitySet())
                {
                    user = ctx.Users.Where(p => p.Name == User).FirstOrDefault();
                }
            }

            return user != null ? user.SubjectId : 0;
        }


        public T Get<T>(int Id, string User) where T : Models.Subject, new()
        {
            if (typeof(T) == typeof(Models.Person) && Id == -1)
            {
                Id = SubjectsDataContext.GetUserId(User);
            }

            using (var graph = new GraphDataContext.GraphDataContext())
            {
                if (typeof(T) == typeof(Models.Service))
                    return graph.GetService<T>(Id);
                if (typeof(T) == typeof(Models.Offer))
                    return graph.GetOffer<T>(Id);
                else
                    return graph.GetSubject<T>(Id);
            }

        }

        private QlanirEntities.Subject GetOrCreateSubjectEnty<T>(QlanirEntities.QlanirEntitySet ctx, int? Id, string User, T Subject) where T : Models.Subject
        {
            var owner = (int)GetUserId(User, ctx);

            if (Subject.Type != "offer")
            {
                QlanirEntities.Subject subjectEnty = null;

                if (Id != null)
                {
                    subjectEnty = ctx.Subjects.FirstOrDefault(p => p.Id == Id && (p.OwnerId == owner || p.Id == owner));
                }
                else// if (!string.IsNullOrEmpty(Subject.Label))
                {
                    if (Subject is Models.Person)
                    {
                        subjectEnty = new QlanirEntities.Person { OwnerId = owner };
                    }
                    else if (Subject is Models.Organization)
                    {
                        subjectEnty = new QlanirEntities.Company { OwnerId = owner };
                    }
                    else
                    {
                        throw new NotFiniteNumberException();
                    }

                    ctx.Subjects.AddObject(subjectEnty);
                }

                if (subjectEnty == null)
                {
                    throw new ObjectNotFoundException();
                }

                if (subjectEnty is QlanirEntities.Person)
                {
                    var person = (QlanirEntities.Person)subjectEnty;

                    var modelPerson = (Models.Person)(Models.Subject)Subject;

                    person.Birthday = string.IsNullOrEmpty(modelPerson.Birthdate) ? null :
                     (DateTime?)DateTime.ParseExact(modelPerson.Birthdate, "dd.MM.yyyy", System.Globalization.CultureInfo.InvariantCulture);

                    person.Sex = modelPerson.Sex;

                    bool namePartsNotDefined =
                        modelPerson.NameParts == null || (
                        string.IsNullOrEmpty(modelPerson.NameParts.FirstName) &&
                        string.IsNullOrEmpty(modelPerson.NameParts.LastName) &&
                        string.IsNullOrEmpty(modelPerson.NameParts.MiddleName) &&
                        string.IsNullOrEmpty(modelPerson.NameParts.NickName));

                    if (!namePartsNotDefined)
                    {
                        person.FirstName = modelPerson.NameParts.FirstName;
                        person.LastName = modelPerson.NameParts.LastName;
                        person.MiddleName = modelPerson.NameParts.MiddleName;
                        person.NickName = modelPerson.NameParts.NickName;
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(Subject.Label) && subjectEnty.Label != Subject.Label)
                        {
                            var spts = Subject.Label.Split(' ');

                            if (subjectEnty is QlanirEntities.Person)
                            {
                                if (modelPerson.NameParts == null || string.IsNullOrEmpty(modelPerson.NameParts.LastName))
                                    person.LastName = spts.Length > 1 ? spts[0].Trim() : null;

                                if (modelPerson.NameParts == null || string.IsNullOrEmpty(modelPerson.NameParts.FirstName))
                                    person.FirstName = spts[spts.Length == 1 ? 0 : 1].Trim();

                                if (spts.Length > 2 && (modelPerson.NameParts == null || string.IsNullOrEmpty(modelPerson.NameParts.MiddleName)))
                                    person.MiddleName = string.Join(" ", spts.Skip(2).Select(p => p.Trim()));

                            }
                        }
                    }
                }
                else if (subjectEnty is QlanirEntities.Identifier)
                {
                    if (!string.IsNullOrEmpty(Subject.Label) && subjectEnty.Label != Subject.Label)
                    {
                        var ident = (QlanirEntities.Identifier)subjectEnty;

                        ident.Value = Subject.Label;
                    }
                }
                else if (subjectEnty is QlanirEntities.Company)
                {
                    if (!string.IsNullOrEmpty(Subject.Label) && subjectEnty.Label != Subject.Label)
                    {

                        var company = (QlanirEntities.Company)subjectEnty;

                        company.Name = Subject.Label;
                    }
                }
                else
                {
                    throw new ArgumentOutOfRangeException("Subject of unknown type");
                }

                return subjectEnty;
            }
            else
            {
                throw new ArgumentOutOfRangeException("Offer must be created as link to the subject only!");
            }
        }

        private IEnumerable<QlanirEntities.Subject> UpdateSubjectEntyLinks<T>(QlanirEntities.QlanirEntitySet ctx, QlanirEntities.Subject Enty, T Model) where T : Models.ISubjectWithLinks
        {
            List<QlanirEntities.Subject> res = new List<Subject>();

            if (Model.Tags != null)
                UpdateLinkedTags(ctx, Enty, Model);
            if (Model.Contacts != null)
                UpdateLinkedContacts(ctx, Enty, Model);
            /*if (Model.Offers != null)
                res.AddRange(UpdateLinkedOffers(ctx, Enty, Model));*/
            if (Model.People != null)
                res.AddRange(UpdateLinkedPeople(ctx, Enty, Model));
            if (Model.Organizations != null)
                res.AddRange(UpdateLinkedOrganizations(ctx, Enty, Model));

            return res;
        }

        public Models.Subject Convert<T>(string User, T Enty) where T : Models.Subject, new()
        {
            Models.Subject convertedSubject = null;
            if (Enty is Models.Person)
                convertedSubject = new Models.Organization();
            else if (Enty is Models.Organization)
                convertedSubject = new Models.Person();
            else
                throw new ArgumentOutOfRangeException("Convet supports only Person and Organization models!");

            convertedSubject.Label = Enty.Label;
            ((Models.ISubjectWithLinks)convertedSubject).Contacts = ((Models.ISubjectWithLinks)Enty).Contacts;
            ((Models.ISubjectWithLinks)convertedSubject).Tags = ((Models.ISubjectWithLinks)Enty).Tags;

            foreach (var contact in ((Models.ISubjectWithLinks)convertedSubject).Contacts)
                contact.Id = 0;

            Delete<T>(Enty.Id, User); //удаление прежнего субъекта

            //сохранение нового субъекта
            if (convertedSubject is Models.Person)
                return Update(null, User, (Models.Person)convertedSubject);
            else
                return Update(null, User, (Models.Organization)convertedSubject);
        }

        public void Import<T>(String OwnerEmail, string User, T Enty) where T : Models.Subject, new()
        {
            if (GetUserId(User) != -1) //проверка на системного юзера
                return;

            String ownerName;
            using (var ctx = new QlanirEntities.QlanirEntitySet()) //search owner
            {
                var systemEmail = ctx.Subjects.OfType<QlanirEntities.Identifier>().FirstOrDefault(f => f.TagId == (int)qlanir_web_api.Models.TagTypes.SystemEmail && f.Value == OwnerEmail);

                if (systemEmail == null)
                    return;

                var owner = ctx.Users.FirstOrDefault(f => f.SubjectId == systemEmail.OwnerId);

                if (owner == null)
                    return;

                //если owner или systemEmail не найдены импорт субъекта не производится
                ownerName = owner.Name;
            }

            if (Enty is Models.ISubjectWithLinks)
            {
                Models.Tag[] postTags = new Models.Tag[] { new Models.Tag { Id = -602 }, new Models.Tag { Id = -601 } }; //метки: 'контакт отправлен по почте', 'не подтверждено'

                if (((Models.ISubjectWithLinks)Enty).Tags == null)
                    ((Models.ISubjectWithLinks)Enty).Tags = postTags;
                else
                    ((Models.ISubjectWithLinks)Enty).Tags = ((Models.ISubjectWithLinks)Enty).Tags.Union(postTags).ToArray();
            }

            Update(null, ownerName, Enty);
        }

        public T Update<T>(int? Id, string User, T Subject) where T : Models.Subject, new()
        {
            using (var ctx = new QlanirEntities.QlanirEntitySet())
            {
                var owner = (int)GetUserId(User, ctx);

                Subject.Owner = owner;

                var subjectEnty = GetOrCreateSubjectEnty<T>(ctx, Id, User, Subject);

                bool withLinks = Subject is Models.ISubjectWithLinks;

                IEnumerable<Subject> linkedSubjects = null;
                IEnumerable<Subject> offerLinkedSubjects = new List<Subject>();

                if (withLinks)
                {
                    linkedSubjects = UpdateSubjectEntyLinks(ctx, subjectEnty, (Models.ISubjectWithLinks)Subject);

                    if (((Models.ISubjectWithLinks)Subject).Offers != null)
                        offerLinkedSubjects = UpdateLinkedOffers(ctx, subjectEnty, (Models.ISubjectWithLinks)Subject);

                    ctx.SaveChanges();

                    foreach (var linkedSubject in linkedSubjects)
                        ctx.Refresh(System.Data.Entity.Core.Objects.RefreshMode.StoreWins, linkedSubject);

                    foreach (var linkedSubject in offerLinkedSubjects)
                        ctx.Refresh(System.Data.Entity.Core.Objects.RefreshMode.StoreWins, linkedSubject);
                }
                else
                {
                    ctx.SaveChanges();
                }

                ctx.Refresh(System.Data.Entity.Core.Objects.RefreshMode.StoreWins, subjectEnty);

                var alreadyIndexed = Index(subjectEnty, withLinks);

                //если в linkedSubject есть замененный исполнитель предложения услуги, который привязан к subjectEnty, то он индексируется в методе выше, но почему-то связь с предложением у него не пропадает, только есть он проиндексируется тут всё нормально.
                foreach (var linkedSubject in offerLinkedSubjects.Distinct())
                {
                    Index(linkedSubject, withLinks);
                }

                //Index only prfofile linked to Subject via Proxy !!!
                foreach (var linkedSubject in linkedSubjects.Distinct().Where(p => p.Id == owner && !alreadyIndexed.Contains(p.Id)))
                {
                    Index(linkedSubject, withLinks);
                }

                return Get<T>(subjectEnty.Id, User);
            }
        }

        public void Delete<T>(int Id, string User) where T : Models.Subject, new()
        {


            using (var ctx = new QlanirEntities.QlanirEntitySet())
            {
                var owner = (int)GetUserId(User, ctx);

                IEnumerable<int?> offersIds = null;

                if (typeof(T) == typeof(Models.Offer))
                {
                    var eventsToDelete = ctx.Events.Where(p => p.Id == Id);

                    var evt = eventsToDelete.FirstOrDefault();

                    foreach (var tag in evt.Tags.ToArray())
                    {
                        evt.Tags.Remove(tag);
                    }

                    ctx.Events.DeleteObject(evt);
                }
                else
                {

                    var enty = ctx.Subjects.FirstOrDefault(p => p.Id == Id && (p.OwnerId == owner || p.Id == owner));

                    if (enty == null)
                    {
                        throw new ObjectNotFoundException();
                    }

                    var eventsToDelete = enty.Links.Select(p => p.Event);

                    offersIds = enty.Links.Where(p => p.Event.TypeId == (int)qlanir_web_api.Models.EventTypes.Resource).Select(s => s.EventId).ToArray();

                    foreach (var evt in eventsToDelete.ToArray())
                    {
                        //remove tags (not removed by trigger)
                        foreach (var tag in evt.Tags.ToArray())
                            evt.Tags.Remove(tag);

                        //remove events (not removed by trigger)
                        ctx.Events.DeleteObject(evt);
                    }

                    ctx.Subjects.DeleteObject(enty);
                }

                ctx.SaveChanges();

                using (var elastic = new ElasticDataContext.ElasticDataContext())
                {
                    if (offersIds != null)
                        foreach (int offerId in offersIds)
                            elastic.Delete(offerId, "offer", owner);

                    elastic.Delete(Id, GetSubjectType<T>(), owner);
                }

                using (var garph = new GraphDataContext.GraphDataContext())
                {
                    if (offersIds != null)
                        foreach (int offerId in offersIds)
                            garph.DeleteNode(offerId, "offer");

                    garph.DeleteNode(Id, GetSubjectType<T>());
                }



            }

        }

        string GetSubjectType<T>()
        {
            if (typeof(T) == typeof(Models.Offer))
            {
                return "offer";
            }
            else if (typeof(T) == typeof(Models.Person))
            {
                return "person";
            }
            else if (typeof(T) == typeof(Models.Organization))
            {
                return "organization";
            }
            else if (typeof(T) == typeof(Models.Contact))
            {
                return "contact";
            }
            else
            {
                throw new ArgumentOutOfRangeException("Type not found");
            }
        }

        public void UpdateLink(int SubjectId, int LinkedSubjectId, string LinkedSubjectType, string User, Models.Tag LinkTag)
        {
            using (var ctx = new QlanirEntities.QlanirEntitySet())
            {
                var owner = (int)GetUserId(User, ctx);

                QlanirEntities.Subject subjectEnty = null;

                subjectEnty = ctx.Subjects.FirstOrDefault(p => p.Id == SubjectId && (p.OwnerId == owner || p.Id == owner));

                if (subjectEnty == null)
                {
                    throw new ObjectNotFoundException();
                }

                if (LinkedSubjectType == "contact")
                {
                    var linkedLinkEnty = ctx.Events.Include("Links.Event.Links.Subject.TagMaps.Tag").SelectMany(p => p.Links)
                        .Where(p => p.SubjectId == LinkedSubjectId).Where(p => p.Event.Links.Any(s => s.SubjectId == SubjectId))
                        .ToArray().FirstOrDefault();

                    ((QlanirEntities.Identifier)linkedLinkEnty.Subject).TagId = LinkTag.Id;
                }
                else if (LinkedSubjectType == "person")
                {
                    var linkedLinkEnty = ctx.Events.Include("Links.Event.Links.Subject.TagMaps.Tag").SelectMany(p => p.Links)
                        .Where(p => p.SubjectId == LinkedSubjectId).Where(p => p.Event.Links.Any(s => s.SubjectId == SubjectId))
                        .ToArray().FirstOrDefault();

                    linkedLinkEnty.RoleId = LinkTag.Id;

                }
                else if (LinkedSubjectType == "organization")
                {
                    var linkedLinkEnty = ctx.Events.Include("Links.Event.Links.Subject.TagMaps.Tag").SelectMany(p => p.Links)
                        .Where(p => p.SubjectId == LinkedSubjectId).Where(p => p.Event.Links.Any(s => s.SubjectId == SubjectId))
                        .ToArray().FirstOrDefault();

                    linkedLinkEnty.RoleId = LinkTag.Id;

                }


                ctx.SaveChanges();

                ctx.Refresh(System.Data.Entity.Core.Objects.RefreshMode.StoreWins, subjectEnty);

                Index(subjectEnty, true);

                //return Get<T>(subjectEnty.Id, User);
            }

        }


    }
}
