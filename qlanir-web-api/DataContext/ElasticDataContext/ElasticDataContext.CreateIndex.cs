﻿using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qlanir_web_api.DataContext.ElasticDataContext
{
    partial class ElasticDataContext
    {
        public void CreateIndex()
        {
            var indexFile = Resource.subjects_index;

            var obj = Newtonsoft.Json.JsonConvert.DeserializeObject(indexFile);

            var res = _client.Raw.IndicesCreate("subjects", new [] { obj });

            //System.Diagnostics.Debug.WriteLine(System.Text.Encoding.UTF8.GetString(res.Request));

            ///

            indexFile = Resource.names_index;

            obj = Newtonsoft.Json.JsonConvert.DeserializeObject(indexFile);

            res = _client.Raw.IndicesCreate("names", new[] { obj });

            //System.Diagnostics.Debug.WriteLine(System.Text.Encoding.UTF8.GetString(res.Request));

        }
    }
}
