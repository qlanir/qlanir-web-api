﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qlanir_web_api.DataContext.ElasticDataContext
{
    public class SerachResult
    {
        public IDictionary<string, int> TypeTotalCounts { get; set; }

        public IEnumerable<SearchResultItem> Results { get; set; }
    }

    public class SearchResultItem
    {
        public int DocId { get; set; }

        public string DocType { get; set; }

        public int HitsDocId { get; set; }

        public string HitsDocType { get; set; }        

        public IEnumerable<SearchResultHit> Hits { get; set; }
    }

    public class SearchResultHit
    {
        public string Type { get; set; }

        public string Hit { get; set; }
    }
}
