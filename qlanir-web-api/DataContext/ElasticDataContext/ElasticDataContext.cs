﻿using Nest;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qlanir_web_api.DataContext.ElasticDataContext
{
    public partial class ElasticDataContext : IDisposable
    {
        ElasticClient _client = null;

        public ElasticDataContext()
        {
            var elasticUri = ConfigurationManager.AppSettings.Get("ELASTIC_URI");

            var uri = new Uri(elasticUri);
            var settings = new ConnectionSettings(uri).SetDefaultIndex("subjects");
            _client = new ElasticClient(settings);

        }
        /*
        private string GetSubjectType(Models.Subject Subject)
        {
            return Subject.Type;
        }
         */

        public void Dispose()
        {
        }
    }
}
