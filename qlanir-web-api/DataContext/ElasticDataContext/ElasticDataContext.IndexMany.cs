﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qlanir_web_api.DataContext.ElasticDataContext
{
    partial class ElasticDataContext
    {
        private void IndexNames(IEnumerable<object> docs, string Type)
        {
            IndexMany(docs, "names", Type);
        }

        private void IndexSubjects(IEnumerable<object> docs, string Type)
        {
            IndexMany(docs, "subjects", Type);
        }

        private void IndexMany(IEnumerable<object> docs, string Index, string Type) 
        {
            if (docs.Count() > 0)
            {
                /*
                var prms = new Nest.SimpleBulkParameters { Refresh = true };

                this._client.IndexMany(docs, Index, Type, prms);
                 */
                this._client.Bulk((p) => p.IndexMany(docs, (s, obj) => s.Index(Index).Type(Type)).Refresh());
            }
        }
    }
}
