﻿using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qlanir_web_api.DataContext.ElasticDataContext
{
    partial class ElasticDataContext
    {
        public IEnumerable<Models.Tag> SearchNames(string User, DataContextFilter Filter)
        {
            if (Filter == null || string.IsNullOrEmpty(Filter.Term))
                return new Models.Tag[0];

            var owner = SubjectsDataContext.GetUserId(User);

            var query = Query<object>.QueryString(q => q.Query(string.Format("label:{0}~0.7", Filter.Term))) && (Query<object>.Term("owner", owner) || Query<object>.Term("owner", 0));

            var result = this._client.Search<Models.Tag>(s => s.Index("names").Type(Filter.Type).Query(query).Sort(p => p.OnField("label").Ascending()).Take(20));

            return result.Hits != null ? result.Hits.Select(s => new Models.Tag(s.Type, s.Source)).OrderBy(p => p.Label).ToArray()  : new Models.Tag[0];
        }
    }
}
