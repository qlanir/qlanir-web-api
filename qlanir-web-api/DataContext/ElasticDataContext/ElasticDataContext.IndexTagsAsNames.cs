﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qlanir_web_api.DataContext.ElasticDataContext
{
    partial class ElasticDataContext
    {       
        public void IndexTagsAsNames(string NameType, IEnumerable<Models.Tag> Tags)
        {
         
            foreach(var tag in Tags)
            {
                DeleteName(tag.Id, NameType, tag.Owner);
            }

            var docs = Tags.Select(p => {
                var label = p.Label.Remove(0, p.Label.IndexOf('/') + 1).Trim();
                return new { id = p.Id, label = label, owner = p.Owner };
            });

            this.IndexNames(docs, NameType);

        }

    }
}
