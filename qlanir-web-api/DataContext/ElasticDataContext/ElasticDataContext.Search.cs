﻿using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qlanir_web_api.DataContext.ElasticDataContext
{
    partial class ElasticDataContext
    {

        public SerachResult Search(int Owner, DataContextFilter Filter)
        {

            int peopleTotalCount = GetCount(Owner, "person", Filter.Term);
            int orgsTotalCount = GetCount(Owner, "organization", Filter.Term);
            int servicesTotalCount = GetCount(Owner, "offer", Filter.Term);

            IEnumerable<SearchResultItem> res = null;

            if (Filter.Type == "person" || Filter.Type == "organization" || Filter.Type == "offer")
            {
                res = GetTypeResultItem(Owner, Filter);
            }

            var dict = new Dictionary<string, int> { { "person", peopleTotalCount }, { "organization", orgsTotalCount }, { "offer", servicesTotalCount } };

            return new SerachResult
            {
                TypeTotalCounts = dict,
                Results = res
            };
        }

        private int GetCount(int Owner, string Type, string Term)
        {
            var query = Query<object>.Term("owner", Owner);

            var labelQuery = Query<object>.Term("field", "label") || Query<object>.Term("field", "tag");

            query = query && labelQuery;

            if (!string.IsNullOrEmpty(Term))
            {
                var termQuery = Query<object>.QueryString(q => q.Query(string.Format("val.autocomplete:{0}~0.7", Term)));

                query = query && termQuery;
            }

            var result = this._client.Search<dynamic>(s => s.Index("subjects").Type(Type).Query(query).Take(0).FacetTerm("_doc", (p) => p.OnField("_doc").Size(10000)));
            
            return ((TermFacet)result.Facets["_doc"]).Items.Count();
        }

        private IEnumerable<SearchResultItem> GetTypeResultItem(int Owner, DataContextFilter Filter)
        {
            var query = Query<object>.Term("owner", Owner);

            bool isTerm = Filter != null && !string.IsNullOrEmpty(Filter.Term);

            if (isTerm)            
            {
                var termQuery = Query<object>.QueryString(q => q.Query(string.Format("val.autocomplete:{0}~0.7", Filter.Term)));

                query = query && termQuery;

            }
            else
            {
                var labelTypeQuery = Query<object>.Term("field", "label");
                query = query && labelTypeQuery;
            }
            

            var result = this._client.Search<dynamic>((s) => {
                var q = s.Index("subjects").Type(Filter.Type).Query(query);
                if (!isTerm)
                {
                    q = q.Filter(p => p.Script(x => x.Script("_source._doc == _source.doc")));
                    q = q.Sort(p => p.OnField("created").Descending());

                }
                else
                {
                    q = q.Sort(p => p.OnField("_score").Descending());
                }

                return q.Take(20).Highlight(h => h.OnFields(f => f.OnField("val.autocomplete")));
            });

#if DEBUG
            System.Diagnostics.Debug.WriteLine(System.Text.Encoding.UTF8.GetString(result.ConnectionStatus.Request));
#endif

            var hts = result.Highlights;

            if (hts.Count > 0)
            {
                var r = hts.Select(s =>
                {

                    var ht = s.Value["val.autocomplete"];
                    var doc = result.Hits.First(p => p.Id == ht.DocumentId);
                    var src = doc.Source;


                    return new SearchResultItem
                    {
                        DocId = src._doc,
                        DocType = doc.Type,
                        HitsDocId = src.doc,
                        HitsDocType = src.type,
                        Hits = ht.Highlights.Select(p => new SearchResultHit
                        {
                            Hit = p,
                            Type = src.field == "label" ? src.type : "tag"
                        })
                    };
                });



                return r.ToArray();
            }
            else
            {
                if (result.Hits != null)
                {
                    return result.Hits.Select(s =>
                    {
                        return new SearchResultItem
                        {
                            DocId = s.Source._doc,
                            DocType = s.Type,
                            HitsDocId = s.Source.doc,
                            HitsDocType = s.Source.type,
                            Hits = new SearchResultHit[0]
                        };
                    });
                }
                else
                {
                    return new SearchResultItem[0];
                }
            }

        }
    }
}
