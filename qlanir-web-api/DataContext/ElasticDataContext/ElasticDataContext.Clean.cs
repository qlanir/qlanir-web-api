﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qlanir_web_api.DataContext.ElasticDataContext
{
    partial class ElasticDataContext
    {
        public void Clean()
        {
            _client.DeleteIndex(p => p.Index("subjects"));
            _client.DeleteIndex(p => p.Index("names"));
        }

        public static void CleanAll()
        {
            using (var elastic = new ElasticDataContext())
            {
                elastic.Clean();
            }
        }
    }
}
