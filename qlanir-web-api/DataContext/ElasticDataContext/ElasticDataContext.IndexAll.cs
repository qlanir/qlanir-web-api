﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qlanir_web_api.DataContext.ElasticDataContext
{
    partial class ElasticDataContext
    {
        public static void Index(bool Clean = true)
        {
            using (var elastic = new ElasticDataContext())
            {
                elastic.IndexAll(Clean);
            }
        }

        public void IndexAll(bool Clean = true)
        {
            if (Clean)
            {
                this.Clean();
                this.CreateIndex();
            }

            using (var ctx = new QlanirEntities.QlanirEntitySet())
            {
                using (var grp = new GraphDataContext.GraphDataContext())
                {
                    foreach (var entySubj in ctx.Subjects)
                    {
                        if (entySubj.Id == -1) continue;

                        Models.ISubjectWithLinks subject = null;

                        if (entySubj is QlanirEntities.Person)
                        {
                            subject = grp.GetSubject<Models.Person>(entySubj.Id);
                        }
                        else if (entySubj is QlanirEntities.Company)
                        {
                            subject = grp.GetSubject<Models.Organization>(entySubj.Id);
                        }
                        
                        if (subject != null)
                        {
                            var owner = entySubj.OwnerId ?? entySubj.Id;
                            this.Index(owner, IndexActionType.Create, subject);
                        }

                    }

                    foreach (var entySubj in ctx.Events.Where(p => p.TypeId == (int)Models.EventTypes.Resource))
                    {
                        var subject = grp.GetSubject<Models.Offer>(entySubj.Id);

                        if (subject != null)
                        {
                            var owner = entySubj.OwnerId ?? entySubj.Id;
                            this.Index(owner, IndexActionType.Create, subject);
                        }
                    }

                    this.IndexNames(ctx);
                }
            }

        }

        private void IndexNames(QlanirEntities.QlanirEntitySet ctx)
        {
            var idents = ctx.Tags.Where(p => p.ParentId == (int)Models.TagTypes.Ident).Select(p => new Models.Tag { Id = p.Id, Label = p.Label, Owner = p.OwnerId ?? 0 }).ToArray();
            this.IndexTagsAsNames("ident", idents);

            var roles = ctx.Roles.Where(p => p.Id < 0).Select(p => new Models.Tag { Id = p.Id, Label = p.Label, Owner = p.OwnerId ?? 0 }).ToArray();
            this.IndexTagsAsNames("role", roles);

            var tags = ctx.Tags.Select(p => new Models.Tag { Id = p.Id, Label = p.Label, Owner = p.OwnerId ?? 0 }).ToArray();
            this.IndexTagsAsNames("tag", tags);


        }

    }
}
