﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qlanir_web_api.DataContext.ElasticDataContext
{
    partial class ElasticDataContext
    {
        private IEnumerable<object> GetCreateDocs(int _doc, Models.ISubject Subject, bool WithTags = true)
        {
            var docs = new List<object>();

            var labeldoc = new
            {
                _doc = _doc,
                owner = Subject.Owner,
                doc = Subject.Id,
                type = Subject.Type,
                field = "label",
                val = Subject.Label,
                created  = Subject.Created
            };


            if (!string.IsNullOrEmpty(labeldoc.val))
                docs.Add(labeldoc);

            if (Subject is Models.Offer)
            {
                var labeltagdoc = new
                {
                    _doc = _doc,
                    owner = Subject.Owner,
                    doc = Subject.Id,
                    type = Subject.Type,
                    field = "label",
                    val = ((Models.Offer)Subject).Tags[0].Label,
                    created = Subject.Created
                };

                if (!string.IsNullOrEmpty(labeltagdoc.val))
                    docs.Add(labeltagdoc);
            }
            else
            {
                if (WithTags)
                {
                    if (Subject is Models.ISubjectWithLinks)
                    {
                        foreach (var tag in ((Models.ISubjectWithLinks)Subject).Tags)
                        {
                            var tagdoc = new
                            {
                                _doc = _doc,
                                owner = Subject.Owner,
                                doc = tag.Id,
                                type = Subject.Type,
                                field = "tag",
                                val = tag.Label
                            };

                            docs.Add(tagdoc);
                        }
                    }
                }
            }

            return docs;
        }


        public void Index(int Owner, IndexActionType IndexActionType, Models.ISubjectWithLinks Subject)
        {           
            if (IndexActionType == IndexActionType.Create)
            {
                Delete(Subject.Id, Subject.Type, Owner);

                var docs = new List<object>();
                var subDocs = new List<object>();

                docs.AddRange(GetCreateDocs(Subject.Id, Subject));

                foreach (var link in Subject.People)
                {
                    docs.AddRange(GetCreateDocs(Subject.Id, link.Subject));

                    subDocs.Clear();
                    subDocs.AddRange(GetCreateDocs(link.Subject.Id, Subject, false));
                    this.IndexSubjects(subDocs, "person");
                }

                foreach (var link in Subject.Organizations)
                {
                    docs.AddRange(GetCreateDocs(Subject.Id, link.Subject));

                    subDocs.Clear();
                    subDocs.AddRange(GetCreateDocs(link.Subject.Id, Subject, false));
                    this.IndexSubjects(subDocs, "organization");
                }

                foreach (var link in Subject.Offers)
                {
                    var servSubj = new Models.OfferBase { Id = link.Subject.Id, Label = link.Subject.Label, Owner = link.Subject.Owner };
                    docs.AddRange(GetCreateDocs(Subject.Id, servSubj));

                    subDocs.Clear();
                    subDocs.AddRange(GetCreateDocs(link.Subject.Id, Subject, false));
                    this.IndexSubjects(subDocs, "offer");

                }

                this.IndexSubjects(docs, Subject.Type);

                this.IndexTagsAsNames("tag", Subject.Tags.Where(p => p.Id > 0));

                this.IndexSubjectsAsNames(Subject);
            }
        }

        private void IndexSubjectsAsNames(Models.ISubjectWithLinks Subject)
        {
            if (string.IsNullOrEmpty(Subject.Label))
                return;

            var peopleNames = new List<Models.Tag>();
            if (Subject.Type == "person")
                peopleNames.Add(new Models.Tag { Id = Subject.Id, Label = Subject.Label, Owner = Subject.Owner });            
            //peopleNames.AddRange(Subject.People.Select(p => new Models.Tag { Id = p.Subject.Id, Label = p.Subject.Label, Owner = p.Subject.Owner }));

            this.IndexTagsAsNames("person", peopleNames);

            var orgNames = new List<Models.Tag>();
            if (Subject.Type == "organization")
                orgNames.Add(new Models.Tag { Id = Subject.Id, Label = Subject.Label, Owner = Subject.Owner });
            //orgNames.AddRange(Subject.Organizations.Select(p => new Models.Tag { Id = p.Subject.Id, Label = p.Subject.Label, Owner = p.Subject.Owner }));

            this.IndexTagsAsNames("organization", orgNames);
        }

        private IEnumerable<KeyValuePair<string, string>> GetTerms(Models.Subject Subject)
        {
            List<KeyValuePair<string, string>> res = new List<KeyValuePair<string, string>>();

            res.Add(new KeyValuePair<string, string>(Subject.Label, "label"));

            return res.ToArray();
        }

        private object GetContent(int Owner, Models.Subject Subject)
        {
            var terms = GetTerms(Subject);

            return new { terms = terms.Select(p => new { val = p.Key, type = p.Value }).ToArray(), owner = Owner };
        }

    }
}
