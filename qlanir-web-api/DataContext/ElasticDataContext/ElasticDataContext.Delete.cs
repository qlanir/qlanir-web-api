﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qlanir_web_api.DataContext.ElasticDataContext
{
    partial class ElasticDataContext
    {
        public void Delete(int Id, string Type, int Owner)
        {
            var q = new {query = new {
                @bool = new
                {
                    must = new dynamic[] {
                    new { term = new { owner = new { value = Owner } } }, 
                    new { @bool = new { should = new dynamic [] {
                        new {term = new { _doc = new { value = Id} } },
                        new {term = new { doc = new { value = Id} } },
                        }
                    } } 
                }
                }

            }};

            var result = this._client.Raw.DeleteByQuery("subjects", q);
            

#if DEBUG
            System.Diagnostics.Debug.WriteLine(System.Text.Encoding.UTF8.GetString(result.Request));
#endif

            DeleteName(Id, Type, Owner);
    }

    public void DeleteName(int Id, string Type, int Owner)
    {
        //
        if (Type != null)
        {
            var q = new
            {
                query = new
                {
                    @bool = new
                    {
                        must = new dynamic[] {
                        new { term = new { owner = new { value = Owner } } },                          
                        new {term = new { id = new { value = Id} } } }
                    }
                }
            };

            var result = this._client.Raw.DeleteByQuery("names", Type, q);

#if DEBUG
            System.Diagnostics.Debug.WriteLine(System.Text.Encoding.UTF8.GetString(result.Request));
#endif
            }
        }
    }
}
