﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qlanir_web_api.DataContext
{
    partial class SubjectsDataContext
    {
        public static QlanirEntities.Subject CreateNewSubject(QlanirEntities.QlanirEntitySet ctx, int ownerId, String name, String type)
        {
            QlanirEntities.Subject newSubject = null;
            if (type == "person")
            {
                newSubject = new QlanirEntities.Person { OwnerId = ownerId };

                var spts = name.Trim().Split(' ');
                ((QlanirEntities.Person)newSubject).LastName = spts[0].Trim();
                if (spts.Length > 1)
                    ((QlanirEntities.Person)newSubject).FirstName = spts[1].Trim();
            }
            else if (type == "organization")
                newSubject = new QlanirEntities.Company { OwnerId = ownerId, Name = name };

            ctx.Subjects.AddObject(newSubject);
            ctx.SaveChanges();
            ctx.Refresh(System.Data.Entity.Core.Objects.RefreshMode.StoreWins, newSubject);
            Index(newSubject, false);

            return newSubject;

        }

        public static QlanirEntities.Role CreateNewRole(QlanirEntities.QlanirEntitySet ctx, int ownerId, Models.Tag role)
        {
            QlanirEntities.Role newRole = new QlanirEntities.Role { OwnerId = ownerId, Name = role.Label };
            ctx.Roles.AddObject(newRole);
            ctx.SaveChanges();
            ctx.Refresh(System.Data.Entity.Core.Objects.RefreshMode.StoreWins, newRole);
            return newRole;

        }
    }
}
