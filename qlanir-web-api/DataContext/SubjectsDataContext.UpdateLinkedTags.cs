﻿using QlanirEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qlanir_web_api.DataContext
{
    partial class SubjectsDataContext
    {
        private void UpdateLinkedTags<T>(QlanirEntities.QlanirEntitySet ctx, QlanirEntities.Subject Enty, T Model) where T : Models.ISubjectWithLinks
        {
            if (Model.Tags == null) //see commit for QL-122
                return;

            //delete all tags links
            foreach (var tagMap in Enty.TagMaps.ToArray())
                ctx.TagMaps.DeleteObject(tagMap);

            //1. create tags
            foreach (var tag in Model.Tags)
            {
                QlanirEntities.Tag t;

                if (tag.Id == 0)
                {
                    if (string.IsNullOrEmpty(tag.Label))
                    {
                        throw new ArgumentException("For tag either Id or Label should be defined");
                    }
                    else
                    {
                        t = ctx.Tags.FirstOrDefault(p => p.Label.ToLower() == tag.Label.ToLower() && (p.OwnerId == Model.Owner || p.Id < 0));

                        if (t == null)
                        {
                            t = new QlanirEntities.Tag { Name = tag.Label.ToLower(), OwnerId = Model.Owner };

                            ctx.AddToTags(t);
                        }
                    }
                }
                else
                {
                    t = ctx.Tags.FirstOrDefault(p => p.Id == tag.Id && (p.OwnerId == Model.Owner || p.Id < 0));
                  
                    if (t == null)
                    {
                        throw new ArgumentException("Tag not found");
                    }
                }

                Enty.TagMaps.Add(new TagMap { Subject = Enty, Tag = t });
            }
        }
    }
}
