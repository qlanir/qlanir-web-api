﻿using qlanir_web_api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qlanir_web_api.DataContext
{
    partial class SubjectsDataContext
    {

        private Dictionary<int, List<int>> _mutualRelations;

        private Dictionary<int, List<int>> MutualRelations
        {
            get
            {
                if (_mutualRelations == null)
                {
                    _mutualRelations = new Dictionary<int, List<int>>();
                    _mutualRelations.Add((int)EventTypes.Owner, new List<int> { (int)RoleTypes.Owner, (int)RoleTypes.Ownership }); //владение - {владелец, во-владении}
                    _mutualRelations.Add((int)EventTypes.Registration, new List<int> { (int)RoleTypes.Founder, (int)RoleTypes.Foundation }); //регистрация - {учредитель, учреждение}
                }

                return _mutualRelations;
            }
        }


        public int GetEventId(int roleId)
        {
            int? evtId = MutualRelations.Where(w => w.Value.Contains(roleId)).Select(s => s.Key).FirstOrDefault();
            return evtId == 0 ? (int)EventTypes.Work : (int)evtId;
        }

        public int GetEventId(int role1Id, int role2Id)
        {
            int? evtId = MutualRelations.Where(w => w.Value.Contains(role1Id) && w.Value.Contains(role2Id)).Select(s => s.Key).FirstOrDefault();
            return evtId == 0 ? (int)EventTypes.Work : (int)evtId;
        }

        public int? GetRoleId(int eventId, int roleId)
        {
            var roles = MutualRelations.Where(w => w.Key == eventId && w.Value.Contains(roleId)).Select(s => s.Value).FirstOrDefault();

            if (roles != null)
                return roles.FirstOrDefault(f => f != roleId);

            return null;
        }
    }
}
