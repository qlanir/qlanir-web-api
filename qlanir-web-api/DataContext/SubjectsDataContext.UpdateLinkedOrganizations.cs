﻿using QlanirEntities;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qlanir_web_api.DataContext
{
    partial class SubjectsDataContext
    {
        private IEnumerable<QlanirEntities.Subject> UpdateLinkedOrganizations<T>(QlanirEntities.QlanirEntitySet ctx, QlanirEntities.Subject Enty, T Model) where T : Models.ISubjectWithLinks
        {
            var linkedSubjects = new List<QlanirEntities.Subject>();

            foreach (var org in Model.Organizations)
            {
                int owner = Model.Owner;

                var subjectEnty = Enty;
                var linkedSubjectEnty = ctx.Subjects.FirstOrDefault(p => p.Id == org.Subject.Id && (p.OwnerId == owner || p.Id == owner));

                if (linkedSubjectEnty == null)
                {
                    linkedSubjectEnty = new Company { Name = org.Subject.Label, OwnerId = owner };
                    ctx.Subjects.AddObject(linkedSubjectEnty);
                }

                if (subjectEnty == null || linkedSubjectEnty == null)
                {
                    throw new ObjectNotFoundException();
                }

                if (org.Id == 0)
                {

                    var evt = new QlanirEntities.Event { OwnerId = owner };
                    var link1 = new QlanirEntities.Link { OwnerId = owner, Event = evt };
                    var link2 = new QlanirEntities.Link { OwnerId = owner, Event = evt };

                    link1.Subject = subjectEnty;
                    link2.Subject = linkedSubjectEnty;

                    evt.TypeId = GetEventId(link1.RoleId);
                    link1.RoleId = org.Tag != null ? org.Tag.Id : (int)Models.RoleTypes.Employee;
                    var orgRole = GetRoleId(evt.TypeId, link1.RoleId);
                    link2.RoleId = orgRole == null ? (int)Models.RoleTypes.Employer : (int)orgRole;

                    ctx.Events.AddObject(evt);
                    ctx.Links.AddObject(link1);
                    ctx.Links.AddObject(link2);
                }
                else
                {
                    var link = ctx.Links.FirstOrDefault(p => p.Id == org.Id);

                    if (link == null)
                    {
                        throw new ObjectNotFoundException();
                    }

                    link.Subject = linkedSubjectEnty;
                }

                linkedSubjects.Add(linkedSubjectEnty);
            }

            return linkedSubjects;
        }
    }
}
