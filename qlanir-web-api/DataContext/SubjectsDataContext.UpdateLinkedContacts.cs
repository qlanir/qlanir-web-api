﻿using QlanirEntities;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qlanir_web_api.DataContext
{
    partial class SubjectsDataContext
    {
        private void UpdateLinkedContacts<T>(QlanirEntities.QlanirEntitySet ctx, QlanirEntities.Subject Enty, T Model) where T : Models.ISubjectWithLinks
        {
            foreach (var contactLink in Model.Contacts)
            {

                int owner = Model.Owner;
                var subjectEnty = Enty;

                var identRoleId = contactLink.Tag != null ?
                    ctx.Roles.FirstOrDefault(p => p.Id == contactLink.Tag.Id && p.OwnerId == null).Id : (int)qlanir_web_api.Models.RoleTypes.Identifyers;
                var identTagEnty = ctx.Tags.FirstOrDefault(p => p.Id == ((Models.Contact)contactLink.Subject).ContactType.Id && p.OwnerId == null);

                if (subjectEnty == null || identTagEnty == null)
                {
                    throw new ObjectNotFoundException();
                }

                if (contactLink.Id == 0)
                {
                    var identEntry = new QlanirEntities.Identifier { Value = contactLink.Subject.Label, Tag = identTagEnty, OwnerId = owner };

                    var evt = new Event { OwnerId = owner, TypeId = (int)qlanir_web_api.Models.EventTypes.Owner };
                    var link1 = new Link { OwnerId = owner, RoleId = (int)qlanir_web_api.Models.RoleTypes.Owner, Subject = subjectEnty, Event = evt };
                    var link2 = new Link { OwnerId = owner, RoleId = identRoleId, Subject = identEntry, Event = evt };

                    ctx.Subjects.AddObject(identEntry);
                    ctx.Events.AddObject(evt);
                    ctx.Links.AddObject(link1);
                    ctx.Links.AddObject(link2);
                }
                else
                {
                    var link = ctx.Links.Include("Subject").FirstOrDefault(p => p.Id == contactLink.Id);

                    if (link == null)
                    {
                        throw new ObjectNotFoundException();
                    }

                    var ident = (Identifier)link.Subject;

                    ident.Tag = identTagEnty;
                    ident.Value = contactLink.Subject.Label;
                }

            }
        }
    }
}
