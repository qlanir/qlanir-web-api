﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qlanir_web_api.DataContext
{
    public interface IProfileDataContext
    {
        /// <summary>
        /// Create user by email if it doesn't exist
        /// </summary>
        /// <param name="UserEmail"></param>
        /// <returns>Returns -1 if user already exists</returns>
        int CreateUserIfNotExist(string UserLabel, string UserLoginIdentifyer, qlanir_web_api.Models.TagTypes UserLoginIdentifyerType);
    }
}
