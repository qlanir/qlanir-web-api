﻿using QlanirEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qlanir_web_api.DataContext
{
    partial class SubjectsDataContext
    {
        internal static Models.Subject Enty2Model(object Enty)
        {
            if (Enty is QlanirEntities.Event && ((QlanirEntities.Event)Enty).TypeId == (int)Models.EventTypes.Resource)
            {
                var evt = (QlanirEntities.Event)Enty;

                Models.Offer Model = new Models.Offer();
                Model.Id = evt.Id;
                var tag = evt.Tags.FirstOrDefault();//.Tag;
                if (tag != null)
                {
                    Model.Label = tag.Label;
                    return Model;
                }
                else
                {
                    return null;
                }
            }
            if (Enty is QlanirEntities.Person)
            {
                var person = (QlanirEntities.Person)Enty;

                if (person.Id == -1)
                    return null;

                Models.Person Model = new Models.Person();
                Model.Id = person.Id;
                Model.Label = person.Label;


                return Model;
            }
            else if (Enty is QlanirEntities.Company)
            {
                var person = (QlanirEntities.Company)Enty;

                if (person.Id == -1)
                    return null;

                Models.Organization Model = new Models.Organization();
                Model.Id = person.Id;
                Model.Label = person.Label;


                return Model;
            }
            else if (Enty is QlanirEntities.Identifier)
            {
                var person = (QlanirEntities.Identifier)Enty;

                if (person.Id == -1)
                    return null;

                Models.Identifier Model = new Models.Identifier();
                Model.Id = person.Id;
                Model.Label = person.Label;
            }

            return null;
        }

    }
}
