﻿using qlanir_web_api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qlanir_web_api.DataContext
{
    public class ProfileDataContext : IProfileDataContext
    {
        public int CreateUserIfNotExist(string UserLabel, string UserLoginIdentifyer, TagTypes UserLoginIdentifyerType)
        {
            using (var ctx = new QlanirEntities.QlanirEntitySet())
            {
                var userId = SubjectsDataContext.GetUserId(UserLoginIdentifyer, ctx);

                if (userId != 0)
                {
                    //User already exists
                    return -1;
                }

                //Create new user

                //TO DO: Identifying user by contact with Tag Type = Profile, is it good enough ?

                Person person = new Person {
                    Label = UserLabel,
                    Tags = new [] { new Models.Tag { Id = (int)TagTypes.Profile }},
                    Contacts = new [] {
                        new ContactLink {  
                            Subject = new Contact { Label = UserLabel, ContactType = new Tag { Id = (int) UserLoginIdentifyerType } } 
                        }}
                };

                return SubjectsDataContext.CreatePerson(ctx, person, UserLoginIdentifyer);
                
            }
        }
    }
}
