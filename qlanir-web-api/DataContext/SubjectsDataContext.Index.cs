﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qlanir_web_api.DataContext
{
    partial class SubjectsDataContext
    {
     
        internal protected static IEnumerable<int> Index(QlanirEntities.Subject Subject, bool IndexLinks = true)
        {
            Models.ISubjectWithLinks subject = null;
            List<Models.ISubjectWithLinks> linkedSubjects = new List<Models.ISubjectWithLinks>();

            using (var graph = new GraphDataContext.GraphDataContext())
            {
                var linkedIds = graph.Index(Subject, IndexLinks);

                if (Subject is QlanirEntities.Person)
                    subject = graph.GetSubject<Models.Person>(Subject.Id);
                else if (Subject is QlanirEntities.Company)
                    subject = graph.GetSubject<Models.Organization>(Subject.Id);

                linkedSubjects.AddRange(linkedIds.Offers.Select(p => graph.GetSubject<Models.Offer>(p)));
                linkedSubjects.AddRange(linkedIds.People.Select(p => graph.GetSubject<Models.Person>(p)));
                linkedSubjects.AddRange(linkedIds.Orgs.Select(p => graph.GetSubject<Models.Organization>(p)));

            }

            if (subject != null)
            {
                using (var elastic = new ElasticDataContext.ElasticDataContext())
                {
                    elastic.Index(subject.Owner, ElasticDataContext.IndexActionType.Create, subject);

                    foreach (var linkedSubject in linkedSubjects)
                        elastic.Index(subject.Owner, ElasticDataContext.IndexActionType.Create, linkedSubject);
                }
            }

            return linkedSubjects.Select(p => p.Id);
        }
        

    }
}
