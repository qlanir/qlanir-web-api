﻿using QlanirEntities;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qlanir_web_api.DataContext
{
    partial class SubjectsDataContext
    {
        private QlanirEntities.Subject CretateLinkedPerson(QlanirEntities.QlanirEntitySet ctx, QlanirEntities.Subject Enty, Models.Tag EntyRelationRoleTag, Models.PersonBase LinkedPerson, Models.Tag LinkedPersonRelationRoleTag, int owner, int LinkId)
        {

            int LinkedPersonRelationRole = LinkedPersonRelationRoleTag == null ?
                (Enty is QlanirEntities.Person ? (int)qlanir_web_api.Models.RoleTypes.Acquitance : (int)qlanir_web_api.Models.RoleTypes.Employee)
                : LinkedPersonRelationRoleTag.Id;

            int SubjectRelationRole = EntyRelationRoleTag == null ?
                (Enty is QlanirEntities.Person ? (int)qlanir_web_api.Models.RoleTypes.Acquitance : (int)qlanir_web_api.Models.RoleTypes.Employer)
                : EntyRelationRoleTag.Id;

            if (SubjectRelationRole == 0)
            {
                if (string.IsNullOrEmpty(EntyRelationRoleTag.Label))
                {
                    throw new ArgumentException("Role's tag Id or Label must be defined");
                }

                var role = ctx.Roles.FirstOrDefault(p => p.Name.ToLower() == EntyRelationRoleTag.Label.ToLower()
                    && p.ParentId == (int)Models.RoleTypes.Relation);

                //TODO : Create new roles if nothing found

                if (role == null)
                {
                    throw new NotImplementedException("Create new roles if nothing found");
                }

                SubjectRelationRole = role.Id;
            }

            if (LinkedPersonRelationRole == 0)
            {
                if (string.IsNullOrEmpty(LinkedPersonRelationRoleTag.Label))
                {
                    throw new ArgumentException("Role's tag Id or Label must be defined");
                }

                var role = ctx.Roles.FirstOrDefault(p => p.Name.ToLower() == LinkedPersonRelationRoleTag.Label.ToLower()
                    && p.ParentId == (int)Models.RoleTypes.Relation);

                //TODO : Create new roles if nothing found

                if (role == null)
                {
                    throw new NotImplementedException("Create new roles if nothing found");
                }

                LinkedPersonRelationRole = role.Id;
            }


            var subjectEnty = Enty;
            var linkedSubjectEnty = ctx.Subjects.FirstOrDefault(p => p.Id == LinkedPerson.Id && (p.OwnerId == owner || p.Id == owner));

            if (linkedSubjectEnty == null)
            {
                var spts = LinkedPerson.Label.Split(' ');
                var firstName = spts.Length == 1 ? spts[0] : spts[1];
                var lastName = spts.Length > 1 ? spts[0] : null;
                var middleName = spts.Length > 2 ? spts[2] : null;


                linkedSubjectEnty = new Person { FirstName = firstName, LastName = lastName, MiddleName = middleName, OwnerId = owner };
                ctx.Subjects.AddObject(linkedSubjectEnty);
            }

            if (subjectEnty == null || linkedSubjectEnty == null)
            {
                throw new ObjectNotFoundException();
            }

            if (LinkId == 0)
            {
                var evt = new QlanirEntities.Event { OwnerId = owner };
                var link1 = new QlanirEntities.Link { OwnerId = owner, Event = evt };
                var link2 = new QlanirEntities.Link { OwnerId = owner, Event = evt };

                link1.Subject = subjectEnty;
                link2.Subject = linkedSubjectEnty;

                link1.RoleId = SubjectRelationRole;
                link2.RoleId = LinkedPersonRelationRole;

                ctx.Events.AddObject(evt);
                ctx.Links.AddObject(link1);
                ctx.Links.AddObject(link2);

                evt.TypeId = Enty is QlanirEntities.Person ? (int)Models.EventTypes.Relation : GetEventId(link2.RoleId);
            }
            else
            {
                var link = ctx.Links.FirstOrDefault(p => p.Id == LinkId);

                //TODO : Find link should be more obvious
                var subjectLink = link.Event.Links.FirstOrDefault(p => p.Id != link.Id);

                if (link == null || subjectLink == null)
                {
                    throw new ObjectNotFoundException();
                }

                link.RoleId = LinkedPersonRelationRole;
                link.Subject = linkedSubjectEnty;

                subjectLink.RoleId = SubjectRelationRole;

                link.Event.TypeId = Enty is QlanirEntities.Person ? (int)Models.EventTypes.Relation : GetEventId(link.RoleId);
            }

            return linkedSubjectEnty;
        }

        private IEnumerable<QlanirEntities.Subject> UpdateLinkedPeople<T>(QlanirEntities.QlanirEntitySet ctx, QlanirEntities.Subject Enty, T Model) where T : Models.ISubjectWithLinks
        {
            var linkedSubjects = new List<QlanirEntities.Subject>();

            foreach (var personLink in Model.People)
            {
                //when link subject is null, consider it is link to profile
                if (personLink.Subject == null)
                {
                    personLink.Subject = new Models.PersonBase { Id = Model.Owner };
                }

                if (personLink.Proxy == null)
                {
                    var linkedSubjectEnty = CretateLinkedPerson(ctx, Enty, personLink.HostTag, personLink.Subject, personLink.Tag, Model.Owner, personLink.Id);
                    linkedSubjects.Add(linkedSubjectEnty);
                }
                else
                {
                    // Create link between subject and proxy
                    var proxySubjectEnty = CretateLinkedPerson(ctx, Enty, null, personLink.Proxy, null, Model.Owner, personLink.Id);
                    linkedSubjects.Add(proxySubjectEnty);

                    //Create link between linkedSubject and proxy
                    var linkedSubjectEnty = CretateLinkedPerson(ctx, proxySubjectEnty, personLink.HostTag, personLink.Subject, personLink.Tag, Model.Owner, 0);
                    linkedSubjects.Add(linkedSubjectEnty);
                }
            }

            return linkedSubjects;
        }
    }
}
