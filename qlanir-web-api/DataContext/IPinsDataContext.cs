﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qlanir_web_api.DataContext
{
    public interface IPinsDataContext
    {
        IEnumerable<Models.Pin> GetPins(string User);

        Models.Pin GetPin(string User, int Id);

        Models.Pin AddPin(string User, Models.CreatePin Pin);

        Models.Pin UpdatePin(string User, int Id, string Description);

        void RemovePin(string User, int SubjectId);
    }
}
