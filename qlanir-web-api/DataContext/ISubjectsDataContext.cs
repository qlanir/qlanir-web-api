﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qlanir_web_api.DataContext
{
    public interface ISubjectsDataContext        
    {
        T Get<T>(int Id, string User) where T : Models.Subject, new();

        T Update<T>(int? Id, string User, T Enty) where T : Models.Subject, new();

        void Delete<T>(int Id, string User) where T : Models.Subject, new();

        void UpdateLink(int SubjectId, int LinkedSubjectId, string LinkedSubjectType, string User, Models.Tag LinkTag);

        T DeleteSubjectLink<T>(string User, int LinkId, int SubjectId) where T : Models.Subject, new();

        void Import<T>(String OwnerEmail, string User, T Enty) where T : Models.Subject, new();

        Models.Subject Convert<T>(string User, T Enty) where T : Models.Subject, new();
    }

}
