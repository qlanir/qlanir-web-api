﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qlanir_web_api.DataContext
{
    public class TagsDataContext
    {
        public static Models.Tag UpdateLinkedTag(string User, int SubjectId, string Label)
        {
            using (var ctx = new QlanirEntities.QlanirEntitySet())
            {
                var owner = SubjectsDataContext.GetUserId(User, ctx);

                var tag = ctx.Tags.FirstOrDefault(p => p.Label == Label && (p.OwnerId == null || p.OwnerId == owner));

                if (tag == null)
                {
                    tag = new QlanirEntities.Tag { Name = Label, OwnerId = owner };

                    ctx.Tags.AddObject(tag);
                }

                var subject = ctx.Subjects.FirstOrDefault(p => p.Id == SubjectId);

                if (!subject.TagMaps.Any(p => p.Tag.Id == tag.Id))
                    subject.TagMaps.Add(new QlanirEntities.TagMap { Subject = subject, Tag = tag });

                ctx.SaveChanges();

                ctx.Refresh(System.Data.Entity.Core.Objects.RefreshMode.StoreWins, tag);
                ctx.Refresh(System.Data.Entity.Core.Objects.RefreshMode.StoreWins, subject);

                var modelTag = new Models.Tag { Id = tag.Id, Label = tag.Label, Owner = tag.OwnerId ?? 0};

                SubjectsDataContext.Index(subject, true);

                return modelTag;
            }

        }

        public static void RemoveLinkedTag(string User, int SubjectId, string Label)
        {
            using (var ctx = new QlanirEntities.QlanirEntitySet())
            {
                var owner = SubjectsDataContext.GetUserId(User, ctx);

                var tagMap = ctx.TagMaps.FirstOrDefault(p => p.SubjectId == SubjectId && p.Tag.Label == Label && p.Subject.OwnerId == owner);

                if (tagMap == null)
                {
                    throw new ObjectNotFoundException();
                }

                ctx.TagMaps.DeleteObject(tagMap);

                var subject = ctx.Subjects.FirstOrDefault(p => p.Id == SubjectId);

                ctx.SaveChanges();

                SubjectsDataContext.Index(subject, true);
            }
        }

        public static Models.Tag CreateTag(string User, string Label)
        {
            using (var ctx = new QlanirEntities.QlanirEntitySet())
            {
                var owner = SubjectsDataContext.GetUserId(User, ctx);

                var existentTag = ctx.Tags.FirstOrDefault(p => p.Label == Label && p.OwnerId == owner);

                if (existentTag != null)
                {
                    throw new ArgumentException(string.Format("Tag [{0}] already exists", Label));
                }

                var tag = new QlanirEntities.Tag { Name = Label, OwnerId = owner };

                ctx.Tags.AddObject(tag);

                ctx.SaveChanges();

                ctx.Refresh(System.Data.Entity.Core.Objects.RefreshMode.StoreWins, tag);

                var modelTag = new Models.Tag { Id = tag.Id, Label = tag.Label, Owner = tag.OwnerId ?? 0 };

                using (ElasticDataContext.ElasticDataContext etx = new ElasticDataContext.ElasticDataContext())
                {
                    etx.IndexTagsAsNames("tag", new[] { modelTag });
                }

                return modelTag;
            }        
        }

        public static void RemoveTag(string User, int Id)
        {
            using (var ctx = new QlanirEntities.QlanirEntitySet())
            {

                var owner = SubjectsDataContext.GetUserId(User, ctx);

                var tag = ctx.Tags.FirstOrDefault(p => p.Id == Id && p.OwnerId == owner);

                if (tag == null)
                {
                    throw new ObjectNotFoundException();
                }

                foreach (var tagMap in tag.TagMaps.ToArray())
                {
                    ctx.TagMaps.DeleteObject(tagMap);
                }

                ctx.Tags.DeleteObject(tag);

                ctx.SaveChanges();

                using (GraphDataContext.GraphDataContext gtx = new GraphDataContext.GraphDataContext())
                {
                    gtx.DeleteNode(Id, "tag");
                }

                using (ElasticDataContext.ElasticDataContext etx = new ElasticDataContext.ElasticDataContext())
                {
                    etx.Delete(Id, "tag", owner);
                }
            }
        }

        public static int GetTagLinksCount(string User, int Id)
        {
            using (var ctx = new QlanirEntities.QlanirEntitySet())
            {
                var owner = SubjectsDataContext.GetUserId(User, ctx);

                return ctx.TagMaps.Count(p => p.TagId == Id && p.Tag.OwnerId == owner);
            }
        }
    }
}
