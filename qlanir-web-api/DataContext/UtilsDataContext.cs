﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qlanir_web_api.DataContext
{
    static class UtilsDataContext
    {
        /// <summary>
        /// Used in e2e tests, prepare services
        /// </summary>
        internal static void CleanDbAndCreateProfile()
        {
            SubjectsDataContext.CleanAll();

            using (var graph = new GraphDataContext.GraphDataContext())
            {
                graph.IndexAll();
            }

            using (var elastic = new ElasticDataContext.ElasticDataContext())
            {
                elastic.IndexAll();
            }

            var profileLabel = ConfigurationManager.AppSettings.Get("DEV_USER");

            ProfileDataContext profileDataContext = new ProfileDataContext();

            profileDataContext.CreateUserIfNotExist(profileLabel, profileLabel, Models.TagTypes.Email);
        }
    }
}
