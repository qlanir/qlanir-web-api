﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qlanir_web_api.DataContext
{
    public class SearchSubjectsDataContext : ISearchSubjectsDataContext<Models.SearchResult>
    {

        public Models.SearchResult Search(string User, DataContextFilter Filter)
        {
            var userId = (int)SubjectsDataContext.GetUserId(User);

            ElasticDataContext.SerachResult indexResults = null;

            using (var elastic = new ElasticDataContext.ElasticDataContext())
            {
                indexResults = elastic.Search(userId, Filter);
            }

            var subjIds = indexResults.Results.Where(p => p.DocType != "offer").Select(p => p.DocId).Distinct();
            var evtIds = indexResults.Results.Where(p => p.DocType == "offer").Select(p => p.DocId).Distinct();

            using (var ctx = new QlanirEntities.QlanirEntitySet())
            {
                IEnumerable<dynamic> subjs = null;
                if (Filter.Type != "offer")
                {
                    if (Filter.IncludeLinkedTags)
                    {
                        subjs = ctx.Subjects.Include("TagMaps.Tag").Where(p => subjIds.Contains(p.Id)).Select(p =>
                            new { Id = p.Id, Label = p.Label, Tags = p.TagMaps.Select(s => s.Tag.Label) }).ToArray();
                    }
                    else
                    {
                        subjs = ctx.Subjects.Where(p => subjIds.Contains(p.Id)).Select(p =>
                            new { Id = p.Id, Label = p.Label }).ToArray();
                    }
                }
                else
                {
                    subjs = ctx.Events.Include("Tags").Where(p => evtIds.Contains(p.Id)).Select(p => new { Id = p.Id, Label = p.Tags.FirstOrDefault().Label, TypeId = p.Tags.FirstOrDefault().Id }).ToArray();
                }

                var res = indexResults.Results.Select(p =>
                {
                    var subj = subjs.FirstOrDefault(s => s.Id == p.DocId);

                    if (subj == null)
                        return null;

                    var label = subj.Label;
                    var tags = new string[0];
                    try
                    {
                        tags = subj.Tags.ToArray();
                    }
                    catch { }
                    
                    var hits = p.Hits;

                    return new Models.SearchResultItem
                    {
                        Id = Filter.Type == "offer" && subjs.Count(c => c.TypeId == subj.TypeId) > 1 ? subj.TypeId : p.DocId, /*Filter.Type != "offer" ? p.DocId : subj.TypeId,*/ //QL-126
                        Name = label,
                        Type = p.DocType == "offer" && subjs.Count(c => c.TypeId == subj.TypeId) > 1 ? "service" : p.DocType,/*p.DocType == "offer" ? "service" : p.DocType,*/ //QL-126
                        Hits = hits/*.Where(w => Filter.Type != "offer" ? true : p.DocType == "offer" && w.Type != "offer")*/.Select(s => new Models.SearchResultItemHit { Hit = s.Hit, Type = s.Type }).ToArray(),
                        Tags = tags
                    };
                });

                var groups = res.Where(w => w != null).GroupBy(p => p.Id);
                var groupedRes = new List<Models.SearchResultItem>();

                foreach (var grp in groups)
                {
                    var first = grp.First();
                    first.Hits = grp.SelectMany(p => p.Hits).ToArray();
                    groupedRes.Add(first);
                }

                foreach (var g in groupedRes)
                {
                    var docHits = g.Hits.Where(s => (s.Type == g.Type || (s.Type == "offer" && g.Type == "service")) && s.Hit.Replace("<em>", "").Replace("</em>", "") == g.Name).ToArray();                    
                    if (docHits.Count() > 0)
                    {
                        g.Name = docHits.First().Hit;
                        g.Hits = g.Hits.Except(docHits).ToArray();
                    }
                    if (!string.IsNullOrEmpty(Filter.Term))
                        g.Hits = g.Hits.OrderBy(p => p.Hit.Replace("<em>", "").Replace("</em>", "").ToLowerInvariant()).ToArray();
                }

                return new Models.SearchResult
                {
                    Term = Filter != null ? Filter.Term : null,
                    Type = Filter != null ? Filter.Type : null,

                    TotalCount = new Models.SearchResultTotalCount
                    {
                        People = indexResults.TypeTotalCounts["person"],
                        Organizations = indexResults.TypeTotalCounts["organization"],
                        Offers = indexResults.TypeTotalCounts["offer"]
                    },

                    //TODO : Need to order items by boost score (found directly in names score x2, in linked suvject names x1)
                    
                    Results = string.IsNullOrEmpty(Filter.Term) ?
                    groupedRes.ToArray() :
                    groupedRes.OrderBy(p =>  p.Name.Replace("<em>", "").Replace("</em>", "").ToLowerInvariant()).ToArray()
                };
            }
        }
    }
}
