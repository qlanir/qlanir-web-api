﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qlanir_web_api.DataContext
{
    public class PinsDataContext : IPinsDataContext
    {
        public IEnumerable<Models.Pin> GetPins(string User)
        {
            return GetPins(User, 0);
        }

        public Models.Pin GetPin(string User, int PinId)
        {
            return GetPins(User, PinId).FirstOrDefault();
        }

        private IEnumerable<Models.Pin> GetPins(string User, int PinId)
        {
            IEnumerable<Models.Pin> pins;

            int owner;

            using (var ctx = new QlanirEntities.QlanirEntitySet())
            {
                owner = SubjectsDataContext.GetUserId(User, ctx);

                var q = (IQueryable<QlanirEntities.Pin>) ctx.Pins.Include("Subject").Where(p => p.OwnerId == owner);

                if (PinId != 0)
                    q = q.Where(p => p.Id == PinId);

                pins = q.Select(p => new Models.Pin
                {
                    Id = p.Id,
                    Description = p.Description,
                    Subject = new Models.SubjectBase { Id = p.Subject.Id, Label = p.Subject.Label, Owner = p.Subject.OwnerId == null ? p.Id : (int)p.Subject.OwnerId,
                                                       Type = (p.Subject is QlanirEntities.Person ? "person" : (p.Subject is QlanirEntities.Company ? "organization" : "offer"))
                    }
                }).ToArray();

             }

            using(var graph = new GraphDataContext.GraphDataContext())
            {
                foreach(var pin in pins)
                {
                    var rels = graph.GetRelationsBetween2Subjects(owner, pin.Subject.Id);

                    switch (rels.Count())
                    {
                        case 1:
                            pin.UserLinkType = qlanir_web_api.Models.UserLinkType.Primary;
                            pin.UserRole = rels.First().link.name;
                            break;
                        case 2:
                            pin.UserLinkType = qlanir_web_api.Models.UserLinkType.Secondary;
                            var proxy = rels.First().endNode;
                            if (proxy.type == "person") {
                                pin.UserProxy = new Models.Person { Id = proxy.id, Label = proxy.name, Owner = proxy.owner };
                            }
                            else {
                                pin.UserProxy = new Models.Organization { Id = proxy.id, Label = proxy.name, Owner = proxy.owner };
                            }
                            break;
                        default:
                            pin.UserLinkType = qlanir_web_api.Models.UserLinkType.Missing;
                            break;
                    }
                }
            }

             
            return pins;

        }

        public Models.Pin AddPin(string User, Models.CreatePin Pin)
        {
            QlanirEntities.Pin pin = null;

            using (var ctx = new QlanirEntities.QlanirEntitySet())
            {
                var owner = SubjectsDataContext.GetUserId(User, ctx);

                pin = ctx.Pins.FirstOrDefault(p => p.OwnerId == owner && p.SubjectId == Pin.SubjectId && (p.Description == Pin.Description || (p.Description == null && Pin.Description == null)));

                if (pin == null)
                {
                    pin = new QlanirEntities.Pin { SubjectId = Pin.SubjectId, Description = Pin.Description, OwnerId = owner };

                    ctx.Pins.AddObject(pin);

                    ctx.SaveChanges();
                }
            }

            return GetPins(User, pin.Id).First();
        }

        public Models.Pin UpdatePin(string User, int Id, string Description)
        {
            using (var ctx = new QlanirEntities.QlanirEntitySet())
            {
                var owner = SubjectsDataContext.GetUserId(User, ctx);

                var pin = ctx.Pins.FirstOrDefault(p => p.OwnerId == owner && p.Id == Id);

                if (pin == null)
                {
                    throw new ObjectNotFoundException();
                }

                pin.Description = Description;

                ctx.SaveChanges();
            }

            return GetPins(User, Id).First();        
        }

        public void RemovePin(string User, int PinId)
        {
            using (var ctx = new QlanirEntities.QlanirEntitySet()) 
            {
                var owner = SubjectsDataContext.GetUserId(User, ctx);

                var pin = ctx.Pins.FirstOrDefault(p => p.OwnerId == owner && p.Id == PinId);

                if (pin != null)
                {
                    ctx.DeleteObject(pin);

                    ctx.SaveChanges();
                }
                else
                {
                    throw new ObjectNotFoundException();
                }
            }
        }
    }
}
