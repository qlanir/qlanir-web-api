﻿using QlanirEntities;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*
namespace qlanir_web_api.DataContext
{
    partial class SubjectsDataContext
    {
        public static void DeleteContactLink(string User, int ContactLinkId, int SubjectId)
        {

            using (var ctx = new QlanirEntities.QlanirEntitySet())
            {
                var owner = (int)GetUserId(User, ctx);

                var subjectEnty = ctx.Subjects.FirstOrDefault(p => p.Id == SubjectId && (p.OwnerId == owner || p.Id == owner));

                var identLinkEntry = ctx.Links.Include("Event").Where(p => p.Id == ContactLinkId && p.OwnerId == owner).FirstOrDefault();

                if (subjectEnty == null || identLinkEntry == null)
                {
                    throw new ObjectNotFoundException();
                }

                var evt = identLinkEntry.Event;
                var linkedSubj = identLinkEntry.Subject;

                ctx.Events.DeleteObject(evt);
                ctx.Subjects.DeleteObject(linkedSubj);
                
                ctx.SaveChanges();

                ctx.Refresh(System.Data.Entity.Core.Objects.RefreshMode.StoreWins, subjectEnty);

                Index(subjectEnty, true);

            }

        }

        

    }
}
*/