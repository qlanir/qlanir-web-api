﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qlanir_web_api.DataContext
{
    public interface ISearchSubjectsDataContext<M>
        where M : class
    {
        Models.SearchResult Search(string User, DataContextFilter Filter);
    }


}
