﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qlanir_web_api.DataContext
{
    partial class SubjectsDataContext
    {
        public static void AttachLinkedSubject(string user, int subjectId, Models.Tag hostRole, Models.Tag role, int? linkedSubjectId, String linkedSubjectName, String type)
        {
            using (var ctx = new QlanirEntities.QlanirEntitySet())
            {
                var owner = (int)GetUserId(user, ctx);

                var subjectEnty = ctx.Subjects.FirstOrDefault(p => p.Id == subjectId && (p.OwnerId == owner || p.Id == owner));
                var linkedSubjectEnty = ctx.Subjects.FirstOrDefault(p => p.Id == linkedSubjectId && (p.OwnerId == owner || p.Id == owner));

                if (linkedSubjectEnty == null)
                    linkedSubjectEnty = CreateNewSubject(ctx, owner, linkedSubjectName, type);

                if (subjectEnty == null || linkedSubjectEnty == null)
                    throw new ObjectNotFoundException();

                QlanirEntities.Role _hostRole = ctx.Roles.FirstOrDefault(p => (p.Id == hostRole.Id || p.Name == hostRole.Label) && (p.OwnerId == owner || p.Id < 0));
                if (_hostRole == null)
                    _hostRole = CreateNewRole(ctx, owner, hostRole);

                QlanirEntities.Role _role = ctx.Roles.FirstOrDefault(p => (p.Id == role.Id || p.Name == role.Label) && (p.OwnerId == owner || p.Id < 0));
                if (_role == null)
                    _role = CreateNewRole(ctx, owner, role);

                var evt = new QlanirEntities.Event { OwnerId = owner };
                var link1 = new QlanirEntities.Link { OwnerId = owner, Event = evt };
                var link2 = new QlanirEntities.Link { OwnerId = owner, Event = evt };

                link1.Subject = subjectEnty;
                link2.Subject = linkedSubjectEnty;

                link1.Role = _hostRole;
                link2.Role = _role;

                ctx.Events.AddObject(evt);
                ctx.Links.AddObject(link1);
                ctx.Links.AddObject(link2);

                evt.TypeId = (int)Models.EventTypes.Relation;
                
                ctx.SaveChanges();

                ctx.Refresh(System.Data.Entity.Core.Objects.RefreshMode.StoreWins, subjectEnty);

                Index(subjectEnty, true);
            }
        }

        public static void UpdateLinkedSubject(string user, int subjectId, Models.Tag hostRole, Models.Tag role, int? linkedSubjectId, String linkedSubjectName, String type, int linkId)
        {
            using (var ctx = new QlanirEntities.QlanirEntitySet())
            {
                var owner = (int)GetUserId(user, ctx);

                var subjectEnty = ctx.Subjects.FirstOrDefault(p => p.Id == subjectId && (p.OwnerId == owner || p.Id == owner));
                var linkedSubjectEnty = ctx.Subjects.FirstOrDefault(p => p.Id == linkedSubjectId && (p.OwnerId == owner || p.Id == owner));

                if (linkedSubjectEnty == null)
                    linkedSubjectEnty = CreateNewSubject(ctx, owner, linkedSubjectName, type);

                if (subjectEnty == null || linkedSubjectEnty == null)
                    throw new ObjectNotFoundException();

                var link1 = ctx.Links.FirstOrDefault(p => p.Id == linkId);
                var link2 = ctx.Links.FirstOrDefault(p => p.SubjectId != subjectId && p.EventId == link1.EventId);

                link2.SubjectId = linkedSubjectEnty.Id;

                QlanirEntities.Role _hostRole = ctx.Roles.FirstOrDefault(p => (p.Id == hostRole.Id || p.Name == hostRole.Label) && (p.OwnerId == owner || p.Id < 0));
                if (_hostRole == null)
                    _hostRole = CreateNewRole(ctx, owner, hostRole);

                QlanirEntities.Role _role = ctx.Roles.FirstOrDefault(p => (p.Id == role.Id || p.Name == role.Label) && (p.OwnerId == owner || p.Id < 0));
                if (_role == null)
                    _role = CreateNewRole(ctx, owner, role);

                link1.RoleId = _hostRole.Id;
                link2.RoleId = _role.Id;

                ctx.SaveChanges();

                ctx.Refresh(System.Data.Entity.Core.Objects.RefreshMode.StoreWins, subjectEnty);

                Index(subjectEnty, true);
            }
        }

    }
}
