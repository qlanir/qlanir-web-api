﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qlanir_web_api.DataContext
{
    partial class SubjectsDataContext
    {
        public static void AttachLinkedOffer(string User, int SubjectId, string OfferLabel)
        {
            using (var ctx = new QlanirEntities.QlanirEntitySet())
            {
                var subjEnty = ctx.Subjects.FirstOrDefault(p => p.Id == SubjectId);

                var owner = (int)GetUserId(User, ctx);

                QlanirEntities.Tag evtTagEnty = ctx.Tags.FirstOrDefault(p => p.Label.ToLower() == OfferLabel.ToLower() && (p.OwnerId == owner || p.Id < 0));

                if (evtTagEnty == null)
                    evtTagEnty = new QlanirEntities.Tag { Name = OfferLabel, OwnerId = owner };
                else
                {
                    //Check if this tag already linked with same subject (via event)
                    if (ctx.Events.Any(p => p.Tags.Any(s => s.Id == evtTagEnty.Id) && p.Links.Any(s => s.SubjectId == SubjectId)))
                        throw new ArgumentException("Same offer (event's tag with the same label) could be linked to subject only once");
                }

                var evtEnty = new QlanirEntities.Event { OwnerId = owner, TypeId = (int)Models.EventTypes.Resource};
                evtEnty.Tags.Add(evtTagEnty);
                var linkEnty = new QlanirEntities.Link { OwnerId = owner, Event = evtEnty, RoleId = (int)Models.RoleTypes.ReourceOwner, SubjectId = SubjectId };
                
                ctx.AddToEvents(evtEnty);
                ctx.AddToLinks(linkEnty);
                ctx.AddToTags(evtTagEnty);

                ctx.SaveChanges();

                ctx.Refresh(System.Data.Entity.Core.Objects.RefreshMode.StoreWins, evtEnty);

                Index(subjEnty, true);
            }
        }

    }
}
