﻿using Ninject;
using qlanir_web_api.DataContext;
using QlanirEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qlanir_web_api
{
    public static class NinjectConfig
    {
        public static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();

            try
            {
                kernel.Bind<ISubjectsDataContext>().To<SubjectsDataContext>();
                kernel.Bind<ISearchSubjectsDataContext<Models.SearchResult>>().To<SearchSubjectsDataContext>();
                kernel.Bind<IPinsDataContext>().To<PinsDataContext>();
                kernel.Bind<IProfileDataContext>().To<ProfileDataContext>();

                return kernel;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
