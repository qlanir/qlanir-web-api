﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Runtime.Caching;
using System.Text;
using System.Threading.Tasks;

namespace qlanir_web_api.Cache
{
    class Cache : ICache
    {
        private readonly ObjectCache _cache;

        private readonly CacheItemPolicy _policy;

        internal Cache()
        {
            var timeout = int.Parse(ConfigurationManager.AppSettings["USER_CACHE_TIMEOUT"]);

            _cache = MemoryCache.Default;
            _policy = new CacheItemPolicy { SlidingExpiration =  new TimeSpan(0, timeout, 0) };
        }

        public void Set(string Key, object Val)
        {
            _cache.Add(new CacheItem(Key, Val), _policy);
        }

        public object Get(string Key)
        {
            if (_cache.Contains(Key))
            {
                return _cache.Get(Key);
            }
            else
            {
                return null;
            }
        }
    }
}
