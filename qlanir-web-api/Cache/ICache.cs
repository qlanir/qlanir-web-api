﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qlanir_web_api.Cache
{
    interface ICache
    {
        void Set(string Key, object Val);

        object Get(string Key);
    }
}
