﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Owin;
using qlanir_web_api.Cache;
using qlanir_web_api.DataContext;
using System.Configuration;
using System.IO;
using System.Net;
using System.Net.Http;

namespace qlanir_web_api
{
    using AppFunc = Func<IDictionary<string, object>, Task>;
    using Newtonsoft.Json;

    public class JWTAuthenticationComponent
    {
        AppFunc _next;

        public JWTAuthenticationComponent(AppFunc next)
        {
            _next = next;
        }

        public async Task Invoke(IDictionary<string, object> env)
        {
            if ((string)env["owin.RequestMethod"] != "OPTIONS")
            {

                bool isDev = false;
                var user = ConfigurationManager.AppSettings.Get("DEV_USER");

                if (string.IsNullOrEmpty(user))
                {

                    var headers = (IDictionary<string, string[]>)env["owin.RequestHeaders"];
                    var assertion = headers.Where(p => p.Key == "Authorization").SelectMany(p => p.Value).FirstOrDefault();
                    if (!string.IsNullOrEmpty(assertion))
                    {
                        try
                        {
                            var profile = JWTAuth.Authorize(assertion, null);

                            user = profile.provider + "_" + profile.id;
                        }
                        catch (JWT.SignatureVerificationException)
                        {
                            Console.Out.WriteLine("Invalid token!");
                        }
                    }

                }
                else
                {
                    isDev = true;
                }

                if (user != null && (isDev || SubjectsDataContext.GetUserId(user) != 0))
                {
                    var ident = new System.Security.Principal.GenericIdentity(user, "jwt");

                    var principal = new System.Security.Principal.GenericPrincipal(ident, new string[] { "admin" });

                    env["server.User"] = principal;
                }

            }

            await _next(env);
        }     
    }
}
