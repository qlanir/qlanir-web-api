﻿using Newtonsoft.Json;
using qlanir_web_api.Cache;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace qlanir_web_api
{
    public static class JWTAuth
    {
        private static ICache _cache = new Cache.Cache();

        public static JWTUser Authorize(string Assertion, string Host)
        {
            var token = Assertion.Split(' ')[1];
            var jwtSecrtet = ConfigurationManager.AppSettings.Get("JWT_SECRET");

            var jsonPayload = JWT.JsonWebToken.Decode(token, jwtSecrtet);
            
            var res = JsonConvert.DeserializeObject<dynamic>(jsonPayload);

            return new JWTUser { id = res.user.id, name = res.user.name, provider = res.user.provider };
        }

    }

    public class JWTUser
    {
        public string id { get; set; }

        public string provider { get; set; }

        public string identifyer { get { return provider + "_" + id; } }

        public string name { get; set; }
    }
}
