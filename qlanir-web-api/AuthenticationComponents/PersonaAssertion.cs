﻿using Newtonsoft.Json;
using qlanir_web_api.Cache;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace qlanir_web_api
{
    public static class PersonaAssertion
    {
        private static ICache _cache = new Cache.Cache();

        public static async Task<string> AssertUser(string Bearer, string Host)
        {
            if (!string.IsNullOrEmpty(Bearer))
            {
                var user = (string)_cache.Get(Bearer);

                if (user != null)
                    return user;

                using (var client = new HttpClient())
                {
                    var content = new FormUrlEncodedContent(
                            new Dictionary<string, string> {
                            { "assertion", Bearer },
                            { "audience", Host },
                        }
                        );
                    var result = await client.PostAsync("https://verifier.login.persona.org/verify", content);
                    result.EnsureSuccessStatusCode();
                    var stringresult = await result.Content.ReadAsStringAsync();
                    dynamic jsonresult = JsonConvert.DeserializeObject<dynamic>(stringresult);

                    if (jsonresult.status == "okay")
                    {
                        user = jsonresult.email;

                        _cache.Set(Bearer, user);

                        return user;
                    }
                }
            }

            return null;
        }

    }
}
