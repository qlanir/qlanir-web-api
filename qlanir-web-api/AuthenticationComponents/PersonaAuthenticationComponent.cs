﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Owin;
using qlanir_web_api.Cache;
using qlanir_web_api.DataContext;
using System.Configuration;
using System.IO;
using System.Net;
using System.Net.Http;

namespace qlanir_web_api
{
    using AppFunc = Func<IDictionary<string, object>, Task>;

    public class PersonaAuthenticationComponent
    {
        AppFunc _next;

        public PersonaAuthenticationComponent(AppFunc next)
        {
            _next = next;
        }

        public async Task Invoke(IDictionary<string, object> env)
        {
            if ((string)env["owin.RequestMethod"] != "OPTIONS")            
            {
             
                var user = ConfigurationManager.AppSettings.Get("DEV_USER");

                if (string.IsNullOrEmpty(user))
                {
                    
                    var headers = (IDictionary<string, string[]>)env["owin.RequestHeaders"];
                    var bearer = headers.Where(p => p.Key == "assertion_bearer").SelectMany(p => p.Value).FirstOrDefault();
                    var host = headers.Where(p => p.Key == "Origin").SelectMany(p => p.Value).FirstOrDefault();

                    if (string.IsNullOrEmpty(user))
                    {
                        if (bearer != null && host != null)
                        {
                            user = await PersonaAssertion.AssertUser(bearer, host);
                        }
                    }

                }

                if (user != null && SubjectsDataContext.GetUserId(user) != 0)
                {
                    var ident = new System.Security.Principal.GenericIdentity(user, "persona");

                    var id = new System.Security.Principal.GenericPrincipal(ident, new string[] { "admin" });

                    env["server.User"] = id;

                }

            }

            await _next(env);
        }     
    }
}
