﻿var r =
    {
    "settings": {
        "analysis": {
            "filter": {
                "label_ngram": {
                    "max_gram": 20,
                    "min_gram": 2,
                    "type": "edge_ngram"
                }
            },
            "analyzer": {
                "ngram_index": {
                    "filter": [
                        "lowercase",
                        "label_ngram"
                    ],
                    "tokenizer": "whitespace"
                },
                "whitespace_index": {
                    "filter": [
                        "lowercase"
                    ],
                    "tokenizer": "whitespace"
                },
                "whitespace_search": {
                    "filter": [
                        "lowercase"
                    ],
                    "tokenizer": "whitespace"
                }
            }
        }
    },
    "mappings": {
        "person": {
            "properties": {
                "val" : {
                    "fields": {
                        "autocomplete": {
                            "search_analyzer": "whitespace_search",
                            "index_analyzer": "ngram_index",
                            "type": "string"
                        },
                        "val": {
                            "search_analyzer": "whitespace_search",
                            "index_analyzer": "whitespace_index",
                            "index": "not_analyzed",
                            "type": "string"
                        }
                    },
                    "type": "multi_field"
                }
            }
        },
        "organization": {
            "properties": {
                "val": {
                    "fields": {
                        "autocomplete": {
                            "search_analyzer": "whitespace_search",
                            "index_analyzer": "ngram_index",
                            "type": "string"
                        },
                        "val": {
                            "search_analyzer": "whitespace_search",
                            "index_analyzer": "whitespace_index",
                            "index": "not_analyzed",
                            "type": "string"
                        }
                    },
                    "type": "multi_field"
                }
            }
        },
        "offer": {
            "properties": {
                "val": {
                    "fields": {
                        "autocomplete": {
                            "search_analyzer": "whitespace_search",
                            "index_analyzer": "ngram_index",
                            "type": "string"
                        },
                        "val": {
                            "search_analyzer": "whitespace_search",
                            "index_analyzer": "whitespace_index",
                            "index": "not_analyzed",
                            "type": "string"
                        }
                    },
                    "type": "multi_field"
                }
            }
        }
    }
}		

