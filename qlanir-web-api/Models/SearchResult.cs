﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qlanir_web_api.Models
{
    public class SearchResult
    {
        public SearchResultTotalCount TotalCount { get; set; }

        public string Term { get; set; }

        public string Type { get; set; }

        public SearchResultItem [] Results {get; set;}
    }

    public class SearchResultItem {
        
        public int Id { get; set; }

        public string Name { get; set; }

        public string Type { get; set; }

        public SearchResultItemHit[] Hits { get; set; }

        public string[] Tags { get; set; }
    }

    public class SearchResultItemHit
    {
        public string Hit { get; set; }

        public string Type { get; set; }

    }

    public class SearchResultTotalCount
    {
        public int People { get; set; }

        public int Offers { get; set; }

        public int Organizations { get; set; }
    }
}
