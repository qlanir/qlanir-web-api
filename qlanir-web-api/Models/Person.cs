﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace qlanir_web_api.Models
{
    public class PersonNameParts 
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string NickName { get; set; }
    }

    public class PersonBase : Subject
    {
        /// <summary>
        /// Date in format 'dd.mm.yyyy'
        /// </summary>
        public string Birthdate { get; set; }

        public PersonNameParts NameParts { get; set; }

        public int? Sex { get; set; } //0 - woman, 1 - man, null - unknown

        public override string Type
        {
            get { return "person"; } 
        }
    }

    [KnownType(typeof(Models.Person))]
    public class Person : PersonBase, ISubjectWithLinks
    {
        public Tag[] Tags { get; set; }
        public Link<Contact>[] Contacts { get; set; }
        public OfferLink[] Offers { get; set; }
        public PersonLink[] People { get; set; }
        public Link<OrganizationBase>[] Organizations { get; set; }
        public SubjectPath[] ProfilePaths { get; set; } 
      
    }

}
