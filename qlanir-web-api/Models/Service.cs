﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace qlanir_web_api.Models
{
    public class OfferBase : Subject
    {
        public override string Type
        {
            get { return "offer"; }
        }
    }

    [KnownType(typeof(Models.Offer))]
    public class Offer : OfferBase, ISubjectWithLinks
    {
        public Tag[] Tags { get; set; }
        public Link<Contact>[] Contacts { get; set; }
        public OfferLink[] Offers { get; set; }
        public PersonLink[] People { get; set; }
        public Link<OrganizationBase>[] Organizations { get; set; }
        public SubjectPath[] ProfilePaths { get; set; } 
    }

    public class Service : OfferBase, IService
    {
        public override string Type
        {
            get { return "service"; }
        }

        public OfferFull [] Offers { get; set; }
    }

    public interface IService : ISubject
    {
        OfferFull[] Offers { get; set; }
    }

    public class OfferFull : OfferLink
    {
        public SubjectBase HostSubject { get; set; }
    }

    public class LinkedSubject
    {
        public int Id {get; set;}

        public string Label {get; set;}
    }


    public class LinkedSubjectTyped : LinkedSubject
    {
        public string Type {get; set;}
    }

    public class PostOffer
    { 
        public string Description {get; set;}

        public LinkedSubject Type {get; set;}

        public LinkedSubjectTyped LinkedSubject {get; set;}

        public LinkedSubject Proxy {get; set;}
    }

}
