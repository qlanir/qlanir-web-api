﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qlanir_web_api.Models
{
    public class LinkBase
    {
        public int Id { get; set; }

        public Tag Tag {get; set;}

        public Tag HostTag { get; set; }

        //public string Label { get; set; }
    }

    public class Link<T> : LinkBase 
        where
        T : Models.Subject, new()
    {
        public T Subject { get; set; }
    }

    public class ContactLink : Link<Contact>
    {
        //public int TagId { get; set; }
    }

    public class OfferLink : Link<OfferBase>
    {
        public PersonBase Proxy { get; set; }
    }

    public class PersonLink : Link<PersonBase>
    {
        //This field is used only to create some proxy for linked person .
        //When data is retrieved, this field always empty !
        public PersonBase Proxy { get; set; }
    }

}
