﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qlanir_web_api.Models
{
    public class UserResponse
    {
        public string User { get; set; }

        public bool IsNew { get; set; }
    }
}
