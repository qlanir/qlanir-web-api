﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace qlanir_web_api.Models
{
    public class SubjectBase
    {
        public string Type { get; set; }

        public int Id { get; set; }
        public string Label { get; set; }
        public int Owner { get; set; }

        public System.DateTime Created { get; set; }
    }

    public abstract class Subject : ISubject
    {
        public abstract string Type { get; }
       
        public int Id { get; set; }
        public string Label { get; set; }

        public int Owner { get; set; }
        public System.DateTime Created { get; set; }
        
    }

}
