﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qlanir_web_api.Models
{
    public interface ISubject
    {
        string Type { get; }
        int Id { get; set; }
        string Label { get; set; }

        int Owner { get; set; }
        System.DateTime Created { get; set; }
    }

    public interface ISubjectWithLinks : ISubject
    {
        Tag[] Tags { get; set; }
        Link<Contact>[] Contacts { get; set; }
        OfferLink[] Offers { get; set; }
        PersonLink[] People { get; set; }
        Link<OrganizationBase>[] Organizations { get; set; }
    }

    public class SubjectPath 
    {
        public SubjectBase Issuer { get; set; }
        public SubjectBase Target { get; set; }
        public SubjectBase[] Proxies { get; set; }
    }

    public static class SubjectUtils 
    {
        internal static T CreateSubject<T>() where T : ISubjectWithLinks, new()
        {
            return new T
            {
                Contacts = new Link<Contact>[0],
                People = new PersonLink[0],
                Organizations = new Link<OrganizationBase>[0],
                Offers = new OfferLink[0],
                Tags = new Tag[0]
            };
        }
    }
}
