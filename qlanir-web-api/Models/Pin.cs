﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qlanir_web_api.Models
{
    public class Pin
    {
        public int Id { get; set; }

        public SubjectBase Subject {get; set;}

        public string Description { get; set; }

        public UserLinkType UserLinkType { get; set; }

        public string UserRole { get; set; }

        public Subject UserProxy { get; set; }
    }

    public class CreatePin
    {
        public int SubjectId { get; set; }

        public string Description { get; set; }
    }

    public enum UserLinkType
    {
        Missing,
        Primary,
        Secondary
    }

}
