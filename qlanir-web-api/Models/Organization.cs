﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace qlanir_web_api.Models
{
    public class OrganizationBase : Subject
    {
        public override string Type
        {
            get { return "organization"; }
        }
    }

    [KnownType(typeof(Models.Organization))]
    public class Organization : OrganizationBase, ISubjectWithLinks
    {
        public Tag[] Tags { get; set; }
        public Link<Contact>[] Contacts { get; set; }
        public OfferLink[] Offers { get; set; }
        public PersonLink[] People { get; set; }
        public Link<OrganizationBase>[] Organizations { get; set; }
        public SubjectPath[] ProfilePaths { get; set; } 

    }
}
