﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qlanir_web_api.Models
{
    public enum EventTypes
    {
        Relation = -10,
        Owner = -13,
        Resource = -14,
        Work = -9,
        Registration = -3
    }
}
