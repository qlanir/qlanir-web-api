﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qlanir_web_api.Models
{
    public enum RoleTypes
    {
        Identifyers = -20,
        WorkIdent = -44,
        Owner = -28,
        Ownership = -29,
        Acquitance = -39,
        Brother = -151,
        Sister = -150,
        ReourceOwner = -42,
        Resource = -41,
        ResourceProxy = -43,
        Partner = -5,
        Employer = -17,
        Employee = -31,
        Relation = -34,
        Founder = -22,
        Foundation = -23
    }
}
