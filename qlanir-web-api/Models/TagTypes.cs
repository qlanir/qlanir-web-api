﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qlanir_web_api.Models
{
    public enum TagTypes
    {
        Profile = -501,
        Phone = -12,
        Email = -14,
        Skype = -15,
        Site = -16,
        SystemEmail = -603,
        Ident = -8,
        Twitter = -19,
        Vkontakte = -20,
        Google = -18,
        Facebook = -17
    }
}
