﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qlanir_web_api.Models
{
    public class Contact : Subject
    {
        public override string Type
        {
            get { return "contact"; }
        }

        public Tag ContactType { get; set; }
    }

}
