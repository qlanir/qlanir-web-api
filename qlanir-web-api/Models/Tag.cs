﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qlanir_web_api.Models
{
    public class Tag : Subject
    {
        private readonly string _type;

        public Tag() : this("tag")
        {
        }

        public Tag(string Type) : this(Type, null)
        {
        }
       
        public Tag(string Type, Tag Tag)
        {
            _type = Type;
            if (Tag != null)
            {
                this.Id = Tag.Id;
                this.Label = Tag.Label;
                this.Owner = Tag.Owner;
            }
        }

        public override string Type
        {
            get { return _type; }
        }
    }
}
