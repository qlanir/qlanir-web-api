﻿using qlanir_web_api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qlanir_web_api_test.specs
{
    class create_org_spec : subj_base_spec
    {
        PersonBase linkedPerson;

        void it_index_all()
        {
            SubjUtils.IndexAll();
        }

        void it_create_profile()
        {
            profileId = SubjUtils.CreateAndStoreProfile();
        }

        
        void it_create_new_org_simple()
        {
            
            var org = SubjUtils.CreateSubject<Organization>("Org Simple", profileId);
           
            var actual = (Organization) SubjUtils.CheckHttpResult(subjectsController.PostOrganization(org));

            org.Match(actual);
        }
        

        void it_create_new_org_with_tag()
        {

            var org = SubjUtils.CreateSubject<Organization>("Org With Tag", profileId);
           
            org.PushLink(new Tag { Label = "test", Owner = profileId });

            var actual = (Organization)SubjUtils.CheckHttpResult(subjectsController.PostOrganization(org));

            org.Match(actual);
        }

        void it_create_new_org_with_contact()
        {
            var org = SubjUtils.CreateSubject<Organization>("Org With Contact", profileId);

            org.PushLink(new ContactLink { Subject = new Contact { Label = "0000", ContactType = new Tag { Id = (int)TagTypes.Skype } } });

            var actual = (Organization)SubjUtils.CheckHttpResult(subjectsController.PostOrganization(org));

            org.Contacts[0].Subject.Owner = profileId;
            org.Contacts[0].Subject.ContactType.Label = "skype";
            org.Contacts[0].Subject.ContactType.Owner = profileId;

            org.Contacts[0].Tag = new Tag { Id = (int)RoleTypes.Identifyers, Label = "идентификаторы", Owner = profileId };

            org.Match(actual);
        }
        

        void it_create_new_org_with_person()
        {
            var org = SubjUtils.CreateSubject<Organization>("Org With Person", profileId);

            org.PushLink(new PersonLink { Subject = new PersonBase { Label = "New Person" }, Tag = new Tag { Id = (int)qlanir_web_api.Models.RoleTypes.Employee } });

            var actual = (Organization)SubjUtils.CheckHttpResult(subjectsController.PostOrganization(org));

            org.People[0].Subject.Owner = profileId;
            org.People[0].Tag.Owner = profileId;
            org.People[0].Tag.Label = "наёмник";

            org.People[0].HostTag = new Tag { Id = (int)RoleTypes.Employer };
            org.People[0].HostTag.Owner = profileId;
            org.People[0].HostTag.Label = "наниматель";

            org.Match(actual);

            linkedPerson = actual.People[0].Subject;
        }

        void it_search_person_empty()
        {
            var expected = SubjUtils.CreateSearchResult("person", null);
            expected.TotalCount.People = 2;
            expected.TotalCount.Organizations = 4;
            expected.PushSerachResultItem(new SearchResultItem { Id = linkedPerson.Id, Name = linkedPerson.Label, Type = "person" });
            expected.PushSerachResultItem(new SearchResultItem { Id = profileId, Name = SubjUtils.GetUserName(), Type = "person" });

            var actual = (SearchResult)SubjUtils.CheckHttpResult(searchController.Search(new qlanir_web_api.DataContext.DataContextFilter { Type = "person" }));

            expected.Match(actual);
        }


        void it_search_person_names()
        {
            var expected = new [] {  new Tag("person") { Id = linkedPerson.Id, Label = linkedPerson.Label, Owner = linkedPerson.Owner } };

            var actual = (IEnumerable<Tag>)SubjUtils.CheckHttpResult(searchController.SearchNames(new qlanir_web_api.DataContext.DataContextFilter { Type = "person", Term = "new" }));

            expected.Match(actual);
        }

        void it_delete_person()
        {
            SubjUtils.CheckHttpEmptyResult(subjectsController.DeletePerson(linkedPerson.Id));
        }

        void it_search_person_names_after_delete()
        {
            var actual = (IEnumerable<Tag>)SubjUtils.CheckHttpResult(searchController.SearchNames(new qlanir_web_api.DataContext.DataContextFilter { Type = "person", Term = "new" }));

            (new Tag[0]).Match(actual);
        }

        void it_search_org_with_tag()
        {
            var expected = SubjUtils.CreateSearchResult("organization", "tag");
            expected.TotalCount.People = 0;
            expected.TotalCount.Organizations = 1;
            expected.PushSerachResultItem(new SearchResultItem { Name = "Org With <em>Tag</em>", Type = "organization", Tags = new [] {"test"} });

            var actual = (SearchResult)SubjUtils.CheckHttpResult(searchController.Search(new qlanir_web_api.DataContext.DataContextFilter { Type = "organization", Term = "tag", IncludeLinkedTags = true }));

            expected.Match(actual);
        }



    }
}
