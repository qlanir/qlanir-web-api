﻿using qlanir_web_api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qlanir_web_api_test.specs
{
    class links_spec : subj_base_spec
    {
        void it_index_all()
        {
            SubjUtils.IndexAll();
        }

        void it_create_profile()
        {
            profileId = SubjUtils.CreateAndStoreProfile();
        }

        int hostPersonId;

        void it_create_host_person()
        {
            var person = SubjUtils.CreateSubject<Person>("Person WithLinks", profileId);
            var actual = (Person)SubjUtils.CheckHttpResult(subjectsController.PostPerson(person));
            person.Match(actual);

            hostPersonId = actual.Id;
        }

        #region attach links
        void it_attach_new_contact()
        {
            SubjUtils.CheckHttpEmptyResult(subjectLinksController.AttachSubjectContact(hostPersonId, new qlanir_web_api.Controllers.SubjectLinksController.ContactLinkBody { roleId = null, typeId = (int)TagTypes.Skype, value = "testSkype" }));
        }

        void it_attach_new_person()
        {
            SubjUtils.CheckHttpEmptyResult(subjectLinksController.AttachSubjectPerson(hostPersonId, "subjects",
                new qlanir_web_api.Controllers.SubjectLinksController.SubjectLinkBody
                {
                    HostRole = new Tag { Id = (int)qlanir_web_api.Models.RoleTypes.Partner },
                    Role = new Tag { Label = "партнер" },
                    LinkedSubject = new qlanir_web_api.Controllers.SubjectLinksController.LinkedSubject { Label = "Person New" }
                }));
        }

        void it_attach_new_org()
        {
            SubjUtils.CheckHttpEmptyResult(subjectLinksController.AttachSubjectOrganization(hostPersonId, "subjects",
                new qlanir_web_api.Controllers.SubjectLinksController.SubjectLinkBody
                {
                    HostRole = new Tag { Id = (int)qlanir_web_api.Models.RoleTypes.Employee },
                    Role = new Tag { Id = (int)qlanir_web_api.Models.RoleTypes.Employer },
                    LinkedSubject = new qlanir_web_api.Controllers.SubjectLinksController.LinkedSubject { Label = "NewOrg" }
                }));
        }

        //TODO attach offer
        #endregion

        int contactLinkId;
        int orgLinkId;
        int linkedOrgId;
        int personLinkId;
        int linkedPersonId;

        void it_get_person_with_links()
        {
            var actual = (Person)SubjUtils.CheckHttpResult(subjectsController.GetPerson(hostPersonId));

            var person = SubjUtils.CreateSubject<Person>("Person WithLinks", profileId);

            ContactLink contact = new ContactLink
            {
                Tag = new Tag { Id = (int)RoleTypes.Identifyers, Label = "идентификаторы", Owner = profileId },
                Subject = new Contact
                    {
                        Owner = profileId,
                        ContactType = new Tag { Label = "skype", Id = -15, Owner = profileId },
                        Label = "testSkype"
                    }
            };

            PersonLink personLink = new PersonLink
            {
                HostTag = new Tag{ Label = "партнер", Id = -5, Owner = profileId},
                Tag = new Tag { Label = "партнер", Id = -5, Owner = profileId},
                Subject = new PersonBase
                {
                    Owner = profileId,
                    Label = "Person New"
                }
            };

            Link<OrganizationBase> orgLink = new Link<OrganizationBase>
            {
                HostTag = new Tag { Label = "наёмник", Id = -31, Owner = profileId },
                Tag = new Tag { Label = "наниматель", Id = -17, Owner = profileId },
                Subject = new OrganizationBase
                {
                    Owner = profileId,
                    Label = "NewOrg"
                }
            };

            //TODO offer for link

            person.PushLink(contact);
            person.PushLink(personLink);
            person.PushLink(orgLink);

            person.Match(actual);

            contactLinkId = actual.Contacts[0].Id;
            personLinkId = actual.People[0].Id;
            linkedPersonId = actual.People[0].Subject.Id;
            orgLinkId = actual.Organizations[0].Id;
            linkedOrgId = actual.Organizations[0].Subject.Id;
        }

        #region update links
        void it_update_linked_contact()
        {
            SubjUtils.CheckHttpEmptyResult(subjectLinksController.UpdateLinkedContact(hostPersonId, contactLinkId, new qlanir_web_api.Controllers.SubjectLinksController.ContactLinkBody { roleId = null, typeId = (int)TagTypes.Skype, value = "updatedSkype" }));
        }

        void it_update_linked_person()
        {
            SubjUtils.CheckHttpEmptyResult(subjectLinksController.UpdateLinkedPerson(hostPersonId, personLinkId,
                new qlanir_web_api.Controllers.SubjectLinksController.SubjectLinkBody
                {
                    HostRole = new Tag { Label = "брат" },
                    Role = new Tag { Label = "брат" },
                    LinkedSubject = new qlanir_web_api.Controllers.SubjectLinksController.LinkedSubject { Label = "Person New" , Id = linkedPersonId}
                }));
        }

        void it_update_linked_org()
        {
            SubjUtils.CheckHttpEmptyResult(subjectLinksController.UpdateLinkedOrg(hostPersonId, orgLinkId,
                new qlanir_web_api.Controllers.SubjectLinksController.SubjectLinkBody
                {
                    HostRole = new Tag { Label = "тестовый работник" },
                    Role = new Tag { Label = "тестовая работа" },
                    LinkedSubject = new qlanir_web_api.Controllers.SubjectLinksController.LinkedSubject { Label = "NewOrg" , Id = linkedOrgId}
                }));
        }

        //TODO attach offer
        #endregion

        /*
        void it_get_person_with_updated_links()
        {
            var actual = (Person)SubjUtils.CheckHttpResult(subjectsController.GetPerson(hostPersonId));

            var person = SubjUtils.CreateSubject<Person>("Person WithLinks", profileId);

            ContactLink contact = new ContactLink
            {
                Tag = new Tag { Id = (int)RoleTypes.Identifyers, Label = "идентификаторы", Owner = profileId },
                Subject = new Contact
                {
                    Owner = profileId,
                    ContactType = new Tag { Label = "skype", Id = -15, Owner = profileId },
                    Label = "updatedSkype"
                }
            };

            PersonLink personLink = new PersonLink
            {
                HostTag = new Tag { Label = "брат", Owner = profileId },
                Tag = new Tag { Label = "брат", Owner = profileId },
                Subject = new PersonBase
                {
                    Owner = profileId,
                    Label = "Person New",
                    Id = linkedPersonId
                }
            };

            Link<OrganizationBase> orgLink = new Link<OrganizationBase>
            {
                HostTag = new Tag { Label = "тестовый работник", Owner = profileId },
                Tag = new Tag { Label = "тестовая работа", Owner = profileId },
                Subject = new OrganizationBase
                {
                    Owner = profileId,
                    Label = "NewOrg",
                    Id = linkedOrgId
                }
            };

            //TODO offer for link

            person.PushLink(contact);
            person.PushLink(personLink);
            person.PushLink(orgLink);

            person.Match(actual);
        }
         */

        #region delete links
        void it_delete_contact()
        {
            var actual = (Person)SubjUtils.CheckHttpResult(subjectLinksController.DeletePersonLink(hostPersonId, contactLinkId));
        }

        void it_delete_link_person()
        {
            var actual = (Person)SubjUtils.CheckHttpResult(subjectLinksController.DeletePersonLink(hostPersonId, personLinkId));
        }

        void it_delete_link_org()
        {
            var actual = (Person)SubjUtils.CheckHttpResult(subjectLinksController.DeletePersonLink(hostPersonId, orgLinkId));
        }
        #endregion

        void it_get_person_with_deleted_links()
        {
            var actual = (Person)SubjUtils.CheckHttpResult(subjectsController.GetPerson(hostPersonId));
            var person = SubjUtils.CreateSubject<Person>("Person WithLinks", profileId);

            person.Match(actual);
        }

        void it_end()
        {
            //SubjUtils.CheckHttpEmptyResult(subjectsController.Delete(hostPersonId));
            //SubjUtils.CheckHttpEmptyResult(subjectsController.Delete(linkedPersonId));
           // SubjUtils.CheckHttpEmptyResult(subjectsController.Delete<Organization>(orgLinkId)); //TODO for org
        }
    }
}
