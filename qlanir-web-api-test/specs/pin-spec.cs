﻿using qlanir_web_api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NSpec;

namespace qlanir_web_api_test.specs
{
    class pins_spec : subj_base_spec
    {
        Person person;
        Organization org;
        //OfferLink serviceLink;
        Subject proxyPerson;
        string serviceDescription;
        int pinId;

        void it_index_all()
        {
            SubjUtils.IndexAll();
        }

        void it_create_profile()
        {
            profileId = SubjUtils.CreateAndStoreProfile();
        }


        // org (no link to user) + service
        void it_create_org()
        {
            var newOrg = SubjUtils.CreateSubject<Organization>("Org Simple", profileId);

            var actual = (Organization)SubjUtils.CheckHttpResult(subjectsController.PostOrganization(newOrg));

            org = actual;
        }

        void it_create_service()
        {
            var servicePost = new qlanir_web_api.Models.PostOffer
            {
                Description = "Description of service linked to org",
                Type = new LinkedSubject { Label = "New Service Of New org" },
                LinkedSubject = new LinkedSubjectTyped { Id = org.Id, Type = "organization", Label = org.Label }
            };

            var actual = (Offer)SubjUtils.CheckHttpResult(servicesController.PostOffer(servicePost));

        }

        void it_create_service_org_pin()
        {
            serviceDescription = "New Service Of New org Description of service linked to org";

            var expected = new Pin
            {
                Description = serviceDescription,
                Subject = new SubjectBase { Id = org.Id, Label = org.Label, Type = org.Type, Owner = org.Owner },
                UserLinkType = UserLinkType.Missing
            };

            var newPin = new CreatePin { SubjectId = org.Id, Description = serviceDescription };

            var pin = (Pin)SubjUtils.CheckHttpResult(pinsController.AddPin(newPin));

            expected.Match(pin);
        }

        void it_get_service_org_pin()
        {
            var expected = new Pin { 
                Description = serviceDescription, 
                Subject = new SubjectBase { Id = org.Id, Label = org.Label, Type = org.Type, Owner = org.Owner }, 
                UserLinkType = UserLinkType.Missing };

            var actual = (IEnumerable<Pin>)SubjUtils.CheckHttpResult(pinsController.GetPins());

            actual.Count().should_be(1);
            var pin = actual.First();

            expected.Match(pin);

            pinId = pin.Id;
        }

        void it_remove_pin()
        {
            SubjUtils.CheckHttpEmptyResult(pinsController.RemovePin(pinId));
        }

        void it_remove_org()
        {
            SubjUtils.CheckHttpEmptyResult(subjectsController.DeleteOrganization(org.Id));
        }
        

        // org (direct link to user) + service
        void it_create_org_linked_to_user()
        {
            var newOrg = SubjUtils.CreateSubject<Organization>("Org Linked", profileId);
            newOrg.PushLink(new PersonLink { Subject = new PersonBase { Id = profileId }});

            var actual = (Organization)SubjUtils.CheckHttpResult(subjectsController.PostOrganization(newOrg));

            org = actual;
        }

        void it_create_service_linked_org_linked_to_user()
        {
            var servicePost = new qlanir_web_api.Models.PostOffer
            {
                Description = "Description of service linked to org",
                Type = new LinkedSubject { Label = "New Service Of New org" },
                LinkedSubject = new LinkedSubjectTyped { Id = org.Id, Type = "organization", Label = org.Label }
            };

            var actual = (Offer)SubjUtils.CheckHttpResult(servicesController.PostOffer(servicePost));

        }


        void it_create_service_user_pin()
        {
            serviceDescription = "New Service Of New org Description of service linked to org";

            var expected = new Pin
            {
                Description = serviceDescription,
                Subject = new SubjectBase { Id = org.Id, Label = org.Label, Type = org.Type, Owner = org.Owner },
                UserLinkType = UserLinkType.Primary,
                UserRole = "наниматель"
            };

            var newPin = new CreatePin { SubjectId = org.Id, Description = serviceDescription };

            var pin = (Pin)SubjUtils.CheckHttpResult(pinsController.AddPin(newPin));

            expected.Match(pin);
        }

        void it_get_linked_org_linked_to_user_pin()
        {
            var expected = new Pin
            {
                Description = serviceDescription,
                Subject = new SubjectBase { Id = org.Id, Label = org.Label, Type = org.Type, Owner = org.Owner },
                UserLinkType = UserLinkType.Primary,
                UserRole = "наниматель"
            };

            var actual = (IEnumerable<Pin>)SubjUtils.CheckHttpResult(pinsController.GetPins());

            actual.Count().should_be(1);
            var pin = actual.First();

            expected.Match(pin);

            pinId = pin.Id;
        }

        void it_remove_org_linked_to_user_pin()
        {
            SubjUtils.CheckHttpEmptyResult(pinsController.RemovePin(pinId));
        }

        void it_remove_org_linked_to_user()
        {
            SubjUtils.CheckHttpEmptyResult(subjectsController.DeleteOrganization(org.Id));
        }

        // org (proxy link to user) + service
        void it_create_proxy_person()
        {
            var newPerson = SubjUtils.CreateSubject<Person>("Person Proxy", profileId);
            newPerson.PushLink(new PersonLink { Subject = new PersonBase { Id = profileId } });

            var actual = (Person)SubjUtils.CheckHttpResult(subjectsController.PostPerson(newPerson));

            person = actual;
            proxyPerson = actual;
        }


        void it_create_org_linked_to_proxy()
        {
            var newOrg = SubjUtils.CreateSubject<Organization>("Org Linked", profileId);
            newOrg.PushLink(new PersonLink { Subject = new PersonBase { Id = person.Id } });

            var actual = (Organization)SubjUtils.CheckHttpResult(subjectsController.PostOrganization(newOrg));

            org = actual;
        }

        void it_create_service_linked_org_linked_to_proxy()
        {
            var servicePost = new qlanir_web_api.Models.PostOffer
            {
                Description = "Description of service linked to org",
                Type = new LinkedSubject { Label = "New Service Of New org" },
                LinkedSubject = new LinkedSubjectTyped { Id = org.Id, Type = "organization", Label = org.Label }
            };

            var actual = (Offer)SubjUtils.CheckHttpResult(servicesController.PostOffer(servicePost));

        }


        void it_create_service_proxy_pin()
        {
            serviceDescription = "New Service Of New org Description of service linked to org";

            var expected = new Pin
            {
                Description = serviceDescription,
                Subject = new SubjectBase { Id = org.Id, Label = org.Label, Type = org.Type, Owner = org.Owner },
                UserLinkType = UserLinkType.Secondary,
                UserProxy = proxyPerson
            };

            var newPin = new CreatePin { SubjectId = org.Id, Description = serviceDescription };

            var pin = (Pin)SubjUtils.CheckHttpResult(pinsController.AddPin(newPin));

            expected.Match(pin);
        }

        void it_get_linked_org_linked_to_prxoy_pin()
        {
            var expected = new Pin
            {
                Description = serviceDescription,
                Subject = new SubjectBase { Id = org.Id, Label = org.Label, Type = org.Type, Owner = org.Owner },
                UserLinkType = UserLinkType.Secondary,
                UserProxy = proxyPerson
            };

            var actual = (IEnumerable<Pin>)SubjUtils.CheckHttpResult(pinsController.GetPins());

            actual.Count().should_be(1);
            var pin = actual.First();

            expected.Match(pin);

            pinId = pin.Id;
        }

        void it_update_linked_org_linked_to_prxoy_pin()
        {
            var expected = new Pin
            {
                Id = pinId,
                Description = "New description",
                Subject = new SubjectBase { Id = org.Id, Label = org.Label, Type = org.Type, Owner = org.Owner },
                UserLinkType = UserLinkType.Secondary,
                UserProxy = proxyPerson
            };

            var pin = (Pin)SubjUtils.CheckHttpResult(pinsController.UpdatePin(pinId, new CreatePin { Description = "New description"}));

            expected.Match(pin);
        }

        void it_get_linked_org_linked_to_prxoy_pin_after_update()
        {
            var expected = new Pin
            {
                Description = "New description",
                Subject = new SubjectBase { Id = org.Id, Label = org.Label, Type = org.Type, Owner = org.Owner },
                UserLinkType = UserLinkType.Secondary,
                UserProxy = proxyPerson
            };

            var actual = (IEnumerable<Pin>)SubjUtils.CheckHttpResult(pinsController.GetPins());

            actual.Count().should_be(1);
            var pin = actual.First();

            expected.Match(pin);

            pinId = pin.Id;
        }

        //create doubled profile pins

        void it_create_doubled_profile_pins()
        {
            var newPin = new CreatePin { SubjectId = profileId };

            var pin1 = (Pin)SubjUtils.CheckHttpResult(pinsController.AddPin(newPin));
            var pin2 = (Pin)SubjUtils.CheckHttpResult(pinsController.AddPin(newPin));

            pin1.Match(pin2);
        }

        void it_remove_org_linked_to_proxy_pin()
        {
            SubjUtils.CheckHttpEmptyResult(pinsController.RemovePin(pinId));
        }

        void it_remove_org_linked_to_proxy()
        {
            SubjUtils.CheckHttpEmptyResult(subjectsController.DeleteOrganization(org.Id));
        } 

    }
}
