﻿using NSpec;
using qlanir_web_api.Controllers;
using qlanir_web_api.DataContext;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qlanir_web_api_test.specs
{
    class subj_base_spec : nspec
    {
        protected int profileId;

        protected SubjectsController subjectsController;
        protected SubjectLinksController subjectLinksController;
        protected TagsController tagsController;
        protected SearchController searchController;
        protected ServicesController servicesController;
        protected PinsController pinsController;


        void before_each()
        {
            subjectsController = new SubjectsController(new SubjectsDataContext());
            subjectLinksController = new SubjectLinksController(new SubjectsDataContext());
            tagsController = new TagsController();
            
            searchController = new SearchController(new SearchSubjectsDataContext());
            servicesController = new ServicesController(new SubjectsDataContext());
            pinsController = new PinsController(new PinsDataContext());

            var ident = new System.Security.Principal.GenericIdentity(SubjUtils.GetUserName());
            var pl = new System.Security.Principal.GenericPrincipal(ident, new string[0]);
            subjectsController.RequestContext.Principal = pl;
            subjectLinksController.RequestContext.Principal = pl;
            tagsController.RequestContext.Principal = pl;
            searchController.RequestContext.Principal = pl;
            servicesController.RequestContext.Principal = pl;
            pinsController.RequestContext.Principal = pl;
        }


    }
}
