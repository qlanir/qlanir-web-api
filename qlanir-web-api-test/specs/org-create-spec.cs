﻿using qlanir_web_api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qlanir_web_api_test.specs
{
    class org_create_spec : subj_base_spec
    {
        int orgId;
        int personId;
      
        void it_index_all()
        {
            SubjUtils.IndexAll();
        }

        void it_create_profile()
        {
            profileId = SubjUtils.CreateAndStoreProfile();
        }

        
        void it_create_simple_org()
        {
            var org = SubjUtils.CreateSubject<Organization>("Org Simple", profileId);
            
            var actual = (Organization)SubjUtils.CheckHttpResult(subjectsController.PostOrganization(org));

            org.Match(actual);
        }

        void it_find_simple_org_by_no_filter()
        {
            var expected = SubjUtils.CreateSearchResult("organization", null);
            expected.TotalCount.People = 1;
            expected.TotalCount.Organizations = 1;
            expected.PushSerachResultItem(new SearchResultItem { Name = "Org Simple", Type = "organization" });
            
            var actual = (SearchResult)SubjUtils.CheckHttpResult(
                searchController.Search(new qlanir_web_api.DataContext.DataContextFilter { Type = "organization", IncludeLinkedTags = false }));

            expected.Match(actual);
        }

        void it_find_simple_org_by_filter()
        {
            var expected = SubjUtils.CreateSearchResult("organization", "sample");
            expected.TotalCount.Organizations = 1;
            expected.PushSerachResultItem(new SearchResultItem { Name = "Org <em>Simple</em>", Type = "organization" });

            var actual = (SearchResult)SubjUtils.CheckHttpResult(
                searchController.Search(new qlanir_web_api.DataContext.DataContextFilter { Type = "organization", Term ="sample", IncludeLinkedTags = false }));

            expected.Match(actual);

            //
            var expected1 = SubjUtils.CreateSubject<Organization>("Org Simple", profileId);
            var foundId = actual.Results[0].Id;
            var actual1 = (Organization)SubjUtils.CheckHttpResult(subjectsController.GetOrganization(foundId));

            expected1.Match(actual1);

            orgId = actual1.Id;
        }
        
        void it_remove_simple_org()
        {
            SubjUtils.CheckHttpEmptyResult(subjectsController.DeleteOrganization(orgId));
        }
        

        void it_find_simple_org_by_no_filter_after_remove()
        {
            var expected = SubjUtils.CreateSearchResult("organization", null);
            expected.TotalCount.People = 1;

            var actual = (SearchResult)SubjUtils.CheckHttpResult(
                searchController.Search(new qlanir_web_api.DataContext.DataContextFilter { Type = "organization", IncludeLinkedTags = false }));

            expected.Match(actual);
        }

        ///org with contact
        void it_create_org_with_contact()
        {
            var org = SubjUtils.CreateSubject<Organization>("Org Contact", profileId);
            org.PushLink(new ContactLink { Subject = new Contact { Label = "333", ContactType = new Tag { Id = (int)TagTypes.Skype } } });

            var actual = (Organization)SubjUtils.CheckHttpResult(subjectsController.PostOrganization(org));

            org.Contacts[0].Subject.Owner = profileId;
            org.Contacts[0].Subject.ContactType.Label = "skype";
            org.Contacts[0].Subject.ContactType.Owner = profileId;
            org.Contacts[0].Tag = new Tag { Id = (int)RoleTypes.Identifyers, Label = "идентификаторы", Owner = profileId };

            org.Match(actual);
        }

        void it_find_org_with_contact_by_filter()
        {
            var expected = SubjUtils.CreateSearchResult("organization", "contact");
            expected.TotalCount.People = 0;
            expected.TotalCount.Organizations = 1;
            expected.PushSerachResultItem(new SearchResultItem { Name = "Org <em>Contact</em>", Type = "organization" });

            var actual = (SearchResult)SubjUtils.CheckHttpResult(
                searchController.Search(new qlanir_web_api.DataContext.DataContextFilter { Type = "organization", Term = "contact", IncludeLinkedTags = false }));

            expected.Match(actual);

            //
            var expected1 = SubjUtils.CreateSubject<Organization>("Org Contact", profileId);
            expected1.PushLink(new ContactLink { Subject = new Contact { Label = "333", ContactType = new Tag { Id = (int)TagTypes.Skype } } });
            expected1.Contacts[0].Subject.Owner = profileId;
            expected1.Contacts[0].Subject.ContactType.Label = "skype";
            expected1.Contacts[0].Subject.ContactType.Owner = profileId;
            expected1.Contacts[0].Tag = new Tag { Id = (int)RoleTypes.Identifyers, Label = "идентификаторы", Owner = profileId };

            var foundId = actual.Results[0].Id;
            var actual1 = (Organization)SubjUtils.CheckHttpResult(subjectsController.GetOrganization(foundId));

            expected1.Match(actual1);

            orgId = actual1.Id;
        }

        void it_remove_org_with_contact()
        {
            SubjUtils.CheckHttpEmptyResult(subjectsController.DeleteOrganization(orgId));
        }

        void it_find_org_with_contact_by_no_filter_after_remove()
        {
            var expected = SubjUtils.CreateSearchResult("organization", null);
            expected.TotalCount.People = 1;
            
            var actual = (SearchResult)SubjUtils.CheckHttpResult(
            searchController.Search(new qlanir_web_api.DataContext.DataContextFilter { Type = "organization", IncludeLinkedTags = false }));

            expected.Match(actual);

        }

        ///
        void it_create_org_with_offer()
        {
            var org = SubjUtils.CreateSubject<Organization>("Org Service", profileId);
            org.PushLink(new Link<OfferBase>
            {
                Tag = new Tag { Label = "New Service" },
                Subject = new OfferBase { Label = "Service description" }
            });

            var actual = (Organization)SubjUtils.CheckHttpResult(subjectsController.PostOrganization(org));

            org.Offers[0].Subject.Owner = profileId;
            org.Offers[0].Tag.Owner = profileId;

            org.Match(actual);
        }

        void it_find_org_with_service_by_filter()
        {
            var expected = SubjUtils.CreateSearchResult("organization", "service");
            expected.TotalCount.Organizations = 1;
            expected.TotalCount.Offers = 1;
            expected.PushSerachResultItem(new SearchResultItem { Name = "Org <em>Service</em>", Type = "organization",
                                                                 Hits = new[] { 
                                                                     new SearchResultItemHit { Type = "offer", Hit = "New <em>Service</em>" }, 
                                                                     new SearchResultItemHit { Type = "offer", Hit = "<em>Service</em> description" } }
            });

            var actual = (SearchResult)SubjUtils.CheckHttpResult(
                searchController.Search(new qlanir_web_api.DataContext.DataContextFilter { Type = "organization", Term = "service", IncludeLinkedTags = false }));
            
            expected.Match(actual);

            //
            var expected1 = SubjUtils.CreateSubject<Organization>("Org Service", profileId);
            expected1.PushLink(new Link<OfferBase>
            {
                Tag = new Tag { Label = "New Service" },
                Subject = new OfferBase { Label = "Service description" }
            });
            expected1.Offers[0].Subject.Owner = profileId;
            expected1.Offers[0].Tag.Owner = profileId;

            var foundId = actual.Results[0].Id;
            var actual1 = (Organization)SubjUtils.CheckHttpResult(subjectsController.GetOrganization(foundId));

            expected1.Match(actual1);

            orgId = actual1.Id;        
        }

        void it_find_linked_offere_by_no_filter()
        {
            var expected = SubjUtils.CreateSearchResult("offer", null);
            expected.TotalCount.People = 1;
            expected.TotalCount.Organizations = 1;
            expected.TotalCount.Offers = 1;
            expected.PushSerachResultItem(new SearchResultItem
            {
                Name = "New Service",
                Type = "offer" /*"service"*/ //QL-126
            });

            var actual = (SearchResult)SubjUtils.CheckHttpResult(
                searchController.Search(new qlanir_web_api.DataContext.DataContextFilter { Type = "offer", IncludeLinkedTags = false }));

            expected.Match(actual);

            //
            /*var expected1 = new Service
            {
                Label = "New Service",
                Owner = profileId,
                Offers = new[] {
                    new OfferFull { 
                        HostSubject = new SubjectBase { Label = "Org Service", Owner = profileId, Type = "organization"},
                        Tag = new Tag { Label = "New Service", Owner = profileId}, 
                        Subject = new OfferBase { Label = "Service description", Owner = profileId}
                    }
            }}; QL-126*/

            var foundId = actual.Results[0].Id;
            /* var actual1 = (Service)SubjUtils.CheckHttpResult(servicesController.GetServices(foundId)); QL-126
            expected1.Match(actual1);*/

            var expected2 = SubjUtils.CreateSubject<Offer>("Service description", profileId);
            expected2.PushLink(new Link<OrganizationBase>
            {
                Subject = new OrganizationBase { Label = "Org Service", Owner = profileId },
                Tag = new Tag { Id = (int)RoleTypes.ReourceOwner, Owner = profileId },
                HostTag = new Tag { Id = (int)RoleTypes.Resource, Owner = profileId }
            });
            expected2.PushLink(new Tag { Label = "New Service", Owner = profileId });

            var actual2 = (Offer)SubjUtils.CheckHttpResult(servicesController.GetOffer(foundId));//actual1.Offers[0].Id)); QL-126

            expected2.Match(actual2);
        }

        void it_remove_org_with_service()
        {
            SubjUtils.CheckHttpEmptyResult(subjectsController.DeleteOrganization(orgId));
        }

        void it_find_org_with_service_by_no_filter_after_remove()
        {
            var expected = SubjUtils.CreateSearchResult("organization", null);
            expected.TotalCount.People = 1;

            var actual = (SearchResult)SubjUtils.CheckHttpResult(
            searchController.Search(new qlanir_web_api.DataContext.DataContextFilter { Type = "organization", IncludeLinkedTags = false }));

            expected.Match(actual);

        }

        /// create person with org
        void it_create_org_with_person()
        {
            var org = SubjUtils.CreateSubject<Organization>("Org Person", profileId);
            org.PushLink(new PersonLink
            {
                Tag = new Tag { Id = (int)RoleTypes.Employee },
                Subject = new PersonBase { Label = "New Person" }
            });

            var actual = (Organization)SubjUtils.CheckHttpResult(subjectsController.PostOrganization(org));

            org.People[0].Tag = new Tag { Id = (int)RoleTypes.Employee, Label = "наёмник", Owner = profileId };
            org.People[0].HostTag = new Tag { Id = (int)RoleTypes.Employer, Label = "наниматель", Owner = profileId };
            org.People[0].Subject.Owner = profileId;
            org.Match(actual);
        }

        void it_find_org_with_person_by_filter()
        {
            var expected = SubjUtils.CreateSearchResult("organization", "perso");
            expected.TotalCount.People = 1;
            expected.TotalCount.Organizations = 1;
            expected.PushSerachResultItem(new SearchResultItem
            {
                Name = "Org <em>Person</em>",
                Type = "organization",
                Hits = new[]  
                {
                    new SearchResultItemHit { Type = "person", Hit = "New <em>Person</em>"}
                }
            });

            var actual = (SearchResult)SubjUtils.CheckHttpResult(
                searchController.Search(new qlanir_web_api.DataContext.DataContextFilter { Type = "organization", Term = "perso", IncludeLinkedTags = false }));

            expected.Match(actual);

            //
            var expected1 = SubjUtils.CreateSubject<Person>("Org Person", profileId);
            expected1.PushLink(new PersonLink
            {
                Tag = new Tag { Id = (int)RoleTypes.Employee },
                Subject = new PersonBase { Label = "New Person" }
            });

            expected1.People[0].Tag = new Tag { Id = (int)RoleTypes.Employee, Label = "наёмник", Owner = profileId };
            expected1.People[0].HostTag = new Tag { Id = (int)RoleTypes.Employer, Label = "наниматель", Owner = profileId };
            expected1.People[0].Subject.Owner = profileId;

            var foundId = actual.Results[0].Id;
            var actual1 = (Organization)SubjUtils.CheckHttpResult(subjectsController.GetOrganization(foundId));

            expected1.Match(actual1);

            orgId = actual1.Id;        
        }

        void it_find_linked_person_by_no_filter()
        {
            var expected = SubjUtils.CreateSearchResult("person", null);
            expected.TotalCount.People = 2;
            expected.TotalCount.Organizations = 1;
            expected.PushSerachResultItem(new SearchResultItem
            {
                Name = "New Person",
                Type = "person"
            });
            expected.PushSerachResultItem(new SearchResultItem
            {
                Name = SubjUtils.GetUserName(),
                Type = "person"
            });

            var actual = (SearchResult)SubjUtils.CheckHttpResult(
                searchController.Search(new qlanir_web_api.DataContext.DataContextFilter { Type = "person", IncludeLinkedTags = false }));

            expected.Match(actual);
            

            //org with person
            var expected1 = SubjUtils.CreateSubject<Person>("New Person", profileId);
            expected1.PushLink(new  Link<OrganizationBase>
            {
                Subject = new OrganizationBase { Label = "Org Person" }
            });
            expected1.Organizations[0].Tag = new Tag { Id = (int)RoleTypes.Employer, Label = "наниматель", Owner = profileId };
            expected1.Organizations[0].HostTag = new Tag { Id = (int)RoleTypes.Employee, Label = "наёмник", Owner = profileId };
            expected1.Organizations[0].Subject.Owner = profileId;

            var foundId = actual.Results[0].Id;
            var actual1 = (Person)SubjUtils.CheckHttpResult(subjectsController.GetPerson(foundId));

            expected1.Match(actual1);

            personId = actual1.Id;
        }

        void it_remove_org_with_person()
        {
            SubjUtils.CheckHttpEmptyResult(subjectsController.DeleteOrganization(orgId));
        }

        void it_find_org_with_person_by_no_filter_after_remove()
        {
            var expected = SubjUtils.CreateSearchResult("organization", null);
            expected.TotalCount.People = 2;
            
            var actual = (SearchResult)SubjUtils.CheckHttpResult(
            searchController.Search(new qlanir_web_api.DataContext.DataContextFilter { Type = "organization", IncludeLinkedTags = false }));

            expected.Match(actual);
        }

        void it_remove_linked_person_to_not_obstruct_next_results()
        {
            SubjUtils.CheckHttpEmptyResult(subjectsController.DeletePerson(personId));
        }
         


        /// org with tags
        
        void it_create_org_with_tags()
        {
            var org = SubjUtils.CreateSubject<Organization>("Org Tags", profileId);
            org.Tags = new [] { new Tag { Id = - 1},  new Tag { Label = "test" }, new Tag { Label = "скидка"}};

            var actual = (Organization)SubjUtils.CheckHttpResult(subjectsController.PostOrganization(org));

            org.Tags[0].Label = "бизнес";
            org.Tags[1].Owner = profileId;
            org.Tags[2].Id = -6;

            org.Tags = org.Tags.OrderBy(p => p.Label).ToArray();

            org.Match(actual);

        }

        void it_find_org_with_tags_by_filter()
        {
            var expected = SubjUtils.CreateSearchResult("organization", "tag");
            expected.TotalCount.Organizations = 1;
            expected.PushSerachResultItem(new SearchResultItem
            {
                Name = "Org <em>Tags</em>",
                Type = "organization",
                Tags = new[] { "бизнес", "скидка", "test" }
            });

            var actual = (SearchResult)SubjUtils.CheckHttpResult(
                searchController.Search(new qlanir_web_api.DataContext.DataContextFilter { Type = "organization", Term = "tag", IncludeLinkedTags = true }));

            expected.Match(actual);

            //
            var expected1 = SubjUtils.CreateSubject<Organization>("Org Tags", profileId);
            expected1.Tags = new[] { new Tag { Id = -1 }, new Tag { Label = "test" }, new Tag { Label = "скидка" } };
            expected1.Tags[0].Label = "бизнес";
            expected1.Tags[1].Owner = profileId;
            expected1.Tags[2].Id = -6;
            expected1.Tags = expected1.Tags.OrderBy(p => p.Label).ToArray();

            var foundId = actual.Results[0].Id;
            var actual1 = (Organization)SubjUtils.CheckHttpResult(subjectsController.GetOrganization(foundId));

            expected1.Match(actual1);

            orgId = actual1.Id;        
        }

        void it_find_person_with_tags_by_tag_label_filter()
        {
            var expected = SubjUtils.CreateSearchResult("organization", "test");
            expected.TotalCount.Organizations = 1;
            expected.PushSerachResultItem(new SearchResultItem
            {
                Name = "Org Tags",
                Type = "organization",
                Tags = new[] { "бизнес", "скидка", "test" },
                Hits = new [] { new SearchResultItemHit { Type = "tag", Hit = "<em>test</em>"  }}
            });

            var actual = (SearchResult)SubjUtils.CheckHttpResult(
                searchController.Search(new qlanir_web_api.DataContext.DataContextFilter { Type = "organization", Term = "test", IncludeLinkedTags = true }));

            expected.Match(actual);
        }

        void it_find_linked_tags_by_no_filter()
        {
            var expected = new[] { new Tag { Label = "test", Owner = profileId} };

            var actual = (Tag[])SubjUtils.CheckHttpResult(
                    searchController.SearchNames(new qlanir_web_api.DataContext.DataContextFilter { Type = "tag", Term = "test" }));


            expected.Match(actual);
        }

        void it_remove_org_with_tags()
        {
            SubjUtils.CheckHttpEmptyResult(subjectsController.DeleteOrganization(orgId));
        }

        void it_find_org_with_tags_by_no_filter_after_remove()
        {
            var expected = SubjUtils.CreateSearchResult("organization", null);
            expected.TotalCount.People = 1;

            var actual = (SearchResult)SubjUtils.CheckHttpResult(
            searchController.Search(new qlanir_web_api.DataContext.DataContextFilter { Type = "organization", IncludeLinkedTags = false }));

            expected.Match(actual);
        }

        void it_find_linked_tags_by_no_filter_after_remove()
        {
            var expected = new[] { new Tag { Label = "test", Owner = profileId } };

            var actual = (Tag[])SubjUtils.CheckHttpResult(
                    searchController.SearchNames(new qlanir_web_api.DataContext.DataContextFilter { Type = "tag", Term = "test" }));


            expected.Match(actual);        
        }
        
    }
}
