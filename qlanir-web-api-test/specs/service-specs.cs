﻿using qlanir_web_api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NSpec;

namespace qlanir_web_api_test.specs
{
    class service_specs : subj_base_spec
    {
        void it_index_all()
        {
            SubjUtils.IndexAll();
        }

        void it_create_profile()
        {
            profileId = SubjUtils.CreateAndStoreProfile();
        }

        Link<OfferBase> linkedService;
        Person newPerson = null;

        void it_create_new_service_linked_to_person()
        {
            //We create service as linked to some subject (person or organization)
            var person = SubjUtils.CreateSubject<Person>("Test Person", profileId);

            person.PushLink(new Link<OfferBase>
            {
                Tag = new Tag { Label = "New Service" },
                Subject = new OfferBase { Label = "Service description" }
            });

            var actual = (Person)SubjUtils.CheckHttpResult(subjectsController.PostPerson(person));

            person.Offers[0].Subject.Owner = profileId;
            person.Offers[0].Tag.Owner = profileId;

            person.Match(actual);

            linkedService = actual.Offers[0];
        }
        
        void it_find_new_service_linked_to_person_by_description()
        {
            var expected = SubjUtils.CreateSearchResult("offer", "descr");
            expected.TotalCount.People = 1;
            expected.TotalCount.Offers = 1;
            expected.PushSerachResultItem(new SearchResultItem { Name = "New Service", Type = "offer", Hits = new[] { new SearchResultItemHit { Type = "offer", Hit = "Service <em>description</em>" } } }); //SearchResultItem.Type = "service" QL-126

            var actual = (SearchResult)SubjUtils.CheckHttpResult(
                searchController.Search(new qlanir_web_api.DataContext.DataContextFilter { Type = "offer", Term = "descr", IncludeLinkedTags = false }));

            expected.Match(actual);
        }
        
        void it_find_person_linked_to_new_service_by_description()
        {
            var expected = SubjUtils.CreateSearchResult("person", "descr");
            expected.TotalCount.People = 1;
            expected.TotalCount.Offers = 1;
            expected.PushSerachResultItem(new SearchResultItem { Name = "Test Person", Type = "person", Hits = new[] { new SearchResultItemHit { Type = "offer", Hit = "Service <em>description</em>" } } });

            var actual = (SearchResult)SubjUtils.CheckHttpResult(
                searchController.Search(new qlanir_web_api.DataContext.DataContextFilter { Type = "person", Term = "descr", IncludeLinkedTags = false }));

            expected.Match(actual);
        }

        void it_find_new_service_by_name()
        {
            var expected = SubjUtils.CreateSearchResult("offer", "serv");
            expected.TotalCount.People = 1;
            expected.TotalCount.Offers = 1;
            expected.PushSerachResultItem(new SearchResultItem 
            {
                Name = "New <em>Service</em>",
                Type = "offer", //service
                Hits = new[] { new SearchResultItemHit { Type = "offer", Hit = "<em>Service</em> description" } }

            });

            var actual = (SearchResult)SubjUtils.CheckHttpResult(
                searchController.Search(new qlanir_web_api.DataContext.DataContextFilter { Type = "offer", Term = "serv", IncludeLinkedTags = false }));

            expected.Match(actual);
        }

        void it_find_person_linked_to_new_service_by_name()
        {
            var expected = SubjUtils.CreateSearchResult("person", "serv");
            expected.TotalCount.People = 1;
            expected.TotalCount.Offers = 1;
            expected.PushSerachResultItem(new SearchResultItem
            {
                Name = "Test Person",
                Type = "person",
                Hits = new[] { new SearchResultItemHit { Type = "offer", Hit = "New <em>Service</em>" }, new SearchResultItemHit { Type = "offer", Hit = "<em>Service</em> description" } }

            });

            var actual = (SearchResult)SubjUtils.CheckHttpResult(
                searchController.Search(new qlanir_web_api.DataContext.DataContextFilter { Type = "person", Term = "serv", IncludeLinkedTags = false }));
            
            expected.Match(actual);

            var expectedPerson = SubjUtils.CreateSubject<Person>("Test Person", profileId);
            expectedPerson.Id = actual.Results[0].Id;
            expectedPerson.NameParts = new PersonNameParts { LastName = "Test", FirstName = "Person" };

            /*
                        person.PushLink(new Link<OfferBase>
            {
                Tag = new Tag { Label = "New Service" },
                Subject = new OfferBase { Label = "Service description" }
            });
             */


            expectedPerson.PushLink(new OfferLink {
                Tag = new Tag { Label = "New Service", Owner = profileId }, 
                Subject = new OfferBase { Label = "Service description", Owner = profileId } 
            });

            var actualPerson = (Person)SubjUtils.CheckHttpResult(subjectsController.GetPerson(expectedPerson.Id));

            expectedPerson.Match(actualPerson);
           
        }



        
        void it_create_person_and_service_linked_to_same_tag()
        {
            //We create service as linked to some subject (person or organization)
            var person = SubjUtils.CreateSubject<Person>("New Person", profileId);

            person.PushLink(new Link<OfferBase>
            {
                Tag = new Tag { Label = "New Service" },
                Subject = new OfferBase { Label = "Service description For New Person" }
            });


            var actual = (Person)SubjUtils.CheckHttpResult(subjectsController.PostPerson(person));


            person.Offers[0].Subject.Owner = profileId;
            person.Offers[0].Tag.Owner = profileId;

            person.Match(actual);

            //Tag Id for both services must be the same (this is Tag Id in SQL !)
            linkedService.Tag.Id.should_be(actual.Offers[0].Tag.Id);

            newPerson = actual;
        }

        void it_dlete_linked_service()
        {
            SubjUtils.CheckHttpEmptyResult(servicesController.DeleteOffer(linkedService.Subject.Id));
        }


        void it_create_new_service_with_proxy_linked_to_Person()
        {
            //We create service as linked to some subject (person or organization)
            var person = SubjUtils.CreateSubject<Person>("Test Person 2", profileId);

            person.PushLink(new OfferLink //Link<ServiceBase>
            {
                Tag = new Tag { Label = "New Service" },
                Subject = new OfferBase { Label = "Service description" },
                Proxy = new PersonBase { Label = "Proxy Person" }
            });

            var actual = (Person)SubjUtils.CheckHttpResult(subjectsController.PostPerson(person));

            actual.Contacts = actual.Contacts.OrderBy(o => o.Subject.ContactType.Id).ToArray();

            person.Offers[0].Subject.Owner = profileId;
            person.Offers[0].Proxy.Owner = profileId;
            person.Offers[0].Tag.Owner = profileId;

            person.Match(actual);

            linkedService = actual.Offers[0];
        }

        /*
         * TODO : Check user person link type
        void it_create_new_service_with_the_same_proxy_linked_to_new_person_via_new_structure()
        {
            var servicePost = new qlanir_web_api.Models.PostService
            {
                Description = "Description of service linked to new person",
                Type = new LinkedSubject { Label = "New Service Of New Person" },
                LinkedSubject = new LinkedSubjectTyped { Id = newPerson.Id, Type = "person" },
                Proxy = new LinkedSubject { Id = profileId }
            };

            var actual = (Person)SubjUtils.CheckHttpResult(servicesController.PostService(servicePost));

            var person = newPerson;

            person.PushLink(new ServiceLink
            {
                Tag = new Tag { Label = "New Service Of New Person" },
                Subject = new ServiceBase { Label = "Description of service linked to new person" },
                Proxy = new PersonBase { Id = profileId, Label = "max@mail.ru" }
            });
            person.Services[0].Subject.Owner = profileId;
            person.Services[0].Tag.Owner = profileId;
            person.Services[1].Subject.Owner = profileId;
            person.Services[1].Tag.Owner = profileId;

            person.Match(actual);

        }
         */

        Offer offer;
        OrganizationBase org;

        void it_create_new_service_with_org()
        {
            var servicePost = new qlanir_web_api.Models.PostOffer
            {
                Description = "Description of service linked to org",
                Type = new LinkedSubject { Label = "New Service Of New org" },
                LinkedSubject = new LinkedSubjectTyped { Label = "New Org", Type = "organization" }
            };

            var actual = (Offer)SubjUtils.CheckHttpResult(servicesController.PostOffer(servicePost));

            /*
            var org = SubjUtils.CreateSubject<Organization>("New Org", profileId);

            org.PushLink(new OfferLink
            {
                Tag = new Tag { Label = "New Service Of New org" },
                Subject = new OfferBase { Label = "Description of service linked to org" }
            });
            org.Offers[0].Subject.Owner = profileId;
            org.Offers[0].Tag.Owner = profileId;

            org.Match(actual);
            
            orgWithService = actual;

            linkedService = actual.Offers[0];
             */

            offer = actual;
            org = new OrganizationBase { Id = offer.Organizations[0].Subject.Id, Label = offer.Organizations[0].Subject.Label, Owner = offer.Organizations[0].Subject.Owner };
        }

        void it_get_service_by_id()
        {
            var actual = (Offer)SubjUtils.CheckHttpResult(servicesController.GetOffer(offer.Id));

            var service = SubjUtils.CreateSubject<Offer>("Description of service linked to org", profileId);
            service.Id = offer.Id;
            service.PushLink(new Tag
            {
                Label = "New Service Of New org",
                Owner = profileId
            });
            service.PushLink(new Link<OrganizationBase>
            {
                HostTag = new Tag { Id = -41, Owner = profileId },
                Tag = new Tag { Id = -42, Owner = profileId },
                Subject = org
            });

            service.Match(actual);
        }

        int ServiceTypeId;

        void it_update_service()
        {
            var servicePost = new qlanir_web_api.Models.PostOffer
            {
                Description = "Updated Description",
                Type = new LinkedSubject { Label = "New Service" },
                LinkedSubject = new LinkedSubjectTyped { Label = "New Org", Type = "organization", Id = org.Id },
                Proxy = new LinkedSubject { Label = newPerson.Label, Id = newPerson.Id }
            };

            var actual = (Offer)SubjUtils.CheckHttpResult(servicesController.PutOffer(offer.Id, servicePost));

            //orgWithService.Services[0].Subject.Label = "Updated Description";
            //orgWithService.Services[0].Tag = new Tag { Label = "New Service", Owner = profileId };
            //orgWithService.Services[0].Proxy = new PersonBase {Label = newPerson.Label, Id = newPerson.Id, Owner = profileId};

            var service = SubjUtils.CreateSubject<Offer>("Updated Description", profileId);
            service.Id = offer.Id;
            service.PushLink(new Tag
            {
                Label = "New Service",
                Owner = profileId
            });
            service.PushLink(new Link<OrganizationBase>
            {
                HostTag = new Tag { Id = -41, Owner = profileId },
                Tag = new Tag { Id = -42, Owner = profileId },
                Subject = org
            });
            service.PushLink(new PersonLink
            {
                HostTag = new Tag { Id = -43, Owner = profileId },
                Tag = new Tag { Id = -43, Owner = profileId },
                Subject = new PersonBase { Id = newPerson.Id, Label = newPerson.Label, Owner = profileId, NameParts = newPerson.NameParts }
            });

            service.Match(actual);

            ServiceTypeId = actual.Tags[0].Id;
        }

        void it_get_group_service()
        {
            var actual = (Service)SubjUtils.CheckHttpResult(servicesController.GetServices(ServiceTypeId));

            actual.Label.should_be("New Service");
            actual.Offers.Length.should_be(3);
        }


        void it_find_new_person_linked_to_service()
        {
            var expected = newPerson;

            expected.PushLink(new OfferLink {
                Tag = new Tag { Label = "New Service", Owner = profileId }, 
                Subject = new OfferBase { Label = "Updated Description", Owner = profileId }, 
                Proxy = newPerson 
            });

            var actualPerson = (Person)SubjUtils.CheckHttpResult(subjectsController.GetPerson(expected.Id));

            expected.Match(actualPerson);
        
        }

        /*
         * TODO : Test Fail
        void it_get_all_group_services()
        {
            var actual = (SearchResult)SubjUtils.CheckHttpResult(searchController.Search(new qlanir_web_api.DataContext.DataContextFilter{Type = "service"}));

            actual.Results.Length.should_be(2);
            actual.TotalCount.Services.should_be(4);
        }
         */

    }


}

