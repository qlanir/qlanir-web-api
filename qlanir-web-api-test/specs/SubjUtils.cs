﻿using qlanir_web_api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using NSpec;
using System.Web.Http.Results;
using qlanir_web_api.Controllers;
using qlanir_web_api.DataContext;
using System.Configuration;


namespace qlanir_web_api_test
{
    internal static class SubjUtils
    {
        internal static object CheckHttpResult(IHttpActionResult HttpActionResult)
        {
            HttpActionResult.should_not_be_null();
            HttpActionResult.should_cast_to<OkNegotiatedContentResult<object>>();
            return ((OkNegotiatedContentResult<object>)HttpActionResult).Content;
        }

        internal static void CheckHttpResultFail(IHttpActionResult HttpActionResult)
        {
            HttpActionResult.should_not_be_null();
            HttpActionResult.should_cast_to<InternalServerErrorResult>();

        }


        internal static void CheckHttpEmptyResult(IHttpActionResult HttpActionResult)
        {
            HttpActionResult.should_not_be_null();
            HttpActionResult.should_cast_to<OkNegotiatedContentResult<CustomOkResult>>();
        }

        internal static string GetUserName()
        {
            return ConfigurationManager.AppSettings.Get("DEV_USER");        
        }

        internal static int CreateAndStoreProfile()
        {
            var profileDataContext = new ProfileDataContext();
            var profileId = profileDataContext.CreateUserIfNotExist(GetUserName(), GetUserName(), TagTypes.Email);
            profileId.should_be_greater_than(0);

            return profileId;
        }

        internal static void IndexAll()
        {
            var adminController = new AdminController();
            SubjectsDataContext.CleanAll();
            adminController.Index();
        }

        internal static T CreateSubject<T>() where T : ISubjectWithLinks, new()
        {
            var subj = new T
            {
                Contacts = new Link<Contact>[0],
                People = new PersonLink[0],
                Organizations = new Link<OrganizationBase>[0],
                Offers = new OfferLink[0],
                Tags = new Tag[0]
            };

            /*
            if (subj is PersonBase)
            {
                ((PersonBase)(ISubjectWithLinks)subj).NameParts = new PersonNameParts();
            }
             */

            return subj;
        }


        internal static T CreateSubject<T>(string Label, int Owner) where T : ISubjectWithLinks, new()
        {
            var subj = CreateSubject<T>();

            subj.Label = Label;
            subj.Owner = Owner;

            return subj;
        }

        /*
        internal static Person CreateProfile(string label, int profileId) 
        {
            var profile = SubjUtils.CreateSubject<Person>(label, profileId);
            profile.PushLink(new ContactLink { Subject = new Contact { Label = label, Owner = profileId }, Tag = new Tag { Id = (int)TagTypes.Email, Owner = profileId, Label = "e-mail" } });
            profile.PushLink(new Tag { Id = (int)TagTypes.Profile, Label = "системные/ профайл"});

            return profile;
        }
         */

        internal static void PushLink(this ISubjectWithLinks Subject, PersonLink Link)
        {
            Subject.People = Subject.People.Union(new[] { Link }).ToArray();
        }

        internal static void PushLink(this ISubjectWithLinks Subject, ContactLink Link)
        {
            Subject.Contacts = Subject.Contacts.Union(new[] { Link }).ToArray();
        }

        internal static void PuchContactProfileLink(this Person Person, string ContactLabel, int profileId, TagTypes ProfileContactType = TagTypes.Email)
        {
            string ProfileContactTypeLabel = ProfileContactType == TagTypes.Email ? "e-mail" : ProfileContactType.ToString().ToLowerInvariant();

            Person.PushLink(new ContactLink
            {
                Tag = new Tag { Id = (int)RoleTypes.Identifyers, Owner = profileId, Label = "идентификаторы" },
                HostTag = new Tag { Id = (int)RoleTypes.Owner, Label = "владелец", Owner = profileId },
                Subject = new Contact
                {
                    Label = ContactLabel,
                    ContactType = new Tag { Id = (int)ProfileContactType, Label = ProfileContactTypeLabel, Owner = profileId },
                    Owner = profileId
                }
            });

        }

        internal static void PushLink(this ISubjectWithLinks Subject, Link<OrganizationBase> Link)
        {
            Subject.Organizations = Subject.Organizations.Union(new[] { Link }).ToArray();
        }

        internal static void PushLink(this ISubjectWithLinks Subject, Link<OfferBase> Link)
        {
            var offerLink = new OfferLink { Id = Link.Id, Subject = Link.Subject, Tag = Link.Tag, HostTag = Link.HostTag };

            Subject.Offers = Subject.Offers.Union(new[] { offerLink }).ToArray();
        }

        internal static void PushLink(this ISubjectWithLinks Subject, OfferLink Link)
        {
            var offerLink = new OfferLink { Id = Link.Id, Subject = Link.Subject, Tag = Link.Tag, HostTag = Link.HostTag, Proxy = Link.Proxy };

            Subject.Offers = Subject.Offers.Union(new[] { offerLink }).ToArray();
        }

        internal static void PushLink(this ISubjectWithLinks Subject, Tag Tag)
        {
            Subject.Tags = Subject.Tags.Union(new[] { Tag }).ToArray();
        }

        internal static SearchResult CreateSearchResult(string type, string term)
        {
            return new SearchResult
            {
                Type = type,
                Term = term,
                TotalCount = new SearchResultTotalCount(),
                Results = new SearchResultItem[0]
            };
        }

        internal static void PushSerachResultItem(this SearchResult SearchResult, SearchResultItem SearchResultItem)
        {
            if (SearchResultItem.Hits == null)
            {
                SearchResultItem.Hits = new SearchResultItemHit[0];
            }

            if (SearchResultItem.Tags == null)
            {
                SearchResultItem.Tags = new string[0];
            }

            SearchResult.Results = SearchResult.Results.Union(new[] { SearchResultItem }).ToArray();
        }



        internal static void Match(this Person Expected, Person Actual)
        {
            Actual.Birthdate.should_be(Expected.Birthdate);
            /*
            if (Actual.NameParts != null)
            {
                Expected.NameParts.should_not_be_null();
            }
             */


            //TODO
            if (Expected.NameParts != null)
            {
                Actual.NameParts.should_not_be_null();
                Actual.NameParts.FirstName.should_be(Expected.NameParts.FirstName);
                Actual.NameParts.LastName.should_be(Expected.NameParts.LastName);
                Actual.NameParts.MiddleName.should_be(Expected.NameParts.MiddleName);
                Actual.NameParts.NickName.should_be(Expected.NameParts.NickName);
            }
            
            ((ISubjectWithLinks)Expected).Match(Actual);
        }

        internal static void Match(this Pin Expected, Pin Actual)
        {
            if (Expected.Id > 0)
                Actual.Id.should_be(Expected.Id);
            else
                Actual.Id.should_be_greater_than(0);
            if (Expected.UserProxy == null)
            {
                Actual.UserProxy.should_be_null();
            }
            else
            {
                Actual.UserProxy.Id.should_be(Expected.UserProxy.Id);
                Actual.UserProxy.Label.should_be(Expected.UserProxy.Label);
                Actual.UserProxy.Owner.should_be(Expected.UserProxy.Owner);
                Actual.UserProxy.Type.should_be(Expected.UserProxy.Type);
            }
            Actual.Description.should_be(Expected.Description);
            Actual.Subject.Id.should_be(Expected.Subject.Id);
            Actual.Subject.Label.should_be(Expected.Subject.Label);
            Actual.Subject.Owner.should_be(Expected.Subject.Owner);
            Actual.Subject.Type.should_be(Expected.Subject.Type);
            Actual.UserLinkType.should_be(Expected.UserLinkType);            
            Actual.UserRole.should_be(Expected.UserRole);
        }

        internal static void MatchSubjs(ISubjectWithLinks Expected, ISubjectWithLinks Actual)
        {
            if (Expected.Id == 0)
                Actual.Id.should_be_greater_than(0);
            else
                Actual.Id.should_be(Expected.Id);
            Actual.Label.should_be(Expected.Label);
            Actual.Owner.should_be(Expected.Owner);

            Actual.Contacts.Length.should_be(Expected.Contacts.Length);
            for (var i = 0; i < Actual.Contacts.Length; i++)
            {
                Actual.Contacts[i].Id.should_be_greater_than(0);

                Actual.Contacts[i].Subject.Label.should_be(Expected.Contacts[i].Subject.Label);
                Actual.Contacts[i].Subject.Owner.should_be(Expected.Contacts[i].Subject.Owner);
                Actual.Contacts[i].Subject.Type.should_be(Expected.Contacts[i].Subject.Type);

                Actual.Contacts[i].Subject.ContactType.Id.should_be(Expected.Contacts[i].Subject.ContactType.Id);
                Actual.Contacts[i].Subject.ContactType.Label.should_be(Expected.Contacts[i].Subject.ContactType.Label);
                Actual.Contacts[i].Subject.ContactType.Owner.should_be(Expected.Contacts[i].Subject.ContactType.Owner);

                if (Expected.Contacts[i].Tag.Id == 0)
                    Actual.Contacts[i].Tag.Id.should_be_greater_than(0);
                else
                    Actual.Contacts[i].Tag.Id.should_be(Expected.Contacts[i].Tag.Id);
                Actual.Contacts[i].Tag.Label.should_be(Expected.Contacts[i].Tag.Label);
                Actual.Contacts[i].Tag.Owner.should_be(Expected.Contacts[i].Tag.Owner);
            }

            Actual.People.Length.should_be(Expected.People.Length);
            for (var i = 0; i < Actual.People.Length; i++)
            {
                Actual.People[i].Id.should_be_greater_than(0);

                Actual.People[i].Subject.Label.should_be(Expected.People[i].Subject.Label);
                Actual.People[i].Subject.Owner.should_be(Expected.People[i].Subject.Owner);
                Actual.People[i].Subject.Type.should_be(Expected.People[i].Subject.Type);

                if (Expected.People[i].Subject.NameParts != null)
                {
                    Actual.People[i].Subject.should_not_be_null();
                    Actual.People[i].Subject.NameParts.FirstName.should_be(Expected.People[i].Subject.NameParts.FirstName);
                    Actual.People[i].Subject.NameParts.LastName.should_be(Expected.People[i].Subject.NameParts.LastName);
                    Actual.People[i].Subject.NameParts.MiddleName.should_be(Expected.People[i].Subject.NameParts.MiddleName);
                    Actual.People[i].Subject.NameParts.NickName.should_be(Expected.People[i].Subject.NameParts.NickName);
                }

                if (Expected.People[i].HostTag.Id == 0)
                    Actual.People[i].HostTag.Id.should_not_be(0);
                else
                    Actual.People[i].HostTag.Id.should_be(Expected.People[i].HostTag.Id);
                Actual.People[i].HostTag.Label.should_be(Expected.People[i].HostTag.Label);
                Actual.People[i].HostTag.Owner.should_be(Expected.People[i].HostTag.Owner);

                if (Expected.People[i].Tag.Id == 0)
                    Actual.People[i].Tag.Id.should_not_be(0);
                else
                    Actual.People[i].Tag.Id.should_be(Expected.People[i].Tag.Id);
                Actual.People[i].Tag.Label.should_be(Expected.People[i].Tag.Label);
                Actual.People[i].Tag.Owner.should_be(Expected.People[i].Tag.Owner);
            }

            Actual.Organizations.Length.should_be(Expected.Organizations.Length);
            for (var i = 0; i < Actual.Organizations.Length; i++)
            {
                Actual.Organizations[i].Id.should_be_greater_than(0);

                Actual.Organizations[i].Subject.Label.should_be(Expected.Organizations[i].Subject.Label);
                Actual.Organizations[i].Subject.Owner.should_be(Expected.Organizations[i].Subject.Owner);
                Actual.Organizations[i].Subject.Type.should_be(Expected.Organizations[i].Subject.Type);

                if (Expected.Organizations[i].HostTag.Id == 0)
                    Actual.Organizations[i].HostTag.Id.should_not_be(0);
                else
                    Actual.Organizations[i].HostTag.Id.should_be(Expected.Organizations[i].HostTag.Id);
                Actual.Organizations[i].HostTag.Label.should_be(Expected.Organizations[i].HostTag.Label);
                Actual.Organizations[i].HostTag.Owner.should_be(Expected.Organizations[i].HostTag.Owner);

                if (Expected.Organizations[i].Tag.Id == 0)
                    Actual.Organizations[i].Tag.Id.should_not_be(0);
                else
                    Actual.Organizations[i].Tag.Id.should_be(Expected.Organizations[i].Tag.Id);
                Actual.Organizations[i].Tag.Label.should_be(Expected.Organizations[i].Tag.Label);
                Actual.Organizations[i].Tag.Owner.should_be(Expected.Organizations[i].Tag.Owner);
            }

            Actual.Offers.Length.should_be(Expected.Offers.Length);
            for (var i = 0; i < Actual.Offers.Length; i++)
            {
                Actual.Offers[i].Id.should_be_greater_than(0);

                Actual.Offers[i].Subject.Label.should_be(Expected.Offers[i].Subject.Label);
                Actual.Offers[i].Subject.Owner.should_be(Expected.Offers[i].Subject.Owner);
                Actual.Offers[i].Subject.Type.should_be(Expected.Offers[i].Subject.Type);

                if (Expected.Offers[i].Tag.Id == 0)
                    Actual.Offers[i].Tag.Id.should_not_be(0);
                else
                    Actual.Offers[i].Tag.Id.should_be(Expected.Offers[i].Tag.Id);
                Actual.Offers[i].Tag.Label.should_be(Expected.Offers[i].Tag.Label);
                Actual.Offers[i].Tag.Owner.should_be(Expected.Offers[i].Tag.Owner);

                if (Expected.Offers[i].Proxy == null)
                    Actual.Offers[i].Proxy.should_be_null();
                else
                {
                    Actual.Offers[i].Proxy.should_not_be_null();
                    if (Expected.Offers[i].Proxy.Id == 0)
                        Actual.Offers[i].Proxy.Id.is_greater_than(0);
                    else
                        Actual.Offers[i].Proxy.Id.should_be(Expected.Offers[i].Proxy.Id);
                    Actual.Offers[i].Proxy.Label.should_be(Expected.Offers[i].Proxy.Label);
                    if (Expected.Offers[i].Proxy.Owner == 0)
                        Actual.Offers[i].Proxy.Owner.should_be_greater_than(0);
                    else
                        Actual.Offers[i].Proxy.Owner.should_be(Expected.Offers[i].Proxy.Owner);
                    Actual.Offers[i].Proxy.Type.should_be(Expected.Offers[i].Proxy.Type);
                }

            }

            Actual.Tags.Length.should_be(Expected.Tags.Length);
            for (var i = 0; i < Actual.Tags.Length; i++)
            {
                if (Expected.Tags[i].Id == 0)
                    Actual.Tags[i].Id.should_not_be(0);
                else
                    Actual.Tags[i].Id.should_be(Expected.Tags[i].Id);
                Actual.Tags[i].Label.should_be(Expected.Tags[i].Label);
                Actual.Tags[i].Owner.should_be(Expected.Tags[i].Owner);
            }

        }

        internal static void Match(this Service Expected, Service Actual)
        {
            Actual.Label.should_be(Expected.Label);
            Actual.Owner.should_be(Expected.Owner);
            Actual.Id.should_be_greater_than(0);


            Actual.Offers.Length.should_be(Expected.Offers.Length);
            for (var i = 0; i < Actual.Offers.Length; i++)
            {
                Actual.Offers[i].Id.should_be_greater_than(0);

                if (Actual.Offers[i].HostSubject != null)
                {
                    Actual.Offers[i].HostSubject.Id.should_be_greater_than(0);
                    Actual.Offers[i].HostSubject.Label.should_be(Expected.Offers[i].HostSubject.Label);
                    Actual.Offers[i].HostSubject.Owner.should_be(Expected.Offers[i].HostSubject.Owner);
                }

                if (Actual.Offers[i].HostTag != null)
                {
                    Actual.Offers[i].HostTag.Id.should_be_greater_than(0);
                    Actual.Offers[i].HostTag.Label.should_be(Expected.Offers[i].HostTag.Label);
                    Actual.Offers[i].HostTag.Owner.should_be(Expected.Offers[i].HostTag.Owner);
                }


                if (Expected.Offers[i].Proxy != null)
                {
                    Actual.Offers[i].Proxy.Id.should_be_greater_than(0);
                    Actual.Offers[i].Proxy.Label.should_be(Expected.Offers[i].Proxy.Label);
                    Actual.Offers[i].Proxy.Owner.should_be(Expected.Offers[i].Proxy.Owner);
                }

                Actual.Offers[i].Subject.Id.should_be_greater_than(0);
                Actual.Offers[i].Subject.Label.should_be(Expected.Offers[i].Subject.Label);
                Actual.Offers[i].Subject.Owner.should_be(Expected.Offers[i].Subject.Owner);
                Actual.Offers[i].Subject.Type.should_be(Expected.Offers[i].Subject.Type);

                Actual.Offers[i].Tag.Id.should_be_greater_than(0);
                Actual.Offers[i].Tag.Label.should_be(Expected.Offers[i].Tag.Label);
                Actual.Offers[i].Tag.Owner.should_be(Expected.Offers[i].Tag.Owner);
            }

        }


        internal static void Match(this ISubjectWithLinks Expected, ISubjectWithLinks Actual)
        {
            MatchSubjs(Expected, Actual);
        }

        internal static void Match(this SearchResult Expected, SearchResult Actual)
        {
            Actual.Term.should_be(Expected.Term);
            Actual.Type.should_be(Expected.Type);
            Actual.TotalCount.People.should_be(Expected.TotalCount.People);
            Actual.TotalCount.Organizations.should_be(Expected.TotalCount.Organizations);
            Actual.TotalCount.Offers.should_be(Expected.TotalCount.Offers);

            Actual.Results.Length.should_be(Expected.Results.Length);
            for (var i = 0; i < Actual.Results.Length; i++)
            {
                Actual.Results[i].Id.should_be_greater_than(0);
                Actual.Results[i].Name.should_be(Expected.Results[i].Name);
                Actual.Results[i].Type.should_be(Expected.Results[i].Type);
                Actual.Results[i].Hits.Length.should_be(Expected.Results[i].Hits.Length);
                for (var k = 0; k < Actual.Results[i].Hits.Length; k++)
                {
                    var hit = Actual.Results[i].Hits[k];
                    var _hit = Expected.Results[i].Hits[k];
                    hit.Hit.should_be(_hit.Hit);
                    hit.Type.should_be(_hit.Type);
                }
                Actual.Results[i].Tags.Length.should_be(Expected.Results[i].Tags.Length);
                for (var k = 0; k < Actual.Results[i].Tags.Length; k++)
                {
                    var tag = Actual.Results[i].Tags[k];
                    var _tag = Expected.Results[i].Tags[k];
                    tag.should_be(_tag);
                }
            }

        }

        internal static void Match(this IEnumerable<Tag> Expected, IEnumerable<Tag> Actual)
        {
            Expected.Count().should_be(Actual.Count());

            for (var i = 0; i < Expected.Count(); i++)
            {
                var ei = Expected.ElementAt(i);
                var ai = Expected.ElementAt(i);

                ai.Id.should_be(ei.Id);
                ai.Label.should_be(ei.Label);
                ai.Owner.should_be(ei.Owner);
            }
        }

    }
}
