﻿using qlanir_web_api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qlanir_web_api_test.specs
{
    class update_subject_spec: subj_base_spec
    {
      
        void it_index_all()
        {
            SubjUtils.IndexAll();
        }

        void it_create_profile()
        {
            profileId = SubjUtils.CreateAndStoreProfile();
        }

        int orgId;

        
        void it_create_new_org_simple()
        {
        
            var org = SubjUtils.CreateSubject<Organization>("Org Simple", profileId);
           
            var actual = (Organization) SubjUtils.CheckHttpResult(subjectsController.PostOrganization(org));

            org.Match(actual);

            orgId = actual.Id;
        }

        void it_update_org_name()
        {
            //This is the case when we change name of the organization inline
            var org = new Organization { Label = "Org Simple !" };

            var actual = (Organization)SubjUtils.CheckHttpResult(subjectsController.PutOrganization(org, orgId));

            var orgRes = SubjUtils.CreateSubject<Organization>("Org Simple !", profileId);
            orgRes.Match(actual);
        }

    }
}
