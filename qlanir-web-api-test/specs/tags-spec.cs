﻿using qlanir_web_api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NSpec;

namespace qlanir_web_api_test.specs
{
        class tags_spec : subj_base_spec
        {

            void it_index_all()
            {
                SubjUtils.IndexAll();
            }

            void it_create_profile()
            {
                profileId = SubjUtils.CreateAndStoreProfile();
            }

            int tagId = 0;

            void it_create_org_with_tag_and_person_with_the_same_tag()
            {
                var org = SubjUtils.CreateSubject<Organization>("Org Tags", profileId);
                org.Tags = new[] { new Tag { Label = "test" } };

                SubjUtils.CheckHttpResult(subjectsController.PostOrganization(org));

                var person = SubjUtils.CreateSubject<Person>("Person Tags", profileId);
                person.Tags = new[] { new Tag { Label = "test" } };

                SubjUtils.CheckHttpResult(subjectsController.PostPerson(person));

            }

            void it_find_test_tag_should_return_single()
            {
                var person = SubjUtils.CreateSubject<Person>("Person Tags", profileId);
                person.Tags = new[] { new Tag { Label = "test" } };

                var tags = (Tag[])SubjUtils.CheckHttpResult(searchController.SearchNames(new qlanir_web_api.DataContext.DataContextFilter { Term = "test", Type = "tag", IncludeLinkedTags = false }));

                tags.Length.should_be(1);
                tags[0].Label.should_be("test");
                tags[0].Id.should_be_greater_than(0);

                tagId = tags[0].Id;
            }

            void it_test_tags_link_count_should_be_2()
            {
                var linksCount = (int)SubjUtils.CheckHttpResult(tagsController.GetTagLinksCount(tagId));
                linksCount.should_be(2);
            }

            //TODO : #if DEBUG

            /*
            void it_create_another_test_tag_should_return_error()
            {
                SubjUtils.CheckHttpResultFail(tagsController.CreateTag("test"));
            }
             */


            void it_remove_test_tag()
            {
                SubjUtils.CheckHttpEmptyResult(tagsController.RemoveTag(tagId));
            }

            void it_find_test_tag_after_remove()
            {
                var tags = (Tag[])SubjUtils.CheckHttpResult(searchController.SearchNames(new qlanir_web_api.DataContext.DataContextFilter { Term = "test", Type = "tag", IncludeLinkedTags = false }));

                tags.Length.should_be(0);
            }

            void it_find_subjects_by_test_tag_after_remove()
            {

                var result1 = (SearchResult)SubjUtils.CheckHttpResult(searchController.Search(
                    new qlanir_web_api.DataContext.DataContextFilter { Term = "test", Type = "person", IncludeLinkedTags = true }));
                var result2 = (SearchResult)SubjUtils.CheckHttpResult(searchController.Search(
                    new qlanir_web_api.DataContext.DataContextFilter { Term = "test", Type = "organization", IncludeLinkedTags = true }));


                result1.Results.Length.should_be(0);
                result2.Results.Length.should_be(0);
            }

            void it_find_subjects_by_no_filter_after_remove()
            {

                var result1 = (SearchResult)SubjUtils.CheckHttpResult(searchController.Search(
                    new qlanir_web_api.DataContext.DataContextFilter { Term = null, Type = "person", IncludeLinkedTags = true }));
                var result2 = (SearchResult)SubjUtils.CheckHttpResult(searchController.Search(
                    new qlanir_web_api.DataContext.DataContextFilter { Term = null, Type = "organization", IncludeLinkedTags = true }));


                result1.Results.Length.should_be(2);
                result2.Results.Length.should_be(1);
            }

            void it_test_tags_link_count_after_remove_should_be_0()
            {
                var linksCount = (int)SubjUtils.CheckHttpResult(tagsController.GetTagLinksCount(tagId));
                linksCount.should_be(0);
            }

            void it_create_new_tag()
            {
                var result = (Tag)SubjUtils.CheckHttpResult(tagsController.CreateTag("New Tag"));

                result.Id.should_be_greater_than(0);
                result.Owner.should_be_greater_than(0);
                result.Label.should_be("New Tag");
            }

            void it_find_new_tag_should_return_single()
            {
                var tags = (Tag[])SubjUtils.CheckHttpResult(searchController.SearchNames(new qlanir_web_api.DataContext.DataContextFilter { Term = "new", Type = "tag", IncludeLinkedTags = false }));

                tags.Length.should_be(1);
                tags[0].Label.should_be("New Tag");
                tags[0].Id.should_be_greater_than(0);

                tagId = tags[0].Id;
            }

            void it_new_tag_links_count_should_be_0()
            {
                var linksCount = (int)SubjUtils.CheckHttpResult(tagsController.GetTagLinksCount(tagId));
                linksCount.should_be(0);
            }

            void it_remove_new_tag()
            {
                SubjUtils.CheckHttpEmptyResult(tagsController.RemoveTag(tagId));
            }

            void it_find_new_tag_after_remove()
            {
                var tags = (Tag[])SubjUtils.CheckHttpResult(searchController.SearchNames(new qlanir_web_api.DataContext.DataContextFilter { Term = "new", Type = "tag", IncludeLinkedTags = false }));

                tags.Length.should_be(0);
            }
        }
}
