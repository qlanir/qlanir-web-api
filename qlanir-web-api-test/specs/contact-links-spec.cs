﻿using qlanir_web_api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qlanir_web_api_test.specs
{
    class contact_links_spec : subj_base_spec
    {
        void it_index_all()
        {
            SubjUtils.IndexAll();
        }

        void it_create_profile()
        {
            profileId = SubjUtils.CreateAndStoreProfile();
        }

        void it_create_new_person_with_contact()
        {
            var person = SubjUtils.CreateSubject<Person>("Person WithContact", profileId);

            person.PushLink(new ContactLink { Tag = new Tag { Id = (int)RoleTypes.WorkIdent }, 
                Subject = new Contact { Label = "0000", ContactType = new Tag { Id = (int)TagTypes.Skype } } });

            var actual = (Person)SubjUtils.CheckHttpResult(subjectsController.PostPerson(person));

            person.Contacts[0].Subject.Owner = profileId;
            person.Contacts[0].Subject.ContactType.Label = "skype";
            person.Contacts[0].Subject.ContactType.Owner = profileId;
            person.Contacts[0].Tag = new Tag { Id = (int)RoleTypes.WorkIdent, 
                Label = "рабочий", Owner = profileId };
            
            person.Match(actual);
        }

    }
}
