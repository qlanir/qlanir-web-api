﻿using qlanir_web_api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qlanir_web_api_test.specs
{
    class org_update_spec : subj_base_spec
    {
        Organization _org;
      
        
        void it_index_all()
        {
            SubjUtils.IndexAll();
        }

        void it_create_profile()
        {
            profileId = SubjUtils.CreateAndStoreProfile();
        }

        void it_create_org()
        {
            var org = SubjUtils.CreateSubject<Organization>("Org Full", profileId);
            org.PushLink(new ContactLink { Subject = new Contact { Label = "333", ContactType = new Tag { Id = (int)TagTypes.Skype } } });
            org.PushLink(new PersonLink
            {
                Tag = new Tag { Id = (int)RoleTypes.Employee },
                Subject = new PersonBase { Label = "New Person" }
            });
            org.PushLink(new Link<OfferBase>
            {
                Tag = new Tag { Label = "New Service" },
                Subject = new OfferBase { Label = "Offer description" }
            });
            org.Tags = new[] { new Tag { Id = -1 }, new Tag { Label = "test" }, new Tag { Label = "скидка" } };

            var actual = (Organization)SubjUtils.CheckHttpResult(subjectsController.PostOrganization(org));

            org.Contacts[0].Subject.Owner = profileId;
            org.Contacts[0].Subject.ContactType.Label = "skype";
            org.Contacts[0].Subject.ContactType.Owner = profileId;
            org.Contacts[0].Tag = new Tag { Id = (int)RoleTypes.Identifyers, Label = "идентификаторы", Owner = profileId };

            org.People[0].Tag = new Tag { Id = (int)RoleTypes.Employee, Label = "наёмник", Owner = profileId };
            org.People[0].HostTag = new Tag { Id = (int)RoleTypes.Employer, Label = "наниматель", Owner = profileId };
            org.People[0].Subject.Owner = profileId;

            org.Offers[0].Subject.Owner = profileId;
            org.Offers[0].Tag.Owner = profileId;

            org.Tags[0].Label = "бизнес";
            org.Tags[1].Owner = profileId;
            org.Tags[2].Id = -6;
            org.Tags = org.Tags.OrderBy(p => p.Label).ToArray();

            org.Match(actual);

            _org = actual;
        }

        void it_update_org()
        {
            var org = SubjUtils.CreateSubject<Organization>("Org Updated", profileId);
            org.PushLink(new ContactLink 
            {
                Id = _org.Contacts[0].Id,
                Subject = new Contact { Label = "facebook org", ContactType = new Tag { Id = (int)TagTypes.Facebook } } 
            });
            org.PushLink(new PersonLink
            {
                Id = _org.People[0].Id,
                Tag = new Tag { Id = (int)RoleTypes.Owner },
                Subject = new PersonBase { Label = "Updated Person" }
            });
            org.PushLink(new Link<OfferBase>
            {
                Id = _org.Offers[0].Id,
                Tag = new Tag { Label = "Updated Service" },
                Subject = new OfferBase { Id = _org.Offers[0].Subject.Id, Label = "Updated service description" }
            });
            org.Tags = new[] { new Tag { Label = "test" } };

            var actual = (Organization)SubjUtils.CheckHttpResult(subjectsController.PutOrganization(org, _org.Id));

            org.Contacts[0].Subject.Owner = profileId;
            org.Contacts[0].Subject.ContactType.Label = "facebook";
            org.Contacts[0].Subject.ContactType.Owner = profileId;
            org.Contacts[0].Tag = new Tag { Id = (int)RoleTypes.Identifyers, Label = "идентификаторы", Owner = profileId };

            org.People[0].Tag = new Tag { Id = (int)RoleTypes.Owner, Label = "владелец", Owner = profileId };
            org.People[0].HostTag = new Tag { Id = (int)RoleTypes.Employer, Label = "наниматель", Owner = profileId };
            org.People[0].Subject.Owner = profileId;

            org.Offers[0].Subject.Owner = profileId;
            org.Offers[0].Tag.Owner = profileId;

            org.Tags[0].Owner = profileId;

            org.Match(actual);
        }       
    }
}
