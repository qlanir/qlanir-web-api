﻿using qlanir_web_api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qlanir_web_api_test.specs
{
    class person_create_spec : subj_base_spec
    {

        int personId;
        int orgId;

        void it_index_all()
        {
            SubjUtils.IndexAll();
        }

        void it_create_profile()
        {
            profileId = SubjUtils.CreateAndStoreProfile();
        }
         
        void it_create_simple_person()
        {
            var person = SubjUtils.CreateSubject<Person>("Person Simple", profileId);
            person.Birthdate = "10.05.1979";

            var actual = (Person)SubjUtils.CheckHttpResult(subjectsController.PostPerson(person));

            person.Match(actual);
        }

        void it_find_simple_person_by_no_filter()
        {
            var expected = SubjUtils.CreateSearchResult("person", null);
            expected.TotalCount.People = 2;
            expected.PushSerachResultItem(new SearchResultItem { Name = "Person Simple", Type = "person" });
            expected.PushSerachResultItem(new SearchResultItem { Name = SubjUtils.GetUserName(), Type = "person" });
            
            
            var actual = (SearchResult)SubjUtils.CheckHttpResult(
                searchController.Search(new qlanir_web_api.DataContext.DataContextFilter { Type = "person", IncludeLinkedTags = false }));

            expected.Match(actual);
        }

        void it_find_simple_person_by_filter()
        {
            var expected = SubjUtils.CreateSearchResult("person", "sample");
            expected.TotalCount.People = 1;
            expected.PushSerachResultItem(new SearchResultItem { Name = "Person <em>Simple</em>", Type = "person" });

            var actual = (SearchResult)SubjUtils.CheckHttpResult(
                searchController.Search(new qlanir_web_api.DataContext.DataContextFilter { Type = "person", Term ="sample", IncludeLinkedTags = false }));

            expected.Match(actual);

            //
            var expected1 = SubjUtils.CreateSubject<Person>("Person Simple", profileId);
            expected1.Birthdate = "10.05.1979";
            var foundId = actual.Results[0].Id;
            var actual1 = (Person)SubjUtils.CheckHttpResult(subjectsController.GetPerson(foundId));

            expected1.Match(actual1);

            personId = actual1.Id;
        }

        void it_find_simple_person_by_name_at_the_end()
        {
            var expected = SubjUtils.CreateSearchResult("person", "ple");
            expected.TotalCount.People = 1;
            expected.PushSerachResultItem(new SearchResultItem { Name = "Person <em>Simple</em>", Type = "person" });

            var actual = (SearchResult)SubjUtils.CheckHttpResult(
                searchController.Search(new qlanir_web_api.DataContext.DataContextFilter { Type = "person", Term = "ple", IncludeLinkedTags = false }));

            expected.Match(actual);
        }
                 
        //BUGFIX : QL-91, passed no changes here
        void it_find_simple_person_by_filter_with_part_of_the_name()
        {
            var expected = SubjUtils.CreateSearchResult("person", "impl");
            expected.TotalCount.People = 1;
            expected.PushSerachResultItem(new SearchResultItem { Name = "Person <em>Simple</em>", Type = "person" });

            var actual = (SearchResult)SubjUtils.CheckHttpResult(
                searchController.Search(new qlanir_web_api.DataContext.DataContextFilter { Type = "person", Term = "impl", IncludeLinkedTags = false }));

            expected.Match(actual);

            //
            var expected1 = SubjUtils.CreateSubject<Person>("Person Simple", profileId);
            expected1.Birthdate = "10.05.1979";
            var foundId = actual.Results[0].Id;
            var actual1 = (Person)SubjUtils.CheckHttpResult(subjectsController.GetPerson(foundId));

            expected1.Match(actual1);

            personId = actual1.Id;
        }

        
        void it_remove_simple_person()
        {
            SubjUtils.CheckHttpEmptyResult(subjectsController.DeletePerson(personId));
        }
        

        //BUGFIX: QL-94
        void it_create_simple_person_with_middle_name()
        {
            var person = SubjUtils.CreateSubject<Person>("Person With Middle", profileId);

            var actual = (Person)SubjUtils.CheckHttpResult(subjectsController.PostPerson(person));

            person.Match(actual);
        }


        void it_find_simple_person_with_middle_by_filter()
        {
            var expected = SubjUtils.CreateSearchResult("person", "middle");
            expected.TotalCount.People = 1;
            expected.PushSerachResultItem(new SearchResultItem { Name = "Person With <em>Middle</em>", Type = "person" });

            var actual = (SearchResult)SubjUtils.CheckHttpResult(
                searchController.Search(new qlanir_web_api.DataContext.DataContextFilter { Type = "person", Term = "middle", IncludeLinkedTags = false }));

            expected.Match(actual);

            //
            var expected1 = SubjUtils.CreateSubject<Person>("Person With Middle", profileId);
            
            var foundId = actual.Results[0].Id;
            var actual1 = (Person)SubjUtils.CheckHttpResult(subjectsController.GetPerson(foundId));

            expected1.Match(actual1);

            personId = actual1.Id;
        }

        void it_remove_simple_person_with_middle()
        {
            SubjUtils.CheckHttpEmptyResult(subjectsController.DeletePerson(personId));
        }

         
        void it_find_simple_person_by_no_filter_after_remove()
        {
            var expected = SubjUtils.CreateSearchResult("person", null);
            expected.TotalCount.People = 1;
            expected.PushSerachResultItem(new SearchResultItem { Name = SubjUtils.GetUserName(), Type = "person" });

            var actual = (SearchResult)SubjUtils.CheckHttpResult(
                searchController.Search(new qlanir_web_api.DataContext.DataContextFilter { Type = "person", IncludeLinkedTags = false }));

            expected.Match(actual);
        }

        ///
        void it_create_person_with_contact()
        {
            var person = SubjUtils.CreateSubject<Person>("Person Contact", profileId);
            person.PushLink(new ContactLink { Subject = new Contact { Label = "333", ContactType = new Tag { Id = (int)TagTypes.Skype } } });

            var actual = (Person)SubjUtils.CheckHttpResult(subjectsController.PostPerson(person));

            person.Contacts[0].Subject.Owner = profileId;
            person.Contacts[0].Subject.ContactType.Label = "skype";
            person.Contacts[0].Subject.ContactType.Owner = profileId;
            person.Contacts[0].Tag = new Tag { Id = (int)RoleTypes.Identifyers, Label = "идентификаторы", Owner = profileId };

            person.Match(actual);
        }

        void it_find_person_with_contact_by_filter()
        {
            var expected = SubjUtils.CreateSearchResult("person", "contact");
            expected.TotalCount.People = 1;
            expected.PushSerachResultItem(new SearchResultItem { Name = "Person <em>Contact</em>", Type = "person" });

            var actual = (SearchResult)SubjUtils.CheckHttpResult(
                searchController.Search(new qlanir_web_api.DataContext.DataContextFilter { Type = "person", Term = "contact", IncludeLinkedTags = false }));

            expected.Match(actual);

            //
            var expected1 = SubjUtils.CreateSubject<Person>("Person Contact", profileId);
            expected1.PushLink(new ContactLink { Subject = new Contact { Label = "333", ContactType = new Tag { Id = (int)TagTypes.Skype } } });
            expected1.Contacts[0].Subject.Owner = profileId;
            expected1.Contacts[0].Subject.ContactType.Label = "skype";
            expected1.Contacts[0].Subject.ContactType.Owner = profileId;
            expected1.Contacts[0].Tag = new Tag { Id = (int)RoleTypes.Identifyers, Label = "идентификаторы", Owner = profileId };

            var foundId = actual.Results[0].Id;
            var actual1 = (Person)SubjUtils.CheckHttpResult(subjectsController.GetPerson(foundId));

            expected1.Match(actual1);

            personId = actual1.Id;
        }

        void it_remove_person_with_contact()
        {
            SubjUtils.CheckHttpEmptyResult(subjectsController.DeletePerson(personId));
        }

        void it_find_person_with_contact_by_no_filter_after_remove()
        {
            var expected = SubjUtils.CreateSearchResult("person", null);
            expected.TotalCount.People = 1;
            expected.PushSerachResultItem(new SearchResultItem { Name = SubjUtils.GetUserName(), Type = "person" });

            var actual = (SearchResult)SubjUtils.CheckHttpResult(
            searchController.Search(new qlanir_web_api.DataContext.DataContextFilter { Type = "person", IncludeLinkedTags = false }));

            expected.Match(actual);

        }


        ///
        void it_create_person_with_service()
        {
            var person = SubjUtils.CreateSubject<Person>("Person Service", profileId);
            person.PushLink(new Link<OfferBase>
            {
                Tag = new Tag { Label = "New Service" },
                Subject = new OfferBase { Label = "Service description" }
            });

            var actual = (Person)SubjUtils.CheckHttpResult(subjectsController.PostPerson(person));

            person.Offers[0].Subject.Owner = profileId;
            person.Offers[0].Tag.Owner = profileId;

            person.Match(actual);
        }

        void it_find_person_with_service_by_filter()
        {
            var expected = SubjUtils.CreateSearchResult("person", "service");
            expected.TotalCount.People = 1;
            expected.TotalCount.Offers = 1;
            expected.PushSerachResultItem(new SearchResultItem { Name = "Person <em>Service</em>", Type = "person", Hits = new []  {
                new SearchResultItemHit { Type = "offer", Hit = "New <em>Service</em>"}, new SearchResultItemHit { Type = "offer", Hit = "<em>Service</em> description"}
            }});

            var actual = (SearchResult)SubjUtils.CheckHttpResult(
                searchController.Search(new qlanir_web_api.DataContext.DataContextFilter { Type = "person", Term = "service", IncludeLinkedTags = false }));
            
            expected.Match(actual);

            //
            var expected1 = SubjUtils.CreateSubject<Person>("Person Service", profileId);
            expected1.PushLink(new Link<OfferBase>
            {
                Tag = new Tag { Label = "New Service" },
                Subject = new OfferBase { Label = "Service description" }
            });
            expected1.Offers[0].Subject.Owner = profileId;
            expected1.Offers[0].Tag.Owner = profileId;

            var foundId = actual.Results[0].Id;
            var actual1 = (Person)SubjUtils.CheckHttpResult(subjectsController.GetPerson(foundId));

            expected1.Match(actual1);

            personId = actual1.Id;        
        }

        void it_find_linked_service_by_no_filter()
        {
            var expected = SubjUtils.CreateSearchResult("offer", null);
            expected.TotalCount.People = 2;
            expected.TotalCount.Offers = 1;
            expected.PushSerachResultItem(new SearchResultItem
            {
                Name = "New Service",
                Type = "offer" //service
            });

            var actual = (SearchResult)SubjUtils.CheckHttpResult(
                searchController.Search(new qlanir_web_api.DataContext.DataContextFilter { Type = "offer", IncludeLinkedTags = false }));

            expected.Match(actual);

            //
            //var expected1 = new Service
            //{
            //    Label = "New Service",
            //    Owner = profileId,
            //    Offers = new[] {
            //        new OfferFull { 
            //            HostSubject = new SubjectBase { Label = "Person Service", Owner = profileId, Type = "person"},
            //            Tag = new Tag { Label = "New Service", Owner = profileId}, 
            //            Subject = new OfferBase { Label = "Service description", Owner = profileId}
            //        }
            //}}; QL-126

            //var foundId = actual.Results[0].Id;
            //var actual1 = (Service)SubjUtils.CheckHttpResult(servicesController.GetServices(foundId));

            //expected1.Match(actual1); QL-126


            //var expected2 = SubjUtils.CreateSubject<Offer>("Service description", profileId);
            //expected2.PushLink(new PersonLink
            //{
            //    Subject = new PersonBase { Label = "Person Service", Owner = profileId },
            //    Tag = new Tag { Id = (int)RoleTypes.ReourceOwner, Owner = profileId },
            //    HostTag = new Tag { Id = (int)RoleTypes.Resource, Owner = profileId }
            //});
            //expected2.PushLink(new Tag { Label = "New Service", Owner = profileId });

            //var actual2 = (Offer)SubjUtils.CheckHttpResult(servicesController.GetOffer(foundId));//actual1.Offers[0].Id)); QL-126

            //expected2.Match(actual2);
        }

        void it_remove_person_with_service()
        {
            SubjUtils.CheckHttpEmptyResult(subjectsController.DeletePerson(personId));
        }

        void it_find_person_with_service_by_no_filter_after_remove()
        {
            var expected = SubjUtils.CreateSearchResult("person", null);
            expected.TotalCount.People = 1;
            expected.PushSerachResultItem(new SearchResultItem { Name = SubjUtils.GetUserName(), Type = "person" });

            var actual = (SearchResult)SubjUtils.CheckHttpResult(
            searchController.Search(new qlanir_web_api.DataContext.DataContextFilter { Type = "person", IncludeLinkedTags = false }));

            expected.Match(actual);

        }

        /// create person with org
        void it_create_person_with_org()
        {
            var person = SubjUtils.CreateSubject<Person>("Person Org", profileId);
            person.PushLink(new Link<OrganizationBase>
            {
                Subject = new OrganizationBase { Label = "New Org" }
            });

            var actual = (Person)SubjUtils.CheckHttpResult(subjectsController.PostPerson(person));

            person.Organizations[0].HostTag = new Tag { Id = (int)RoleTypes.Employee, Label = "наёмник", Owner = profileId };
            person.Organizations[0].Tag = new Tag { Id = (int)RoleTypes.Employer, Label = "наниматель", Owner = profileId };
            person.Organizations[0].Subject.Owner = profileId;
            person.Match(actual);
        }

        void it_find_person_with_org_by_filter()
        {
            var expected = SubjUtils.CreateSearchResult("person", "org");
            expected.TotalCount.People = 1;
            expected.TotalCount.Organizations = 1;
            expected.PushSerachResultItem(new SearchResultItem
            {
                Name = "Person <em>Org</em>",
                Type = "person",
                Hits = new[]  
                {
                    new SearchResultItemHit { Type = "organization", Hit = "New <em>Org</em>"}
                }
            });

            var actual = (SearchResult)SubjUtils.CheckHttpResult(
                searchController.Search(new qlanir_web_api.DataContext.DataContextFilter { Type = "person", Term = "org", IncludeLinkedTags = false }));

            expected.Match(actual);

            //
            var expected1 = SubjUtils.CreateSubject<Person>("Person Org", profileId);
            expected1.PushLink(new Link<OrganizationBase>
            {
                Subject = new OrganizationBase { Label = "New Org" }
            });
            expected1.Organizations[0].HostTag = new Tag { Id = (int)RoleTypes.Employee, Label = "наёмник", Owner = profileId };
            expected1.Organizations[0].Tag = new Tag { Id = (int)RoleTypes.Employer, Label = "наниматель", Owner = profileId };
            expected1.Organizations[0].Subject.Owner = profileId;

            var foundId = actual.Results[0].Id;
            var actual1 = (Person)SubjUtils.CheckHttpResult(subjectsController.GetPerson(foundId));

            expected1.Match(actual1);

            personId = actual1.Id;        
        }

        void it_find_linked_org_by_no_filter()
        {
            var expected = SubjUtils.CreateSearchResult("organization", null);
            expected.TotalCount.People = 2;
            expected.TotalCount.Organizations = 1;
            expected.PushSerachResultItem(new SearchResultItem
            {
                Name = "New Org",
                Type = "organization"
            });

            var actual = (SearchResult)SubjUtils.CheckHttpResult(
                searchController.Search(new qlanir_web_api.DataContext.DataContextFilter { Type = "organization", IncludeLinkedTags = false }));

            expected.Match(actual);

            //
            var expected1 = SubjUtils.CreateSubject<Organization>("New Org", profileId);
            expected1.PushLink(new PersonLink
            {
                Subject = new PersonBase { Label = "Person Org" }
            });
            expected1.People[0].HostTag = new Tag { Id = (int)RoleTypes.Employer, Label = "наниматель", Owner = profileId };
            expected1.People[0].Tag = new Tag { Id = (int)RoleTypes.Employee, Label = "наёмник", Owner = profileId };
            expected1.People[0].Subject.Owner = profileId;

            var foundId = actual.Results[0].Id;
            var actual1 = (Organization)SubjUtils.CheckHttpResult(subjectsController.GetOrganization(foundId));

            expected1.Match(actual1);

            orgId = actual1.Id;
        }

        void it_remove_person_with_org()
        {
            SubjUtils.CheckHttpEmptyResult(subjectsController.DeletePerson(personId));
        }

        void it_find_person_with_org_by_no_filter_after_remove()
        {
            var expected = SubjUtils.CreateSearchResult("person", null);
            expected.TotalCount.People = 1;
            expected.TotalCount.Organizations = 1;
            expected.PushSerachResultItem(new SearchResultItem { Name = SubjUtils.GetUserName(), Type = "person" });

            var actual = (SearchResult)SubjUtils.CheckHttpResult(
            searchController.Search(new qlanir_web_api.DataContext.DataContextFilter { Type = "person", IncludeLinkedTags = false }));

            expected.Match(actual);
        }

        void it_remove_linked_org_to_not_obstruct_next_results()
        {
            SubjUtils.CheckHttpEmptyResult(subjectsController.DeleteOrganization(orgId));
        }
         

        /// create person with org & searvice (BUGFIX : QL-90)
        void it_create_person_with_org_and_service()
        {
            var person = SubjUtils.CreateSubject<Person>("Person ServiceOrg", profileId);
            
            
            person.PushLink(new Link<OrganizationBase>
            {
                Subject = new OrganizationBase { Label = "New Org+" },
            });


            person.PushLink(new OfferLink
            {
                Tag = new Tag { Label = "New Service+" }
            });
             

            var actual = (Person)SubjUtils.CheckHttpResult(subjectsController.PostPerson(person));
    
            person.Organizations[0].HostTag = new Tag { Id = (int)RoleTypes.Employee, Label = "наёмник", Owner = profileId };
            person.Organizations[0].Tag = new Tag { Id = (int)RoleTypes.Employer, Label = "наниматель", Owner = profileId };
            person.Organizations[0].Subject.Owner = profileId;
           
            person.Offers[0].Tag.Owner = profileId;

            person.Offers[0].Subject = new OfferBase { Owner = profileId };

            person.Match(actual);
        }

        void it_find_person_with_org_and_service_by_filter()
        {
            var expected = SubjUtils.CreateSearchResult("person", "service");
            expected.TotalCount.People = 1;
            expected.TotalCount.Organizations = 1;
            expected.TotalCount.Offers = 1;
            expected.PushSerachResultItem(new SearchResultItem
            {
                Name = "Person <em>ServiceOrg</em>",
                Type = "person",
                Hits = new[]  
                {
                    new SearchResultItemHit { Type = "offer", Hit = "New <em>Service+</em>"}
                }
            });

            var actual = (SearchResult)SubjUtils.CheckHttpResult(
                searchController.Search(new qlanir_web_api.DataContext.DataContextFilter { Type = "person", Term = "service", IncludeLinkedTags = false }));

            expected.Match(actual);

            //remove all created items to not obstruct next etsts
            var actual1 = (Person)SubjUtils.CheckHttpResult(subjectsController.GetPerson(actual.Results[0].Id));

            SubjUtils.CheckHttpEmptyResult(subjectsController.DeletePerson(actual1.Id));
            SubjUtils.CheckHttpEmptyResult(subjectsController.DeleteOrganization(actual1.Organizations[0].Subject.Id));
        }
        

        /// person with tags
        
        void it_create_person_with_tags()
        {
            var person = SubjUtils.CreateSubject<Person>("Person Tags", profileId);
            person.Tags = new [] { new Tag { Id = - 1},  new Tag { Label = "test" }, new Tag { Label = "скидка"}};

            var actual = (Person)SubjUtils.CheckHttpResult(subjectsController.PostPerson(person));

            person.Tags[0].Label = "бизнес";
            person.Tags[1].Owner = profileId;
            person.Tags[2].Id = -6;

            person.Tags = person.Tags.OrderBy(p => p.Label).ToArray();

            person.Match(actual);

        }

        void it_find_person_with_tags_by_filter()
        {
            var expected = SubjUtils.CreateSearchResult("person", "tag");
            expected.TotalCount.People = 1;
            expected.PushSerachResultItem(new SearchResultItem
            {
                Name = "Person <em>Tags</em>",
                Type = "person",
                Tags = new[] { "бизнес", "скидка", "test" }
            });

            var actual = (SearchResult)SubjUtils.CheckHttpResult(
                searchController.Search(new qlanir_web_api.DataContext.DataContextFilter { Type = "person", Term = "tag", IncludeLinkedTags = true }));

            expected.Match(actual);

            //
            var expected1 = SubjUtils.CreateSubject<Person>("Person Tags", profileId);
            expected1.Tags = new[] { new Tag { Id = -1 }, new Tag { Label = "test" }, new Tag { Label = "скидка" } };
            expected1.Tags[0].Label = "бизнес";
            expected1.Tags[1].Owner = profileId;
            expected1.Tags[2].Id = -6;
            expected1.Tags = expected1.Tags.OrderBy(p => p.Label).ToArray();

            var foundId = actual.Results[0].Id;
            var actual1 = (Person)SubjUtils.CheckHttpResult(subjectsController.GetPerson(foundId));

            expected1.Match(actual1);

            personId = actual1.Id;        
        
        }

        void it_find_person_with_tags_by_tag_label_filter()
        {
            var expected = SubjUtils.CreateSearchResult("person", "test");
            expected.TotalCount.People = 1;
            expected.PushSerachResultItem(new SearchResultItem
            {
                Name = "Person Tags",
                Type = "person",
                Tags = new[] { "бизнес", "скидка", "test" },
                Hits = new [] { new SearchResultItemHit { Type = "tag", Hit = "<em>test</em>"  }}
            });

            var actual = (SearchResult)SubjUtils.CheckHttpResult(
                searchController.Search(new qlanir_web_api.DataContext.DataContextFilter { Type = "person", Term = "test", IncludeLinkedTags = true }));

            expected.Match(actual);
        }

        void it_find_linked_tags_by_no_filter()
        {
            var expected = new[] { new Tag { Label = "test", Owner = profileId} };

            var actual = (Tag[])SubjUtils.CheckHttpResult(
                    searchController.SearchNames(new qlanir_web_api.DataContext.DataContextFilter { Type = "tag", Term = "test" }));


            expected.Match(actual);
        }

        void it_remove_person_with_tags()
        {
            SubjUtils.CheckHttpEmptyResult(subjectsController.DeletePerson(personId));
        }

        void it_find_person_with_tags_by_no_filter_after_remove()
        {
            var expected = SubjUtils.CreateSearchResult("person", null);
            expected.TotalCount.People = 1;
            expected.PushSerachResultItem(new SearchResultItem { Name = SubjUtils.GetUserName(), Type = "person" });

            var actual = (SearchResult)SubjUtils.CheckHttpResult(
            searchController.Search(new qlanir_web_api.DataContext.DataContextFilter { Type = "person", IncludeLinkedTags = false }));

            expected.Match(actual);
        }

        void it_find_linked_tags_by_no_filter_after_remove()
        {
            var expected = new[] { new Tag { Label = "test", Owner = profileId } };

            var actual = (Tag[])SubjUtils.CheckHttpResult(
                    searchController.SearchNames(new qlanir_web_api.DataContext.DataContextFilter { Type = "tag", Term = "test" }));


            expected.Match(actual);        
        }
         

        // person linked to profile directly

        void it_create_person_as_linked_directly_with_profile()
        {
            var person = SubjUtils.CreateSubject<Person>("Person Direct_Profile_Link", profileId);
            person.PushLink(new PersonLink
            { 
                Subject = new PersonBase { Id = profileId }, 
                Tag = new Tag { Label = "знакомый" }, 
                HostTag = new Tag { Id = (int)RoleTypes.Acquitance } 
            });

            var actual = (Person)SubjUtils.CheckHttpResult(subjectsController.PostPerson(person));

            person.People[0].Subject.Label = SubjUtils.GetUserName();
            person.People[0].Subject.Owner = profileId;
            
            person.People[0].HostTag.Label = "знакомый";
            person.People[0].HostTag.Owner = profileId;
            person.People[0].HostTag.Id = (int)RoleTypes.Acquitance;

            person.People[0].Tag.Label = "знакомый";
            person.People[0].Tag.Owner = profileId;
            person.People[0].Tag.Id = (int)RoleTypes.Acquitance;

            person.Match(actual);

        }

        void it_find_person_as_linked_directly_with_profile_by_profile_label()
        {
            var expected = SubjUtils.CreateSearchResult("person", "max");
            expected.TotalCount.People = 2;
            expected.PushSerachResultItem(new SearchResultItem
            {
                Name = "<em>"+SubjUtils.GetUserName()+"</em>",
                Type = "person"
            });
            expected.PushSerachResultItem(new SearchResultItem
            {
                Name = "Person Direct_Profile_Link",
                Type = "person",
                Hits = new [] {new SearchResultItemHit { Hit = "<em>"+SubjUtils.GetUserName()+"</em>", Type = "person" }}
            });

            var actual = (SearchResult)SubjUtils.CheckHttpResult(
                searchController.Search(new qlanir_web_api.DataContext.DataContextFilter { Type = "person", Term = "max", IncludeLinkedTags = false }));

            expected.Match(actual);

            //
            var expected1 = SubjUtils.CreateSubject<Person>("Person Direct_Profile_Link", profileId);
            expected1.PushLink(new PersonLink
            {
                Subject = new PersonBase { Id = profileId },
                Tag = new Tag { Id = (int)RoleTypes.Acquitance },
                HostTag = new Tag { Id = (int)RoleTypes.Acquitance }
            });           

            expected1.People[0].Subject.Label = SubjUtils.GetUserName();
            expected1.People[0].Subject.Owner = profileId;

            expected1.People[0].HostTag.Label = "знакомый";
            expected1.People[0].HostTag.Owner = profileId;

            expected1.People[0].Tag.Label = "знакомый";
            expected1.People[0].Tag.Owner = profileId;

            var foundId = actual.Results[1].Id;
            var actual1 = (Person)SubjUtils.CheckHttpResult(subjectsController.GetPerson(foundId));

            expected1.Match(actual1);

            personId = actual1.Id;                
        }

        void it_remove_person_as_linked_directly_with_profile()
        {
            SubjUtils.CheckHttpEmptyResult(subjectsController.DeletePerson(personId));
        }

        void it_find_person_as_linked_directly_with_profile_after_remove()
        {
            var expected = SubjUtils.CreateSearchResult("person", null);
            expected.TotalCount.People = 1;
            expected.PushSerachResultItem(new SearchResultItem { Name = SubjUtils.GetUserName(), Type = "person" });

            var actual = (SearchResult)SubjUtils.CheckHttpResult(
            searchController.Search(new qlanir_web_api.DataContext.DataContextFilter { Type = "person", IncludeLinkedTags = false }));

            expected.Match(actual);        
        }


        // person linked to profile via proxy
        
        void it_create_person_as_linked_via_proxy_with_profile()
        {
            // Here create two level link [person] -> [proxy] -> [profile]
            // For the person, there is only one link exist [person] -> [proxy], whats why it returns person model only with one link to proxy
            // TODO: Needed to use diffrent structures, to create person and retrieve person (now it is the same), now it is missleading.
            // To create person we use structure with some data, and then we got back the same structure but the data is different.
            var person = SubjUtils.CreateSubject<Person>("Person Proxy_Profile_Link", profileId);
            person.PushLink(new PersonLink
            {
                //!!! Subject = new PersonBase { Id = profileId },
                Tag = new Tag { Id = (int)RoleTypes.Brother }, // Profile person is a brother
                HostTag = new Tag { Id = (int)RoleTypes.Sister}, // Proxy person is a sister
                Proxy = new PersonBase {  Label = "Proxy Person" }
            });

            var actual = (Person)SubjUtils.CheckHttpResult(subjectsController.PostPerson(person));

            person.People = new[] {new PersonLink
            {
                HostTag = new Tag { Id = (int)RoleTypes.Acquitance, Owner = profileId, Label = "знакомый" },
                Tag = new Tag { Id = (int)RoleTypes.Acquitance, Owner = profileId, Label = "знакомый" },
                Subject = new PersonBase { Label = "Proxy Person", Owner = profileId }
            }};

            person.Match(actual);
        }
        
        void it_find_people_by_no_filter()
        {
            var expected = SubjUtils.CreateSearchResult("person", null);
            expected.TotalCount.People = 3;
            expected.PushSerachResultItem(new SearchResultItem
            {
                Name = "Proxy Person",
                Type = "person"
            });
            expected.PushSerachResultItem(new SearchResultItem
            {
                Name = "Person Proxy_Profile_Link",
                Type = "person"
            });
            expected.PushSerachResultItem(new SearchResultItem
            {
                Name = SubjUtils.GetUserName(),
                Type = "person"
            });


            var actual = (SearchResult)SubjUtils.CheckHttpResult(
                searchController.Search(new qlanir_web_api.DataContext.DataContextFilter { Type = "person", IncludeLinkedTags = false }));

            expected.Match(actual);

        }
        

        
        void it_find_person_as_linked_via_proxy_with_profile_by_proxy_label()
        {
            var expected = SubjUtils.CreateSearchResult("person", "proxy");
            expected.TotalCount.People = 3;
            expected.PushSerachResultItem(new SearchResultItem
            {
                Name = SubjUtils.GetUserName(),
                Type = "person",
                Hits = new[] { new SearchResultItemHit { Type = "person", Hit = "<em>Proxy</em> Person" } }
            });
            expected.PushSerachResultItem(new SearchResultItem
            {
                Name = "Person <em>Proxy_Profile_Link</em>",
                Type = "person",
                Hits = new[] { new SearchResultItemHit {  Type = "person", Hit = "<em>Proxy</em> Person"} }
            });
            expected.PushSerachResultItem(new SearchResultItem
            {
                Name = "<em>Proxy</em> Person",
                Type = "person",
                Hits = new[] { new SearchResultItemHit { Type = "person", Hit = "Person <em>Proxy_Profile_Link</em>" } }
            });

            var actual = (SearchResult)SubjUtils.CheckHttpResult(
                searchController.Search(new qlanir_web_api.DataContext.DataContextFilter { Type = "person", Term = "proxy", IncludeLinkedTags = false }));

            expected.Match(actual);


            //prfoile
            personId = expected.Results[0].Id;


            //
            var expected1 = SubjUtils.CreateSubject<Person>("Proxy Person", profileId);
            
            expected1.PushLink(new PersonLink
            {
                Subject = new PersonBase { Id = profileId },
                Tag = new Tag { Id = (int)RoleTypes.Brother },
                HostTag = new Tag { Id = (int)RoleTypes.Sister }
            });

            expected1.People[0].Subject.Label = SubjUtils.GetUserName();
            expected1.People[0].Subject.Owner = profileId;

            expected1.People[0].HostTag.Label = "сестра";
            expected1.People[0].HostTag.Owner = profileId;

            expected1.People[0].Tag.Label = "брат";
            expected1.People[0].Tag.Owner = profileId;

            expected1.PushLink(new PersonLink
            {
                Subject = new PersonBase { Id = profileId },
                Tag = new Tag { Id = (int)RoleTypes.Acquitance },
                HostTag = new Tag { Id = (int)RoleTypes.Acquitance }
            });

            expected1.People[1].Subject.Label = "Person Proxy_Profile_Link";
            expected1.People[1].Subject.Owner = profileId;

            expected1.People[1].HostTag.Label = "знакомый";
            expected1.People[1].HostTag.Owner = profileId;

            expected1.People[1].Tag.Label = "знакомый";
            expected1.People[1].Tag.Owner = profileId;

            //find proxy
            var foundId = actual.Results[2].Id;
            var actual1 = (Person)SubjUtils.CheckHttpResult(subjectsController.GetPerson(foundId));

            expected1.Match(actual1);

            //
            var expected2 = SubjUtils.CreateSubject<Person>("Person Proxy_Profile_Link", profileId);

            //acq with the proxy
            expected2.PushLink(new PersonLink
            {
                Subject = new PersonBase { Id = actual.Results[2].Id },
                Tag = new Tag { Id = (int)RoleTypes.Acquitance },
                HostTag = new Tag { Id = (int)RoleTypes.Acquitance }
            });

            expected2.People[0].Subject.Label = "Proxy Person";
            expected2.People[0].Subject.Owner = profileId;

            expected2.People[0].HostTag.Label = "знакомый";
            expected2.People[0].HostTag.Owner = profileId;

            expected2.People[0].Tag.Label = "знакомый";
            expected2.People[0].Tag.Owner = profileId;

            foundId = actual.Results[1].Id;
            var actual2 = (Person)SubjUtils.CheckHttpResult(subjectsController.GetPerson(foundId));

            expected2.Match(actual2);

            personId = actual.Results[2].Id;
        }
         

        
        void it_remove_proxy_person()
        {
            SubjUtils.CheckHttpEmptyResult(subjectsController.DeletePerson(personId));
        }
         

        void it_find_person_as_linked_via_proxy_with_profile_after_remove()
        {
            var expected = SubjUtils.CreateSearchResult("person", "proxy");
            expected.TotalCount.People = 1;
            expected.PushSerachResultItem(new SearchResultItem
            {
                Name = "Person <em>Proxy_Profile_Link</em>",
                Type = "person"
            });

            var actual = (SearchResult)SubjUtils.CheckHttpResult(
                searchController.Search(new qlanir_web_api.DataContext.DataContextFilter { Type = "person", Term = "proxy", IncludeLinkedTags = false }));

            expected.Match(actual);        
        }

                
        void it_create_person_with_name_parts()
        {
            var person = SubjUtils.CreateSubject<Person>(null, profileId);
            person.NameParts = new PersonNameParts { 
                FirstName = "first",
                LastName = "last",
                MiddleName = "middle",
                NickName = "nick"
            };

            var actual = (Person)SubjUtils.CheckHttpResult(subjectsController.PostPerson(person));

            person.Label = "last first middle";

            person.Match(actual);

            personId = actual.Id;
        }

        void it_get_person_with_name_parts()
        {
            var person = SubjUtils.CreateSubject<Person>("last first middle", profileId);
            person.NameParts = new PersonNameParts
            {
                FirstName = "first",
                LastName = "last",
                MiddleName = "middle",
                NickName = "nick"
            };

            var actual = (Person)SubjUtils.CheckHttpResult(subjectsController.GetPerson(personId));

            person.Match(actual);
        }

        
        void it_create_person_with_first_name_only()
        {
            var person = SubjUtils.CreateSubject<Person>("firstname", profileId);
            person.NameParts = new PersonNameParts
            {
                FirstName = "firstname"
            };

            var actual = (Person)SubjUtils.CheckHttpResult(subjectsController.PostPerson(person));

            person.Match(actual);

            personId = actual.Id;
        }
        

        void it_get_person_with_first_name_only()
        {
            var person = SubjUtils.CreateSubject<Person>("firstname", profileId);
            person.NameParts = new PersonNameParts
            {
                FirstName = "firstname"
            };

            var actual = (Person)SubjUtils.CheckHttpResult(subjectsController.GetPerson(personId));

            person.Match(actual);
        }
        

        void it_create_person_with_first_name_only_via_label()
        {
            var person = SubjUtils.CreateSubject<Person>("firstname", profileId);

            var actual = (Person)SubjUtils.CheckHttpResult(subjectsController.PostPerson(person));

            person.NameParts = new PersonNameParts
            {
                FirstName = "firstname"
            };

            person.Match(actual);

            personId = actual.Id;
        }


        void it_get_person_with_first_name_only_via_label()
        {
            var person = SubjUtils.CreateSubject<Person>("firstname", profileId);

            var actual = (Person)SubjUtils.CheckHttpResult(subjectsController.GetPerson(personId));

            person.NameParts = new PersonNameParts
            {
                FirstName = "firstname"
            };



            person.Match(actual);
        }

        void it_create_person_with_first_last_name_only_via_label()
        {
            var person = SubjUtils.CreateSubject<Person>("lastname firstname", profileId);

            var actual = (Person)SubjUtils.CheckHttpResult(subjectsController.PostPerson(person));

            person.NameParts = new PersonNameParts
            {
                FirstName = "firstname",
                LastName = "lastname"
            };

            person.Match(actual);

            personId = actual.Id;
        }


        void it_get_person_with_first_last_name_only_via_label()
        {
            var person = SubjUtils.CreateSubject<Person>("lastname firstname", profileId);

            var actual = (Person)SubjUtils.CheckHttpResult(subjectsController.GetPerson(personId));

            person.NameParts = new PersonNameParts
            {
                FirstName = "firstname",
                LastName = "lastname"
            };



            person.Match(actual);
        }

    }
}
