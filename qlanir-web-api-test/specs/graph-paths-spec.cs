﻿
using NSpec;
using qlanir_web_api.Controllers;
using qlanir_web_api.DataContext;
using qlanir_web_api.DataContext.GraphDataContext;
using qlanir_web_api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Results;

namespace qlanir_web_api_test.specs
{
    public class graph_paths_spec : nspec
    {
        int profileId;
        int targetPersonId;

        void it_index_all()
        {
            SubjUtils.IndexAll();
        }

        void it_create_profile()
        {
            profileId = SubjUtils.CreateAndStoreProfile();
        }

        void it_create_people()
        {
            SubjectsController subjectsController = new SubjectsController(new SubjectsDataContext());
            var ident = new System.Security.Principal.GenericIdentity(SubjUtils.GetUserName());
            var pl = new System.Security.Principal.GenericPrincipal(ident, new string[0]);
            subjectsController.RequestContext.Principal = pl;

            var person = SubjUtils.CreateSubject<Person>("person1", profileId);
            person.PushLink(new PersonLink { Subject = new PersonBase { Id = profileId } });

            var person1 = (Person)SubjUtils.CheckHttpResult(subjectsController.PostPerson(person));

            person = SubjUtils.CreateSubject<Person>("person2", profileId);
            person.PushLink(new PersonLink { Subject = new PersonBase { Id = person1.Id } });

            var person2 = (Person)SubjUtils.CheckHttpResult(subjectsController.PostPerson(person));

            targetPersonId = person2.Id;
        }

        void it_get_paths()
        {
            GraphController graphController = new GraphController();
            var ident = new System.Security.Principal.GenericIdentity(SubjUtils.GetUserName());
            var pl = new System.Security.Principal.GenericPrincipal(ident, new string[0]);
            graphController.RequestContext.Principal = pl;

            var paths = graphController.GetPaths(targetPersonId);

            paths.should_not_be_null();
            paths.should_cast_to<OkNegotiatedContentResult<IEnumerable<NodeGraphNode>[]>>();

            var res = ((OkNegotiatedContentResult<IEnumerable<NodeGraphNode>[]>)paths).Content;

           res.Length.should_be(1);
        }

    }
}
