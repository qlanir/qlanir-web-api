﻿using qlanir_web_api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qlanir_web_api_test.specs
{
    class person_update_spec : subj_base_spec
    {
        Person _person;
      
        
        void it_index_all()
        {
            SubjUtils.IndexAll();
        }

        void it_create_profile()
        {
            profileId = SubjUtils.CreateAndStoreProfile();
        }

        
        void it_create_person()
        {
            var person = SubjUtils.CreateSubject<Person>("Person Full", profileId);
            person.PushLink(new ContactLink { Subject = new Contact { Label = "333", ContactType = new Tag { Id = (int)TagTypes.Skype } } });
            person.PushLink(new PersonLink
            {
                HostTag = new Tag { Id = (int)RoleTypes.Sister },
                Tag = new Tag { Id = (int)RoleTypes.Brother },
                Subject = new PersonBase { Label = "New Person" },
                Proxy = new PersonBase {  Label = "Proxy person" }
            });
            person.PushLink(new Link<OfferBase>
            {
                Tag = new Tag { Label = "New Service" },
                Subject = new OfferBase { Label = "Service description" }
            });
            person.PushLink(new Link<OrganizationBase>
            {
                Subject = new OrganizationBase { Label = "New Org" }
            });
            person.Tags = new[] { new Tag { Id = -1 }, new Tag { Label = "test" }, new Tag { Label = "скидка" } };

            var actual = (Person)SubjUtils.CheckHttpResult(subjectsController.PostPerson(person));

            person.Contacts[0].Subject.Owner = profileId;
            person.Contacts[0].Subject.ContactType.Label = "skype";
            person.Contacts[0].Subject.ContactType.Owner = profileId;
            person.Contacts[0].Tag = new Tag { Id = (int)RoleTypes.Identifyers, Label = "идентификаторы", Owner = profileId };

            // see comment in person-create-spec
            person.People = new [] {new PersonLink
            {
                HostTag = new Tag { Id = (int)RoleTypes.Acquitance, Owner = profileId, Label = "знакомый" },
                Tag = new Tag { Id = (int)RoleTypes.Acquitance, Owner = profileId, Label = "знакомый" },
                Subject = new PersonBase { Label = "Proxy person", Owner = profileId }
            }};

            person.Offers[0].Subject.Owner = profileId;
            person.Offers[0].Tag.Owner = profileId;

            person.Organizations[0].Tag = new Tag { Id = (int)RoleTypes.Employer, Label = "наниматель", Owner = profileId };
            person.Organizations[0].HostTag = new Tag { Id = (int)RoleTypes.Employee, Label = "наёмник", Owner = profileId };
            person.Organizations[0].Subject.Owner = profileId;

            person.Tags[0].Label = "бизнес";
            person.Tags[1].Owner = profileId;
            person.Tags[2].Id = -6;
            person.Tags = person.Tags.OrderBy(p => p.Label).ToArray();

            person.Match(actual);

            _person = actual;
        }
        
        void it_update_person()
        {            
            var person = SubjUtils.CreateSubject<Person>("Person Updated", profileId);
            person.Birthdate = "11.03.1979";
            person.PushLink(new ContactLink 
            {
                Id = _person.Contacts[0].Id,
                Subject = new Contact { Label = "facebook org", ContactType = new Tag { Id = (int)TagTypes.Facebook } } 
            });
            person.PushLink(new Link<OrganizationBase>
            {
                Id = _person.Organizations[0].Id,
                Subject = new OrganizationBase { Label = "New Org Updated" }
            });
            person.PushLink(new PersonLink
            {
                Id = _person.People[0].Id,
                HostTag = new Tag { Id = (int)RoleTypes.Employer },
                Tag = new Tag { Id = (int)RoleTypes.Employee },
                Subject = new PersonBase { Label = "Updated Person" },
                Proxy = new PersonBase {  Label = "Updated Proxy" }
            });
            person.PushLink(new PersonLink
            {
                HostTag = new Tag { Id = (int)RoleTypes.Brother },
                Tag = new Tag { Id = (int)RoleTypes.Sister },
                Subject = new PersonBase { Label = "Sister" }
            });
            person.PushLink(new Link<OfferBase>
            {
                Id = _person.Offers[0].Id, //здесь может быть 0 или любое другое значение, и по нему не ищется линк, ищется  по subject.id
                Tag = new Tag { Label = "Updated Service" },
                Subject = new OfferBase { Id = _person.Offers[0].Subject.Id, Label = "Updated service description" }
            });
            person.Tags = new[] { new Tag { Label = "test" } };

            var actual = (Person)SubjUtils.CheckHttpResult(subjectsController.PutPerson(person, _person.Id));

            person.Contacts[0].Subject.Owner = profileId;
            person.Contacts[0].Subject.ContactType.Label = "facebook";
            person.Contacts[0].Subject.ContactType.Owner = profileId;
            person.Contacts[0].Tag = new Tag { Id = (int)RoleTypes.Identifyers, Label = "идентификаторы", Owner = profileId };

            // see comment in person-create-spec
            person.People = new[] {new PersonLink {            
                HostTag = new Tag { Id = (int)RoleTypes.Brother, Owner = profileId, Label = "брат" },
                Tag = new Tag { Id = (int)RoleTypes.Sister, Owner = profileId, Label = "сестра" },
                Subject = new PersonBase { Label = "Sister", Owner = profileId }
            }, new PersonLink {
                HostTag = new Tag { Id = (int)RoleTypes.Acquitance, Owner = profileId, Label = "знакомый" },
                Tag = new Tag { Id = (int)RoleTypes.Acquitance, Owner = profileId, Label = "знакомый" },
                Subject = new PersonBase { Label = "Updated Proxy", Owner = profileId }
            }
            };

            person.Offers[0].Subject.Owner = profileId;
            person.Offers[0].Tag.Owner = profileId;

            person.Organizations[0].Tag = new Tag { Id = (int)RoleTypes.Employer, Label = "наниматель", Owner = profileId };
            person.Organizations[0].HostTag = new Tag { Id = (int)RoleTypes.Employee, Label = "наёмник", Owner = profileId };
            person.Organizations[0].Subject.Owner = profileId;

            person.Tags[0].Owner = profileId;

            person.Match(actual);
        }
        
    }
}
